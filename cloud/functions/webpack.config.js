const path = require('path');
var nodeExternals = require('webpack-node-externals');

module.exports = {
	entry: {
		app: './src/index.js'
	},
	output: {
		filename: 'index.js', // <-- Important
		libraryTarget: 'this', // <-- Important
		path: path.resolve(__dirname)
	},
	target: 'node', // in order to ignore built-in modules like path, fs, etc.
	externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
	module: {
		rules: [{
				test: /\.(js|jsx)$/,
				exclude: [/node_modules/],
				use: {
					loader: "babel-loader",
					options: {
						presets: ['@babel/preset-env', "@babel/preset-react"]
					}
				}
			}, {
				test: /\.(png|jpe?g|gif|svg)$/,
				use: [{
					loader: 'file-loader',
				}, ],
			}, {
				test: /\.html$/,
				loader: 'html-loader',
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			}
		]
	},
	mode: "production"
};

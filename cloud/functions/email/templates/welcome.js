const defaultValues = {
	title: "Welcome!!",
	name: "Valued User",
}
exports.welcome = (details) => {
	return `
	<h1>${details.title?details.title:defaultValues.title}</h1>
	<div>Hello ${details.name?details.name:defaultValues.name}, welcome to sealdealfinder.com.</div>`
}

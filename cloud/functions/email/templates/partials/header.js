//const logo = require("../../images/website-logo177.png")
const {
	projectId
} = require("../../../util/admin")
exports.header = (devPreview) => {
	let baseUrl
	switch (projectId) {
		case "seal-deal-finder-app":
			baseUrl = "https://sealdealfinder.com"
			break;
		case "seal-deal-finder-dev-f0675":
			baseUrl = "https://dev.sealdealfinder.com"
			break;
	}

	if (devPreview) {
		baseUrl = "http://localhost:3000"
	}

	return `<img src="${baseUrl}/static/media/website-logo@177.829deccb.png" alt="logo" width="300"/><br/>`
}

const {
	header
} = require("./header")
const {
	templateSelector
} = require("../templateSelector")
const {
	footer
} = require("./footer")

exports.main = (template, data, devPreview) => {
	return `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
    .footer-border {
        width:100%;
        height:35px;
        background-color:#51C46C;
        margin-top: 35px;
    }
    .header-border {
        width:100%;
        height:5px;
        background-color:#51C46C;
        margin-bottom: 15px;
    }
    </style>
</head>
<div class="header-border"></div>
<body>

 ${header(devPreview)}
 ${templateSelector(template,data)}
${footer()}

</body>
<div class="footer-border"></div>
</html>`

}

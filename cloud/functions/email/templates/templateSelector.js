const {
	welcome
} = require("../templates/welcome")

const {
	verification
} = require("../templates/admin/verification")


exports.templateSelector = (template, data) => {
	switch (template) {
		case "welcome":
			return welcome(data)
			break;
		case "verification":
			return verification(data)
			break;
		default:
	}
	return `<div>${data}</div>`
}

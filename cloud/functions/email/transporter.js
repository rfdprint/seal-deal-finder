//Nodemailer Transporter
const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const {
	projectId
} = require('../util/admin');

const functionsConfig = functions.config().env;
const account = functionsConfig.mailer[`${projectId}`]

let nodemailerTransporter = nodemailer.createTransport({
	host: account.smtp.host,
	port: account.smtp.port,
	//secure: account.smtp.secure,
	auth: {
		user: account.smtp.user,
		pass: account.smtp.pass
	},
	logger: false,
	debug: false // include SMTP traffic in the logs
})

module.exports = {
	nodemailerTransporter
}

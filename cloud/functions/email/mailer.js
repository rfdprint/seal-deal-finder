const {
	admin,
	db,
	envInfo
} = require('../util/admin');

const {
	nodemailerTransporter
} = require("./transporter")

const Template = require('./ejs/modules/Template');
/*const {
	welcome
} = require('./templates/welcome');*/

exports.getEmailPreview = async (req, res) => {
	//Best if used with Postman
	data = req.body
	data.imageBaseUrl = envInfo.url
	const emailTemplate = await new Template(data.template, data).render()
	return res.send(emailTemplate)
}

exports.useMailer = async (req, res) => {
	const data = req.body
	data.imageBaseUrl = envInfo.url
	$template = await new Template(data.template, data).render()
	// Message object
	let message = {
		from: data.from,
		to: data.to,
		subject: data.subject,
		html: $template,
		attachments: [{ // use URL as an attachment
			filename: "",
			path: ""
		}, ]
	};


	nodemailerTransporter.sendMail(message, (err, data) => {
		if (err) {
			return res.json({
				error: 'Error occurs'
			});
		}
		return res.json({
			success: 'Email sent!!!'
		});
	});
}


exports.useMailerWithAttachments = (req, res) => {
	const data = req.body
	console.log(data)
	let attachmentsDocument = {}
	db.doc(`/${data.attachments.collection}/${data.attachments.document}`)
		.get()
		.then(doc => {
			if (!doc.exists) {
				return res.json({
					message: {
						info: "Attacthment not found"
					}
				})
			}
			attachmentsDocument = doc.data()
			const filePath = `${attachmentsDocument.fileLocation}/${attachmentsDocument.fileName}`
			console.log(filePath)
			return getAttachmentSignedURL(filePath, 2)
		})
		.then((path) => {
			console.log(path)
			let attachment = {
				filename: `attachment_${attachmentsDocument.originalFileName}`,
				path: path
			}
			return attachment
		})
		.then(attachment => {

			//console.log(attachment)
			// Message object
			let message = {
				from: data.from,
				to: data.to,
				subject: data.subject,
				html: main(
					data.template,
					data.templateDetails
				),
				attachments: [{ // use URL as an attachment
					filename: attachment.filename,
					path: attachment.path
				}, ]
			};

			//Template Literals
			nodemailerTransporter.sendMail(message, (err, data) => {
				if (err) {
					return res.json({
						error: err
					})
				}
				return res.json({
					success: 'Email sent!!!',
					data
				})
			});

		})
		.catch(err => {
			console.log(err)
			return res.json(err)
		})
}

const getAttachmentSignedURL = async (file, hoursToExpiration) => {
	const bucket = admin.storage().bucket();

	// ... inside your function:

	const urlOptions = {
		//version: "v4",
		action: "read",
		expires: Date.now() + 1000 * 60 * 60 * hoursToExpiration,
	}

	const url = await bucket.file(file)
		.getSignedUrl(urlOptions)

	return url[0]

}

const getAttachmentLongLivedURL = async (expires, file) => {
	const bucket = admin.storage().bucket();

	// ... inside your function:

	const urlOptions = {
		//version: "v4",
		action: "read",
		expires: expires,
	}

	const url = await bucket.file(file)
		.getSignedUrl(urlOptions)

	return url

}

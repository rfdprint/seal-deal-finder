const ejs = require('ejs');
var read = require('fs').readFileSync;
var join = require('path').join;

class Template {
	constructor(template, data) {
		this.template = template
		this.data = data
	}

	render = async () => {
		console.log(this.data)
		const joinedTemplate = join(__dirname, `../views/${this.template}.ejs`);
		const $template = await ejs.compile(read(joinedTemplate, 'utf8'), {
			filename: joinedTemplate
		})
		return $template(this.data)
	}
}

module.exports = Template

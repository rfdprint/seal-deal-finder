const hbs = require('nodemailer-express-handlebars');
const {
	nodemailerTransporter
} = require("./transporter")
const {
	main
} = require("./templates/partials/main")


exports.useMailer = (req, res) => {
	const data = req.body

	//Handlebars
	nodemailerTransporter.use('compile', hbs({
		viewEngine: {
			extname: '.hbs', // handlebars extension
			layoutsDir: './email/templates/', // location of handlebars templates
			defaultLayout: 'main', // name of main template
			partialsDir: './email/templates/partials', // location of your subtemplates aka. header, footer etc
		},
		viewPath: './email/templates/',
		extName: ".hbs"
	}));

	let mailOptions = {
		from: 'virgil30@ethereal.email', // TODO: email sender
		to: 'virgil30@ethereal.email', // TODO: email receiver
		subject: 'Nodemailer - Test',
		template: 'hello',
		context: {
			name: 'Test User'
		} // send extra values to template
	};


	//Template Literals
	nodemailerTransporter.sendMail(mailOptions, (err, data) => {
		if (err) {
			return res.json({
				error: 'Error occurs'
			});
		}
		return res.json({
			success: 'Email sent!!!'
		});
	});
}

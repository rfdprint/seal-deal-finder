const functions = require('firebase-functions');
const app = require('express')();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const {
	admin
} = require('./util/admin');

const {
	login,
	getAuthenticatedUser,
	getUserProfileData,
} = require('./handlers/users');
const {
	initEmuUser,
	initDealData,
	appInitUsers
} = require('./handlers/emu')



const FBAuth = require('./util/fbauth');
const cors = require('cors');

app.use(cors());
//app.use(bodyParser.json());
app.use(cookieParser()); //Cookies

//rewrite api prefix
const API_PREFIX = 'api';
// Rewrite Firebase hosting requests: /api/:path => /:path
app.use((req, res, next) => {
	if (req.url.indexOf(`/${API_PREFIX}/`) === 0) {
		req.url = req.url.substring(API_PREFIX.length + 1);
	}
	next();
});

//User routes
app.post('/login', login);
app.get('/user', getAuthenticatedUser);
app.get('/user/:handle', getUserProfileData);
//Test
app.post("/emu", initEmuUser)
app.post("/emu/deals", initDealData)
app.post("/emu/init/users", appInitUsers)

exports.api = functions.https.onRequest(app);

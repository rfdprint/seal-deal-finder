const functions = require("firebase-functions");
const app = require("express")();
const {
	deleteImages
} = require('./util/utilFunctions')
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const methodOverride = require('method-override')
const {
	admin
} = require("./util/admin");

//SSR
/*import React, {
	Fragment
} from "react";
import {
	Helmet
} from "react-helmet";
import ReactDOMServer from "react-dom/server";
//import App from "../../../client/src/App";
import fs from "fs";
import path from "path";
*/

const {
	buildSitemap
} = require("./handlers/sitemap")

const {
	ssrDealPage
} = require("./handlers/ssr")

const {
	facebookPostDeal,
	facebookExtendAccess,
	facebookGetPage,
	facebookTokenAccess,
} = require("./handlers/facebook");

const {
	signup,
	signup2,
	login,
	logout,
	uploadUserImage,
	editUserDetails,
	getAuthenticatedUser,
	getUserDetails,
	deleteUser,
	systemGetUsers,
	postVerifacation,
	uploadVerifacationFile,
	getVerificationFile,
	getUserProfileData,
	setUserRole,
	deleteReadNotifications,
	getNotifications,
	resetPasswordEmail,
	deleteOwnUserAccount
} = require("./handlers/users");

const {
	postDeal,
	deleteDeal,
	editDeal,
	getAllDeals,
	getOneDeal,
	uploadDealImage,
} = require("./handlers/deals");

const {
	commentOnDeal,
	deleteCommentOnDeal,
	updateCommentOnDeal,
	blockCommentOnDeal,
} = require("./handlers/comments");

const {
	getAddressByZipcode,
	getAddressSuggestion,
	getGeocode,
} = require("./handlers/addresses");

const {
	serverManualPaymentIntent,
	confirmPaymentIntent,
} = require("./handlers/checkout");

const {
	getConversations,
	deleteConversation,
	sendDirectMessage,
	deleteMessage,
	sendDirectMessageReply,
	getMessagesByConversationId
} = require("./handlers/inbox");

const {
	updatePaymentDetails,
	updateBillingDocument,
	createSubscription,
	getSubscription,
	getSubscriptionSystem,
	confirmSubscriptionPaymentWebhook,
	createSubcriptionDocument,
	getLiveSubscription,
	getPaymentMethod,
	cancelSubscription,
} = require("./handlers/subscriptions");

/*(const {
	useMailer,
	useMailerWithAttachments,
	getEmailPreview,
} = require("./email/mailer");*/

const {
	subcriptionDeleted,
	chargeUpdated
} = require("./handlers/webhooks");

const {
	initEmuUser,
	initDealData,
	appInitUsers,
	emuEnv
} = require("./handlers/emu");

const FBAuth = require("./util/fbauth");
const FBSystemAuth = require("./util/fbclaims");
const cors = require("cors");
const {
	json
} = require("body-parser");

app.use(cors());
//app.use(bodyParser.json());
app.use(cookieParser()); //Cookies

app.use(bodyParser.json({
	limit: "50mb"
}));
app.use(bodyParser.urlencoded({
	limit: "50mb",
	extended: true,
	parameterLimit: 50000
}));

//rewrite api prefix
const API_PREFIX = "api";
// Rewrite Firebase hosting requests: /api/:path => /:path
app.use((req, res, next) => {
	if (req.url.indexOf(`/${API_PREFIX}/`) === 0) {
		req.url = req.url.substring(API_PREFIX.length + 1);
	}
	next();
});

//SItemap sitemap
app.get("/sitemap", buildSitemap);
//SSR
app.get("/ssr/deals/:dealId", ssrDealPage);
//Facebook
app.post("/facebook/post/deal", facebookPostDeal); //FBSystemAuth,
app.post("/facebook/extend/access", facebookExtendAccess);
app.get("/facebook/posts", facebookGetPage);
app.get("/test/token/3558", facebookTokenAccess)
//User routes
app.post("/signup", signup2);
app.post("/login", login);
app.post("/logout", FBAuth, logout);
app.post("/user/image", FBAuth, uploadUserImage);
app.post("/user", FBAuth, editUserDetails);
app.get("/user", FBAuth, getAuthenticatedUser);
app.get("/user/:handle", getUserProfileData);
app.post("/user/reset/password", resetPasswordEmail);
app.delete("/user", FBAuth, deleteOwnUserAccount)
app.get("/notifications", FBAuth, getNotifications)
//Conversation routes
app.get("/conversations", FBAuth, getConversations)
app.delete("/conversations/delete/:conversationId", FBAuth, deleteConversation)
app.get("/messages/:conversationId", FBAuth, getMessagesByConversationId)
app.post("/message/send", FBAuth, sendDirectMessage);
app.post("/message/send/reply", FBAuth, sendDirectMessageReply);
app.delete("/message/delete/:messageId", FBAuth, deleteMessage);
//Notifications
app.delete("/notifications", FBAuth, deleteReadNotifications);
//Deal routes
app.get("/deals", getAllDeals);
app.get("/deal/:dealId", getOneDeal);
app.post("/deal", FBAuth, postDeal);
app.post("/deal/image", FBAuth, uploadDealImage);
app.post("/deal/edit", FBAuth, editDeal);
app.delete("/deal/:dealId", FBAuth, deleteDeal);
//Comment routes
app.post("/deal/:dealId/comment", FBAuth, commentOnDeal);
app.post("/deal/comment/update", FBAuth, updateCommentOnDeal);
app.post("/deal/comment/block", FBAuth, blockCommentOnDeal);
app.delete("/deal/comment/:commentId", FBAuth, deleteCommentOnDeal);
//Addresses
app.post("/addresses/zipcode", getAddressByZipcode);
app.get("/addresses/suggestion", getAddressSuggestion);
app.get("/addresses/geocode", getGeocode);
//System routes
app.post("/system/user/role", FBAuth, setUserRole);
app.get("/system/users", FBSystemAuth, systemGetUsers);
app.post("/system/users/verify", FBAuth, postVerifacation);
app.post("/system/verification", FBAuth, uploadVerifacationFile);
app.post("/system/verification/url", FBAuth, getVerificationFile);
app.delete("/system/user", FBSystemAuth, deleteUser)
//Subscription routes
app.post("/charge", serverManualPaymentIntent);
app.post("/charge/confirm", confirmPaymentIntent);
app.post("/charge/subscription", FBAuth, createSubscription);
app.post(
	"/charge/subscription/confirm/webhook",
	bodyParser.raw({
		type: "application/json",
	}),
	confirmSubscriptionPaymentWebhook
);
app.post("/subscription/create/document", FBAuth, createSubcriptionDocument);
app.post("/subscription/update/live", FBSystemAuth, getLiveSubscription);
app.post("/subscription/system", FBSystemAuth, getSubscriptionSystem);
app.post("/subscription/", FBAuth, getSubscription);
app.post("/subscription/update/payment", FBAuth, updatePaymentDetails);
app.post("/subscription/update/billing/document", FBAuth, updateBillingDocument)
app.post("/subscription/payment/method", FBAuth, getPaymentMethod);
app.delete("/subscription/cancel", FBAuth, cancelSubscription);
//Mailer
/*
app.post("/mailer/send", FBAuth, useMailer);
app.post("/mailer/send/attachment", FBSystemAuth, useMailerWithAttachments);
*/
//Preview (dev)
/*
app.post("/preview/email/veiw", getEmailPreview);
*/
//Webhooks
app.post(
	"/webhook/subscription/deleted",
	bodyParser.raw({
		type: "application/json",
	}),
	subcriptionDeleted
);
app.post(
	"/webhook/charge/updated",
	bodyParser.raw({
		type: "application/json",
	}),
	chargeUpdated
);
//Test
app.post("/emu/env", emuEnv); //Temp
app.post("/emu", initEmuUser);
app.post("/emu/deals", initDealData);
app.post("/emu/init/users", appInitUsers);

exports.api = functions.https.onRequest(app);
app.use(bodyParser.json({
	limit: '50mb'
}));
app.use(bodyParser.urlencoded({
	limit: '50mb',
	extended: true
}));

exports.onUserImageChange = functions.firestore
	.document("users/{userId}")
	.onUpdate((change) => {

		const previousImage = change.before.data().imageUrl
		const newImage = change.after.data().imageUrl

		if (change.before.data().imageUrl !== change.after.data().imageUrl) {

			//console.log('image has changed');
			let newParticipantData
			let batch = db.batch();
			return db
				.collection("deals")
				.where("userId", "==", change.before.data().userId)
				.get()
				.then((data) => {
					//.log(change.after.data().imageUrl)
					data.forEach((doc) => {
						const deal = db.doc(`/deals/${doc.id}`);
						batch.update(deal, {
							userImage: change.after.data().imageUrl,

						});
					});
					return batch.commit();
				})
				.then(() => {
					batch = db.batch();
					return db
						.collection("conversations")
						.where('participants', 'array-contains', change.before.data().userId)
						.get()
						.then((data) => {
							if (!data.empty) {
								data.forEach((doc) => {
									if (doc.data().firstParticipant.id === change.before.data().userId) {
										conversation = db.doc(`/conversations/${doc.id}`);
										newParticipantData = {
											firstParticipant: {
												id: doc.data().firstParticipant.id,
												imageUrl: change.after.data().imageUrl,
												handle: doc.data().firstParticipant.handle
											}
										}
										batch.update(conversation, newParticipantData);
									}
									if (doc.data().secondParticipant.id === change.before.data().userId) {
										conversation = db.doc(`/conversations/${doc.id}`);
										newParticipantData = {
											secondParticipant: {
												id: doc.data().secondParticipant.id,
												imageUrl: change.after.data().imageUrl,
												handle: doc.data().secondParticipant.handle
											}
										}
										batch.update(conversation, newParticipantData);
									}
								});
								return batch.commit().then((data) => {
									//console.log(data)
								})
							}

						})
				})
				.then(() => {
					batch = db.batch();
					return db
						.collection("notifications")
						.where('participants', 'array-contains', change.before.data().userId)
						.get()
						.then((data) => {
							if (!data.empty) {
								data.forEach((doc) => {
									if (doc.data().sender.id === change.before.data().userId) {
										notification = db.doc(`/notifications/${doc.id}`);
										newParticipantData = {
											sender: {
												id: doc.data().sender.id,
												imageUrl: change.after.data().imageUrl,
												handle: doc.data().sender.handle
											}
										}
										batch.update(notification, newParticipantData);
									}
								});
								return batch.commit()
							}

						})
				})
				.then(() => {
					if (change.before.data().imageLocation) {
						let oldImage = {
							location: change.before.data().imageLocation,
							fileName: change.before.data().imageFileName,
						};
						return admin
							.storage()
							.bucket()
							.file(`${oldImage.location}/${oldImage.fileName}`)
							.delete();
					}
				})
				.then(() => {
					console.log(
						`File ${change.before.data().imageFileName} deleted `
					);
				}).catch((err) => {
					console.log("***onUserImageChange***")
					console.log(err)
					return true
				})
		} else return true;

	});

exports.onUserVerificationChange = functions.firestore
	.document("users/{userId}")
	.onUpdate((change) => {
		//console.log(change.before.data());
		//console.log(change.after.data());

		if (change.before.data().verified !== change.after.data().verified) {
			//console.log('image has changed');
			const batch = db.batch();
			return db
				.collection("deals")
				.where("userId", "==", change.before.data().userId)
				.get()
				.then((data) => {
					data.forEach((doc) => {
						const deal = db.doc(`/deals/${doc.id}`);
						batch.update(deal, {
							userVerified: change.after.data().verified,
						});
					});
					return batch.commit();
				});
		} else return true;
	});

exports.onUserTypeChange = functions.firestore
	.document("users/{userId}")
	.onUpdate((change) => {
		//console.log(change.before.data());
		//console.log(change.after.data());

		if (change.before.data().type !== change.after.data().type) {
			//console.log('image has changed');
			const batch = db.batch();
			return db
				.collection("deals")
				.where("userId", "==", change.before.data().userId)
				.get()
				.then((data) => {
					data.forEach((doc) => {
						const deal = db.doc(`/deals/${doc.id}`);
						batch.update(deal, {
							userType: change.after.data().type,
						});
					});
					return batch.commit();
				})
				.catch(err => {
					console.log("***onUserImageChange***")
					console.log(err)
				})
		} else return true;
	});

/*exports.onUserDelete = functions.firestore
	.document("users/{userId}")
	.onDelete((snap, context) => {
		const {
			userId
		} = context.params;
		const bucket = admin.storage().bucket();

		return bucket.deleteFiles({
			prefix: `/${userId}`,
		});
	});
*/
exports.onVerificationDocumentDelete = functions.firestore
	.document("verifications/{verificationId}")
	.onDelete((snap, context) => {
		const {
			verificationId
		} = context.params;
		const bucket = admin.storage().bucket();

		return bucket.deleteFiles({
			prefix: `files/verification/${verificationId}`,
		});
	});

exports.onUserDelete = functions.firestore
	.document("users/{userId}")
	.onDelete((snap, context) => {
		const {
			userId
		} = context.params;
		const bucket = admin.storage().bucket();

		let batch = db.batch();
		//Delete deals
		return db
			.collection("deals")
			.where("userId", "==", userId)
			.get()
			.then((data) => {
				data.forEach((doc) => {
					const deal = db.doc(`/deals/${doc.id}`);
					batch.delete(deal)
				})
				batch.commit();
				console.log("deals deleted")
			})
			.then(() => {
				let subscriptionBatch = db.batch();
				//Delete subscription
				return db
					.collection("subscriptions")
					.where("userId", "==", userId)
					.get()
					.then((data) => {
						data.forEach((doc) => {
							const subscription = db.doc(`/subscriptions/${doc.id}`);
							subscriptionBatch.delete(subscription)
						})
						subscriptionBatch.commit();
						console.log("subscriptions deleted")
					})
					.catch((err) => {
						console.log(err)
					})
			})
			.then(() => {
				//Delete messages
				let messagesBatch = db.batch();
				db.collection("messages")
					.where("userId", "==", userId)
					.get()
					.then((data) => {
						data.forEach((doc) => {
							const message = db.doc(`/messages/${doc.id}`);
							messagesBatch.delete(message)
						})
						messagesBatch.commit();
						console.log("messages deleted")
					})
					.catch((err) => {
						console.log(err)
					})
			})
			.then(() => {
				bucket.deleteFiles({
					prefix: `images/profiles/${userId}`
				}).catch(err => console.log(err));
			})
			.then(() => {
				//Delete verification documents and files
				bucket.deleteFiles({
					prefix: `files/verification/${userId}`,
				});
				let verificationsBatch = db.batch();
				db.collection("verifications").where("userId", "==", userId)
					.get()
					.then((data) => {
						data.forEach((doc) => {
							const verification = db.doc(`/verifications/${doc.id}`);
							verificationsBatch.delete(verification)
						})
						verificationsBatch.commit();
						console.log("verifications deleted")
					})
					.catch(err => {
						console.log(err)
					})
			})
			.then(() => {
				//Delete notifications
				let notificationsBatch = db.batch();
				db.collection('notifications')
					.where('participants', 'array-contains', userId)
					.get()
					.then((data) => {
						data.forEach((doc) => {
							const notification = db.doc(`/notifications/${doc.id}`);
							notificationsBatch.delete(notification)
						})
						notificationsBatch.commit();
						console.log("notifications deleted")
					})
					.catch(err => {
						console.log(err)
					})
			})
			.then(() => {
				return true;
			})
			.catch(err => {
				console.log(err)
			})

	});

exports.onDealDelete = functions.firestore //firebase deploy --only functions:onDealDelete
	.document("deals/{dealId}")
	.onDelete((snapshot, context) => {
		const {
			dealId
		} = context.params;

		const bucket = admin.storage().bucket();

		let address = db.doc(`addresses/${dealId}`);
		return address
			.get()
			.then((doc) => {
				if (doc.exists) {
					address.delete().catch((err) => console.error(err));
				}
			})
			.then(() => {
				return bucket.deleteFiles({
					prefix: `images/deals/${dealId}`,
				});
			})
			.catch((err) => console.error(err));
	});


exports.createNotificationOnMessage = functions
	.firestore.document('messages/{id}')
	.onCreate((snapshot) => {
		let sender;
		let recipient;
		return db.doc(`/conversations/${snapshot.data().conversationId}`)
			.get()
			.then((doc) => {

				if (doc.data().firstParticipant.id === snapshot.data().senderId) {
					sender = doc.data().firstParticipant
					recipient = doc.data().secondParticipant
				} else {
					sender = doc.data().secondParticipant
					recipient = doc.data().firstParticipant
				}

				return db.doc(`/notifications/${snapshot.id}`).set({
						sender,
						recipient,
						notificationId: snapshot.id,
						createdAt: new Date().toISOString(),
						recipientId: snapshot.data().recipientId,
						participants: doc.data().participants,
						messageId: snapshot.id,
						conversationId: snapshot.data().conversationId,
						type: 'message',
						read: false
					})
					.catch((err) => {
						console.log("notifications create err")
						console.error(err);
						return;
					});
			})

	});

exports.onConversationDelete = functions.firestore //firebase deploy --only functions:onDealDelete
	.document("conversations/{conversationId}")
	.onDelete((snapshot, context) => {
		const {
			conversationId
		} = context.params;

		let batch = db.batch();
		//console.log("onConversationDelete launched")
		return db.collection('messages')
			.where('conversationId', '==', conversationId)
			.get()
			.then((data) => {
				data.forEach((doc) => {
					const message = db.doc(`/messages/${doc.id}`);
					batch.delete(message)
				})
				//console.log(`messages deleted conversationId ${conversationId}`)
				return batch.commit();
			})
			.catch(err => {
				return console.log(err)
			})

	});

exports.onMessageDelete = functions.firestore //firebase deploy --only functions:onDealDelete
	.document("messages/{messageId}")
	.onDelete((snapshot, context) => {
		const {
			messageId
		} = context.params;

		let batch = db.batch();
		//console.log("onMessageDelete launched")
		return db.collection('notifications')
			.where('messageId', '==', messageId)
			.get()
			.then((data) => {
				data.forEach((doc) => {
					const notification = db.doc(`/notifications/${doc.id}`);
					batch.delete(notification)
				})
				//console.log(`notifications deleted messageId ${messageId}`)
				return batch.commit();
			})
			.catch(err => {
				return console.log(err)
			})

	});

const {
	admin,
	db
} = require('./admin');

const {
	errorRreponse
} = require('./errors')

module.exports = (req, res, next) => {
	const sessionCookie = req.cookies.__session || '';

	// Verify the session cookie. In this case an additional check is added to detect
	// if the user's Firebase session was revoked, user deleted/disabled, etc.
	admin.auth().verifySessionCookie(
			sessionCookie, true /** checkRevoked */ )
		.then((decodedToken) => {
			//console.log(decodedToken);
			req.user = decodedToken;
			//console.log(decodedToken)

			//Verify user is admin
			let isAdmin = req.user.admin

			if (typeof isAdmin === "undefined") {
				isAdmin = false
			}

			if (!isAdmin) {
				console.log(isAdmin)
				return res.status(403).send({
					error: {
						alert: {
							title: "Unauthorized Content",
							text: "You do not have sufficeint authorization to access this content"
						}
					}
				});
			} else {
				return next();
			}
		})
		.catch(err => {
			console.error('Error while verifying token ', err)
			return res.status(403).json({
				error: {
					alert: errorRreponse(err.code)
				}
			})
		})

}

const admin = require("firebase-admin");
const functions = require('firebase-functions');
const firebase = require('firebase');

//ENV
const devEnv = process.env.FUNCTIONS_EMULATOR //true if emu is running
//Firebase Config
const config = JSON.parse(process.env.FIREBASE_CONFIG)
const functionsConfig = functions.config().env

//Admin Config
const adminConfig = config
adminConfig.credential = admin.credential.applicationDefault();
//REMOVE IN PROD

admin.initializeApp(adminConfig);

//Firebase Config with Api
const projectId = config.projectId
config.apiKey = functionsConfig.fb[`${projectId}`].apiKey

//Admin Firestore Database
db = admin.firestore();

//Firebase init
firebase.initializeApp(config);

// Initialize Performance Monitoring and get a reference to the service


//Environment Variables
let envInfo = {}
let stripeConfigSK = {}
switch (projectId) {
	case "seal-deal-finder-dev-f0675":
		if (devEnv) {
			envInfo = {
				env: "local",
				url: "http://localhost:3000"
			}
		} else {
			envInfo = {
				env: "dev",
				url: "https://dev.sealdealfinder.com"
			}
		}
		//Stripe
		stripeConfigSK = functionsConfig.stripe.test; //always a test env
		break;
	case "seal-deal-finder-app":
		if (devEnv) {
			envInfo = {
				env: "local",
				url: "http://localhost:3000"
			}
			//Stripe
			stripeConfigSK = functionsConfig.stripe.test; //test env
		} else {
			envInfo = {
				env: "prod",
				url: "https://sealdealfinder.com"
			}
			//Stripe
			stripeConfigSK = functionsConfig.stripe.prod; //CAUTION: production env only
		}
		break;
	default:
		env = "dev"
}


//Exports
module.exports = {
	db,
	admin,
	firebase,
	config,
	projectId,
	stripeConfigSK,
	devEnv,
	envInfo
};

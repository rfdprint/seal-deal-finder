const regEx = require('./regEx')
const isEmail = (email) => {
	//const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	//TODO: Uncomment the line above in production. In dev it is affecting the bracket color change
	if (email.match(regEx.email)) return true;
	else return false;

}

const isEmpty = (string) => {
	if (String(string).trim() === '') return true;
	else return false
}

exports.validateSignupData = (data) => {
	console.log(data)
	//error handling
	let errors = {};

	if (isEmpty(data.email)) {
		errors.email = 'Must not be empty';
	} else if (!isEmail(data.email)) {
		errors.email = "Must be a valid email address";

	}

	if (isEmpty(data.firstName)) {
		errors.firstName = 'Must not be empty';
	}

	if (isEmpty(data.lastName)) {
		errors.lastName = 'Must not be empty';
	}


	if (isEmpty(data.password)) {
		errors.password = 'Must not be empty';
	}

	if (isEmpty(data.type)) {
		errors.type = 'Must not be empty';
	}

	if (data.password !== data.confirmPassword) {
		errors.confirmPassword = 'Passwords must match';
	}

	if (isEmpty(data.handle)) {
		errors.handle = 'Must not be empty';
	}

	if (!data.terms || !data.privacy) {
		errors.termsPrivacyError = 'you must agree to both Terms and Conditions and the Privacy Policy to continue'
	}

	return {
		errors,
		valid: Object.keys(errors).length === 0 ? true : false
	}

}

exports.validateLoginData = (data) => { //error handling
	let errors = {}

	if (isEmpty(data.email)) {
		errors.email = 'Must not be empty';
	} else if (!isEmail(data.email)) {
		errors.email = "Must be a valid email address";

	}
	if (isEmpty(data.password)) {
		errors.password = "Must not be empty";
	};

	return {
		errors,
		valid: Object.keys(errors).length === 0 ? true : false
	}
}

exports.reduceUserDetails = (data) => {
	let userDetails = {
		...data
	};
	if (data.bio && !isEmpty(data.bio.trim())) userDetails.bio = data.bio;
	if (data.type && !isEmpty(data.type.trim())) userDetails.type = data.type;
	if (data.website && !isEmpty(data.website.trim())) {
		if (data.website.trim().substring(0, 4) !== 'http') {
			userDetails.website = `http://${data.website.trim()}`;
		} else userDetails.website = data.website;
	}
	if (data.location && !isEmpty(data.location.trim())) userDetails.location = data.location;
	return userDetails;
}

exports.validateDealData = (data) => {
	//error handling

	let errors = {};

	if (isEmpty(data.title)) {
		errors.title = 'Must not be empty';
	}

	if (isEmpty(data.price) || !data.price) {
		errors.price = 'Must not be empty';
	}

	if (isEmpty(data.zipcode)) {
		errors.zipcode = 'Must not be empty';
	}
	/*if (isEmpty(data.zipcode)) {
		errors.service = 'Must not be empty';
	}*/

	Object.keys(errors).length !== 0 ? errors.general = "Please correct errors and try again." : null;

	return {
		errors,
		valid: Object.keys(errors).length === 0 ? true : false
	}

}

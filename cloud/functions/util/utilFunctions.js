const {
	admin
} = require('./admin');

exports.deleteImages = (folder, docId) => {
	const bucket = admin.storage().bucket();
	return bucket.deleteFiles({
		prefix: `images/${folder}/${docId}`
	}).catch(err => console.log(err));
}

exports.deleteDocs = (collection, query) => {

	const batch = db.batch();
	let ref = db.collection(collection)

	console.log("createdAt: " + new Date(Date.now() + (1 * 3600 * 1000)).toISOString());

	return ref
		.where(query.field, '==', `${ query.matchValue }`)
		.get()
		.then(data => {
			data.forEach(doc => {
				batch.delete(doc.ref);
			})
			return batch.commit();
		})
		.catch(err => {
			console.log(err)
		})
}

exports.filterObjectkeys = (object, keys) => {
	let i;
	for (i = 0; i < keys.length; i++) {
		delete object[keys[i]];
	}
	return object
}

const {
	admin,
	db
} = require('./admin');


const {
	errorRreponse
} = require('./errors')


module.exports = (req, res, next) => {
	const sessionCookie = req.cookies.__session || '';

	if (!req.cookies.__session) {
		// No cookie
		console.log("Not authorized")
		res.status(403).send({
			error: {
				alert: {
					code: 403,
					text: "Not authorized"
				}
			}
		});
		return;
	}

	// Verify the session cookie. In this case an additional check  is added to detect
	// if the user's Firebase session was revoked, user deleted/disabled, etc.
	admin.auth().verifySessionCookie(
			sessionCookie, true /** checkRevoked */ )
		.then((decodedToken) => {
			//console.log(decodedToken);

			if (decodedToken) {
				let expiration = decodedToken.exp * 1000;
				//console.log("today: " + new Date(Date.now()));
				//console.log("session expires: " + new Date(expiration));
			}
			//console.log(decodedToken)
			req.user = decodedToken;

			return db.collection('users')
				.where('userId', '==', req.user.uid)
				.limit(1)
				.get()
				.catch(err => {
					console.log("***FBAuth***")
					console.log(err)
				});
		})
		.then(data => {
			req.user.handle = data.docs[0].data().handle;
			req.user.imageUrl = data.docs[0].data().imageUrl;
			req.user.type = data.docs[0].data().type;
			req.user.verified = data.docs[0].data().verified;
			return next();
		})
		.catch(err => {
			console.error('Error while verifying token ', err)
			return res.status(403).json({
				error: {
					alert: errorRreponse(err.code)
				}
			})
		})

}

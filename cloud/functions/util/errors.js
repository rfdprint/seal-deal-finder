exports.errorRreponse = (errCode, errMessage) => {
	let title
	let text
	switch (errCode) {
		case "app/network-error": //emulator
			title = "Network Error"
			text = "Error while making data request"
			break;
		case "auth/network-request-failed": //emulator
			title = "Network Request Failed"
			text = "Error while making data request"
			break;

		case "auth/session-cookie-revoked":
			title = "Authorization Has Changed"
			text = "Your sealdealfinder.com authorization was adjusted by a system admin, your current session has ended, please log in again."
			break;
		case "user auth":
			title = "Unable to Authorization"
			text = "Although credentials exist, a data record of this user cannot be found. Please login again, if it persists contact support."
			break;
		default:
			title = "Server Error"
			text = errMessage ? errMessage : `An unknown error as occured, please log in again.`
	}
	return {
		title,
		text
	}
}

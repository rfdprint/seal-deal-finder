const functions = require('firebase-functions');
const app = require('express')();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const {
	admin
} = require('./util/admin');

const {
	signup,
	signup2,
	login,
	logout,
	uploadUserImage,
	editUserDetails,
	getAuthenticatedUser,
	getUserDetails,
	systemGetUsers,
	postVerifacation,
	uploadVerifacationFile,
	getVerificationFile,
	getUserProfileData,
	setUserRole
	//markNotificationsRead
} = require('./handlers/users');

const {
	postDeal,
	deleteDeal,
	editDeal,
	getAllDeals,
	getOneDeal,
	uploadDealImage
} = require('./handlers/deals');

const {
	commentOnDeal,
	deleteCommentOnDeal,
	updateCommentOnDeal,
	blockCommentOnDeal
} = require('./handlers/comments');

const {
	getAddressByZipcode,
	getAddressSuggestion,
	getGeocode
} = require('./handlers/addresses');

const {
	serverManualPaymentIntent,
	confirmPaymentIntent
} = require('./handlers/checkout')

const {
	updateCustomer,
	createSubscription,
	getSubscription,
	confirmSubscriptionPaymentWebhook,
	createSubcriptionDocument,
	getLiveSubscription
} = require('./handlers/subscriptions')

const {
	subcriptionDeleted,
	chargeUpdated
} = require('./handlers/webhooks')

const {
	initEmuUser,
	initDealData,
	appInitUsers
} = require('./handlers/emu')

const FBAuth = require('./util/fbauth');
const FBSystemAuth = require('./util/fbclaims');
const cors = require('cors');

app.use(cors());
//app.use(bodyParser.json());
app.use(cookieParser()); //Cookies

//rewrite api prefix
const API_PREFIX = 'api';
// Rewrite Firebase hosting requests: /api/:path => /:path
app.use((req, res, next) => {
	if (req.url.indexOf(`/${API_PREFIX}/`) === 0) {
		req.url = req.url.substring(API_PREFIX.length + 1);
	}
	next();
});

//User routes
app.post('/signup', signup2);
app.post('/login', login);
app.post('/logout', FBAuth, logout)
app.post('/user/image', FBAuth, uploadUserImage);
app.post('/user', FBAuth, editUserDetails);
app.get('/user', FBAuth, getAuthenticatedUser);
app.get('/user/:handle', getUserProfileData);
//Deal routes
app.get('/deals', getAllDeals);
app.get('/deal/:dealId', getOneDeal)
app.post('/deal', FBAuth, postDeal);
app.post('/deal/image', uploadDealImage);
app.post('/deal/edit', FBAuth, editDeal)
app.delete('/deal/:dealId', FBAuth, deleteDeal)
//Comment routes
app.post('/deal/:dealId/comment', FBAuth, commentOnDeal);
app.post('/deal/comment/update', FBAuth, updateCommentOnDeal)
app.post('/deal/comment/block', FBAuth, blockCommentOnDeal)
app.delete('/deal/comment/:commentId', FBAuth, deleteCommentOnDeal)
//Addresses
app.post('/addresses/zipcode', getAddressByZipcode);
app.get('/addresses/suggestion', getAddressSuggestion);
app.get('/addresses/geocode', getGeocode);
//System routes
app.post('/system/user/role', FBAuth, setUserRole);
app.get('/system/users', FBSystemAuth, systemGetUsers);
app.post('/system/users/verify', FBAuth, postVerifacation);
app.post('/system/verification', FBAuth, uploadVerifacationFile);
app.post('/system/verification/url', FBAuth, getVerificationFile);
//Subscription routes
app.post("/charge", serverManualPaymentIntent);
app.post("/charge/confirm", confirmPaymentIntent);
app.post("/charge/subscription", FBAuth, createSubscription);
app.post("/charge/subscription/confirm/webhook", bodyParser.raw({
	type: 'application/json'
}), confirmSubscriptionPaymentWebhook);
app.post("/subscription/create/document", FBAuth, createSubcriptionDocument)
app.post("/subscription/update/live", FBSystemAuth, getLiveSubscription)
app.post("/subscription", FBSystemAuth, getSubscription)
app.post("/subscription/update/customer", FBAuth, updateCustomer)
//Webhooks
app.post("/webhook/subscription/deleted", bodyParser.raw({
	type: 'application/json'
}), subcriptionDeleted)
app.post("/webhook/charge/updated", bodyParser.raw({
	type: 'application/json'
}), chargeUpdated)
//Test
app.post("/emu", initEmuUser)
app.post("/emu/deals", initDealData)
app.post("/emu/init/users", appInitUsers)

exports.api = functions.https.onRequest(app);

exports.onUserImageChange = functions
	.firestore.document('users/{userId}')
	.onUpdate(change => {
		//console.log(change.before.data());
		//console.log(change.after.data());

		if (change.before.data().imageUrl !== change.after.data().imageUrl) {
			//console.log('image has changed');
			const batch = db.batch();
			return db.collection('deals')
				.where('userId', '==', change.before.data().userId).get()
				.then(data => {
					data.forEach(doc => {
						const deal = db.doc(`/deals/${doc.id}`);
						batch.update(deal, {
							userImage: change.after.data().imageUrl
						});
					})
					return batch.commit();
				}).then(() => {
					if (change.before.data().imageLocation) {
						let oldImage = {
							location: change.before.data().imageLocation,
							fileName: change.before.data().imageFileName,
						}
						return admin.storage().bucket().file(`${oldImage.location}/${oldImage.fileName}`).delete()
					}
				}).then(() => {
					console.log(`File ${change.before.data().imageFileName} deleted `)
				})
		} else return true;

	})

exports.onUserVerificationChange = functions
	.firestore.document('users/{userId}')
	.onUpdate(change => {
		//console.log(change.before.data());
		//console.log(change.after.data());

		if (change.before.data().verified !== change.after.data().verified) {
			//console.log('image has changed');
			const batch = db.batch();
			return db.collection('deals')
				.where('userId', '==', change.before.data().userId).get()
				.then(data => {
					data.forEach(doc => {
						const deal = db.doc(`/deals/${doc.id}`);
						batch.update(deal, {
							userVerified: change.after.data().verified
						});
					})
					return batch.commit();
				})
		} else return true;
	})

exports.onUserTypeChange = functions
	.firestore.document('users/{userId}')
	.onUpdate(change => {
		//console.log(change.before.data());
		//console.log(change.after.data());

		if (change.before.data().type !== change.after.data().type) {
			//console.log('image has changed');
			const batch = db.batch();
			return db.collection('deals')
				.where('userId', '==', change.before.data().userId).get()
				.then(data => {
					data.forEach(doc => {
						const deal = db.doc(`/deals/${doc.id}`);
						batch.update(deal, {
							userType: change.after.data().type
						});
					})
					return batch.commit();
				})
		} else return true;
	})

exports.onVerificationDocumentDelete = functions
	.firestore.document('verifications/{verificationId}')
	.onDelete((snap, context) => {
		const {
			verificationId
		} = context.params;
		const bucket = admin.storage().bucket();

		return bucket.deleteFiles({
			prefix: `files/verification/${verificationId}`
		});
	})


exports.onDealDelete = functions
	//firebase deploy --only functions:onDealDelete
	.firestore.document('deals/{dealId}')
	.onDelete((snapshot, context) => {

		const {
			dealId
		} = context.params;

		const bucket = admin.storage().bucket();

		let address = db.doc(`addresses/${dealId}`)
		return address
			.get()
			.then(doc => {
				if (doc.exists) {
					address.delete()
						.catch(err => console.error(err));
				}
			})
			.then(() => {
				return bucket.deleteFiles({
					prefix: `images/deals/${dealId}`
				})
			})
			.catch(err => console.error(err));
	})

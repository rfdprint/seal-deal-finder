const stripe = require('stripe')('sk_test_j6uTNRLgbsxDrVHPbZFI6hHS');
const util = require('util')

exports.charge = (req, res) => {
	//console.log(req.body.data.payload.token.card.id)
	stripe.customers.create({
			email: req.body.data.email,
			source: req.body.data.payload.token.id
		})
		.then(customer =>
			stripe.paymentIntents.create({
				amount: req.body.data.amount,
				name: req.body.data.name,
				description: "Sample Charge",
				currency: "usd",
				customer: customer.id
			}))
		.then((pi) => {
			return res.json(pi.client_secret)
			/*stripe.confirmCardPayment(csKey, {
				payment_method: {
					card: req.body.data.payload.token.card.id,
					billing_details: {
						name: 'Jenny Rosen',
					},
				}
			})*/
		})
		.catch(err => {
			console.log("Error:", err);
			res.status(500).send({
				error: "Purchase Failed"
			});
		});

}



exports.serverManualPaymentIntent = (req, res) => {

	stripe.customers.create({
			email: req.body.data.email,
			source: req.body.data.payload.token.id,
			name: req.body.data.name,
		})
		.then(customer => {
			//customerId = customer.id
			stripe.paymentIntents.create({
					amount: req.body.data.amount,
					description: "Sample Charge",
					currency: "usd",
					customer: customer.id,
					confirmation_method: 'manual',
					confirm: true,
					setup_future_usage: 'off_session'
				})
				.then((intent) => {
					res.send(generateResponse(intent));
				})
		})
		.catch(err => {
			console.log("Error:", err);
			res.status(500).send({
				error: "Purchase Failed"
			});
		});
}

exports.generateResponse = (intent) => {
	// Note that if your API version is before 2019-02-11, 'requires_action'
	// appears as 'requires_source_action'.
	if (
		intent.status === 'requires_action' &&
		intent.next_action.type === 'use_stripe_sdk'
	) {
		// Tell the client to handle the action
		return {
			requires_action: true,
			payment_intent_client_secret: intent.client_secret
		};
	} else if (intent.status === 'succeeded') {
		// The payment didn’t need any additional actions and completed!
		// Handle post-payment fulfillment
		return {
			message: {
				payment: 'payment processed successfully',
				success: true
			}
		}
	} else {
		// Invalid status
		console.error(err);
		return {
			error: 'Invalid PaymentIntent status'
		}
	}
};

/*payment_method: req.body.data.payload.token.card.id,
				payment_method_types: ['card']*/


exports.confirmPaymentIntent = (req, res) => {
	stripe.paymentIntents.confirm(req.body.data.payload)
		.then((intent) => {
			res.send(generateResponse(intent));
		}).catch(err => {
			console.log("Error:", err);
		});
}

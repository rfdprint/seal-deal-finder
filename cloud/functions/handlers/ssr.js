const functions = require('firebase-functions');

//File System
const fs = require("fs");
const path = require("path");

const {
	db,
	projectId
} = require('../util/admin');

//Environment Variables
const functionsConfig = functions.config().env
const facebookKey = functionsConfig.facebook[`${projectId}`]

exports.ssrDealPage = (req, res) => {
	console.log("SSR")
	const dealId = req.params.dealId

	db.doc(`/deals/${dealId}`)
		.get()
		.then(doc => {
			let meta
			if (!doc.exists) {

				meta = `<title>Seal Deal Finder</title>
					<meta property="og:url" content="${facebookKey.seal_deal_finder_base_url}/" />
					<meta property="og:title"  content="Sealdealfinder.com Deal Not Found" />
					<meta property="og:description" content="Deal not found" />
					<meta property="og:image"  content="" />
					<meta name="ssr"  content=${true} />`;

			} else {
				deal = doc.data();
				meta = `<title>${deal.title}</title>
			<meta property="og:url" content="${facebookKey.seal_deal_finder_base_url}/deals/${deal.dealId}" />
			<meta property="og:title"  content="${deal.title}" />
			<meta property="og:description" content="${deal.description}" />
			<meta property="og:image"  content="${deal.imageUrl}" />
			<meta name="ssr"  content=${true} />
			`
			}

			//dealData.dealId = doc.id;

			const indexFile = path.resolve("./web/index.html");
			fs.readFile(indexFile, "utf8", (err, data) => {
				if (err) {
					console.error("Something went wrong:", err);
					return res.status(500).send(`Error: ${err.message}`);
				}

				return res.send(data.replace(`<meta name="%%%META%%%"/>`, meta));
			});

		})

}

const functions = require('firebase-functions');
const {
	admin,
	db,
	projectId,
	stripeConfigSK
} = require('../util/admin');

const {
	errorRreponse
} = require('../util/errors');
const {
	error
} = require('firebase-functions/lib/logger');

const functionsConfig = functions.config().env;

const stripe = require('stripe')(stripeConfigSK.key);
const price = stripeConfigSK.price;


exports.updatePaymentDetails = (req, res) => {

	let billingDetails = req.body.paymentMethod.billing_details
	let updatedAddress = billingDetails.address
	let subscription = db.collection('subscriptions').doc(req.user.uid)
	let user = db.collection('users').doc(req.user.uid)
	let previousPaymentMethod;
	let newPaymentMethod = req.body.paymentMethod.id;
	let customerId;

	const splitString = str => {
		// Splitting up to 2 terms
		var array = str.split(",");
		return array
	}
	const names = splitString(billingDetails.name)

	const updatedBillingInformation = {
		firstName: names[0].trim(),
		lastName: names[1].trim(),
		street: updatedAddress.line1,
		city: updatedAddress.city,
		state: updatedAddress.state
	}

	subscription
		.get()
		.then((doc) => {
			previousPaymentMethod = doc.data().defaultPaymentMethod
			customerId = doc.data().customerId;
		}).then(() => {
			stripe.setupIntents.create({
					payment_method: newPaymentMethod,
					customer: customerId
				})
				.then(setupIntent => {
					if (setupIntent === "succeeded") {
						stripe.paymentMethods.attach(
								newPaymentMethod, {
									customer: customerId
								})
							.then((newMethod) => {

								stripe.customers.update(customerId, {
										invoice_settings: {
											default_payment_method: newMethod.id
										}
									})
									.then(() => {

										subscription.update({
												defaultPaymentMethod: newPaymentMethod,
												previousPaymentMethod,
												billing: updatedBillingInformation
											})
											.catch((err) => {
												console.log(err)
											})
									})
									.then(() => {
										console.log(`${customerId} payment updated`)

										return res.status(200).json({
											message: {
												success: "Payment method updated successfully",
												newPaymentMethod: newPaymentMethod,
												billing: updatedBillingInformation,
												setupIntent: null
											}
										})
									})

							})
							.catch((err) => {
								console.log(err)
							})
					} else {
						let newBillingInformation = {
							defaultPaymentMethod: newPaymentMethod,
							previousPaymentMethod,
							billing: updatedBillingInformation
						}
						return res.send(generateChangeBillingResponse(setupIntent, newBillingInformation))
					}
				})
				.catch(err => {
					return res.status(400).json({
						general: err.message
					})
				})
		})
		.catch(err => {
			return res.status(400).json({
				general: err.message
			})
		})
}

exports.updateBillingDocument = (req, res) => {
	let subscription = db.collection('subscriptions').doc(req.user.uid)
	let newBillingInformation = req.body


	subscription.get()
		.then(doc => {
			if (!doc.exists) {
				return res.status(404).json({
					error: 'Deal not found'
				});
			} else {
				const newSubscription = {
					createdAt: doc.data().createdAt,
					subscriptionId: doc.data().subscriptionId,
					userId: doc.data().userId,
					customerId: doc.data().customerId,
					status: doc.data().status,
					lastestInvoiceId: doc.data().lastestInvoiceId,
					defaultPaymentMethod: newBillingInformation.defaultPaymentMethod,
					previousPaymentMethod: newBillingInformation.previousPaymentMethod,
					billing: newBillingInformation.billing
				}
				subscription.update(newSubscription)
					.then((data) => {
						console.log("billing document updated successfully")
						return res.send({
							message: {
								success: "Payment processed successfully",
								updatedSubscription: newSubscription,
							}
						})
					})
					.catch((err => {
						console.log(err)
					}))
			}
		})
}
exports.createSubscription = (req, res) => {
	let userId = req.user.uid
	let subscription = {}
	let lastestInvoice = {}

	const systemTech = {
		email: functionsConfig.users.roles[`${projectId}`].admins.emails,
		price: functionsConfig.users.roles[`${projectId}`].admins.testProductPrice
	}

	stripe.customers.create({
			metadata: {
				userId: userId
			},
			description: `UserId: ${userId}`,
			email: req.body.data.email,
			payment_method: req.body.data.token.paymentMethod.id,
			invoice_settings: {
				default_payment_method: req.body.data.token.paymentMethod.id,
			}

		})
		.then(customer => {
			stripe.subscriptions.create({
					customer: customer.id,
					metadata: {
						userId: userId
					},
					items: [{
						//plan: plan, //req.body.data.plan
						price: req.body.data.email == systemTech.email ? systemTech.price : price
					}],
					expand: ['latest_invoice', 'latest_invoice.payment_intent', 'latest_invoice.charge', 'default_payment_method'],

				})
				.then((data) => {
					subscription = data
					lastestInvoice = subscription.latest_invoice
					let intent = subscription.latest_invoice.payment_intent
					const newSubscription = {
						createdAt: subscription.created,
						subscriptionId: subscription.id,
						userId: userId,
						customerId: subscription.customer,
						status: subscription.status,
						lastestInvoiceId: lastestInvoice.id,
						defaultPaymentMethod: req.body.data.token.paymentMethod.id,
						billing: req.body.data.billing
					}

					if (intent.status === 'succeeded') {
						let subscriptionsCollection = db.collection('subscriptions')
						subscriptionsCollection
							.doc(userId)
							.set(newSubscription)
							.then(() => {
								db.doc(`/users/${userId}`).update({
										subscriptionPaid: true,
										verificationStatus: "review",
										subscriptionStatus: subscription.status
									})
									.catch(err => {
										console.log("Error:", err);
									})
							})
							.then(() => {
								let newLastestInvoice = {
									userId: userId,
									subscriptionId: subscription.id,
									createdAt: lastestInvoice.created,
									invoiceNumber: lastestInvoice.number,
									invoiceId: lastestInvoice.id,
									invoiceUrl: lastestInvoice.hosted_invoice_url,
									invoiceStatus: lastestInvoice.status,
									invoiceChargeStatus: lastestInvoice.charge.status,
									latestInvoiceChargeRefunded: lastestInvoice.charge.refunded,
									amountDue: lastestInvoice.amount_due,
									attemptCount: lastestInvoice.attempt_count
								}

								lastestInvoicesCollection = db.collection('latestInvoices')
								lastestInvoicesCollection.doc(userId)
									.set(newLastestInvoice)
									.catch(err => {
										console.log(err)
									})
								return newLastestInvoice;
							})
							.then((newLastestInvoice) => {
								return res.send(generateSubscriptionResponse(intent, newSubscription, newLastestInvoice));
							})
							.catch(err => {
								console.log("Error:", err);
							})
					} else {
						return res.send(generateSubscriptionResponse(intent, newSubscription));
					}
				})
				.catch(err => {
					console.log("Error1:", err.code);
					console.log("Error1:", err.message);
					return res.status(400).json({
						general: "Your card could not be processed for reasons unknown"
					})
				})
		})
		.catch(err => {
			console.log("Error2:", err.code);
			console.log("Error2:", err.message);
			let errMessage;
			if (err.message.includes("Stripe")) {
				errMessage = "An error occurred with our server connection."
			} else {
				errMessage = err.message
			}
			return res.status(400).json({
				general: errMessage
			})

		});
}

exports.createSubcriptionDocument = (req, res) => {
	console.log("function: createSubcriptionDocument")
	//Payment intent succeeded
	let newSubscription = req.body
	db.collection('subscriptions')
		.doc(req.user.uid)
		.set(newSubscription)
		.then(() => {
			db.doc(`/users/${req.user.uid}`).update({
				subscriptionPaid: true,
				verificationStatus: "review",
				subscriptionStatus: "active"
			})
		})
		.then(() => {
			stripe.invoices.retrieve(newSubscription.lastestInvoiceId, {
					expand: ['charge']
				})
				.then((data) => {
					let newLastestInvoice = {
						userId: req.user.uid,
						subscriptionId: newSubscription.subscriptionId,
						createdAt: data.created,
						invoiceNumber: data.number,
						invoiceId: data.id,
						invoiceUrl: data.hosted_invoice_url,
						invoiceStatus: data.status,
						invoiceChargeStatus: data.charge.status,
						latestInvoiceChargeRefunded: data.charge.refunded,
						amountDue: data.amount_due,
						attemptCount: data.attempt_count
					}

					db.collection('latestInvoices')
						.doc(req.user.uid)
						.set(newLastestInvoice)
						.then(() => {
							console.log("subcription document created successfully")
							return res.send({
								message: {
									success: "Payment processed successfully",
									subscription: newSubscription,
									invoice: newLastestInvoice
								}
							})
						})
						.catch((err) => {
							console.log(err)
						})
				})
				.catch((err => {
					console.log(err)
				}))
		})
		.catch(err => {
			console.log("Error:", err);
		})

}

exports.cancelSubscription = (req, res) => {
	let userHandle = req.user.handle;
	let confirmedHandle = req.body.confirmedHandle
	let subscription = db.collection('subscriptions').doc(req.user.uid)
	let user = db.collection('users').doc(req.user.uid)
	subscription
		.get()
		.then((doc) => {
			if (doc.exists) {

				if (userHandle !== confirmedHandle) {
					return res.status(500).json({
						error: {
							alert: {
								title: "Subscriptions Cancel Error",
								text: "The user name you entered does not match the user name on record"
							}
						}
					})
				}
				stripe.subscriptions.del(
						doc.data().subscriptionId
					).then(() => {
						let updatedUser;
						user.update({
								subscriptionStatus: "canceled",
								subscriptionPaid: false,
								verified: false
							})
							.then(

								subscription.update({
									status: "canceled"
								})
								.catch(err => {
									console.log(error)
								})
							)
							.catch((err) => {
								console.log(err)
							})

						const updatedSubscription = {
							...doc.data(),
							status: "canceled"
						}
						return res.send({
							message: {
								success: "Subscription cancelled successfully",
								subscription: updatedSubscription,
								profileUpdates: {
									subscriptionStatus: "canceled",
									subscriptionPaid: false,
									verified: false
								}
							}
						})
					})
					.catch((err) => {
						console.log(err)
						return res.send({
							message: {
								error: "An error occured",
							}
						})
					})
			} else {
				return res.status(400).json({
					error: {
						alert: {
							title: "Subscriptions Cancel Error",
							text: "A subscription could not be found for your account"
						}
					}
				})
			}
		})
}

exports.confirmSubscriptionPayment3D = (req, res) => {

	const paymentIntentId = req.body.data

	stripe.paymentIntents.confirm(
		paymentIntentId, {
			return_url: 'http://localhost:3000/subscriptions/success',
		},
	).catch((err) => {
		console.log(err)
	})

}

const generateSubscriptionResponse = (intent, newSubscription, newLastestInvoice) => {
	console.log("function: generateSubscriptionResponse")
	if (
		intent.status === 'requires_action' &&
		intent.next_action.type === 'use_stripe_sdk'
	) {
		return {
			requires_action: true,
			payment_intent_client_secret: intent.client_secret,
			intentId: intent.id,
			status: intent.status,
			newSubscription: newSubscription
		};
	} else if (intent.status === 'succeeded') {

		console.log("function: generateSubscriptionResponse: success")
		return {
			message: {
				success: "Payment processed successfully!",
				subscription: newSubscription,
				latestInvoice: newLastestInvoice,
				subscriptionPaid: true
			}
		}
	} else {
		// Invalid status
		console.error(err);
		return res.status(500).json({
			error: {
				alert: errorRreponse(err.code)
			}
		})
	}
};

const generateChangeBillingResponse = (intent, newBillingInformation) => {
	console.log("function: generateChangeBillingResponse")
	if (intent.status === 'requires_confirmation') {
		return {
			message: {
				setupIntent: {
					requires_action: true,
					setup_intent_client_secret: intent.client_secret,
					intentId: intent.id,
					status: intent.status,
					newBillingInformation: newBillingInformation
				}
			}
		}
	} else if (intent.status === 'succeeded') {

		console.log("function: generateChangeBillingResponse: success")
		return {
			message: {
				success: "Payment processed successfully!",
			}
		}
	} else {
		// Invalid status
		console.error(err);
		return res.status(500).json({
			error: {
				alert: errorRreponse(err.code)
			}
		})
	}
};

exports.testStripe = (req, res) => {
	stripe.customers.retrieve(
		'cus_GjfpbpbHblhgHW',
		function(err, customer) {
			// asynchronously called
		}
	);
}

exports.confirmSubscriptionPaymentWebhook = (req, res) => {
	console.log("Hook Triggered")

	const endpointSecret = 'whsec_pkmCbw6fcHg9XH8m8DoP5j5YHYwvxQTG';
	const sig = req.headers['stripe-signature'];
	//console.log(sig)
	//console.log(req.headers)
	let event

	try {
		event = stripe.webhooks.constructEvent(req.rawBody, sig, endpointSecret);
	} catch (err) {
		res.status(400).send(`Webhook Error: ${err.message}`);
	}

	// Handle the event


	switch (event.type) {
		case 'payment_intent.succeeded':
			const paymentIntent = event.data.object;
			// Then define and call a method to handle the successful payment intent.
			// handlePaymentIntentSucceeded(paymentIntent);
			console.log("the payment intent succeeded")
			return res.json({
				message: {
					success: "Payment processed successfully",
					subscription: null
				}
			})
			break;
		case 'payment_method.attached':
			const paymentMethod = event.data.object;
			// Then define and call a method to handle the successful attachment of a PaymentMethod.
			// handlePaymentMethodAttached(paymentMethod);
			console.log("payment_method.attached")
			break;
			// ... handle other event types
		default:
			// Unexpected event type
			return res.status(400).end();
	}

	// Return a response to acknowledge receipt of the event
	res.json({
		received: true
	})

}


exports.getSubscription = (req, res) => {
	let subscription = {};
	db.doc(`/subscriptions/${req.user.uid}`)
		.get()
		.then(doc => {
			if (doc.exists) {
				subscription = doc.data()
				db.doc(`latestInvoices/${req.user.uid}`)
					.get()
					.then(invoice => {
						if (invoice.exists) {
							latestInvoice = invoice.data()
							return res.json({
								subscription,
								latestInvoice
							})
						} else {
							console.log("latest invoices not found")
						}
					})
					.catch(err => {
						console.log(err)
						return res.status(500).json({
							error: {
								alert: errorRreponse(err.code)
							}
						})
					})
			} else {
				console.log("subscription not found")
				return res.status(404).json({
					error: {
						alert: {
							title: "Subscription Not Found",
							text: `a subscription for your account was not found. After you verify your account this information will become available`
						}
					}

				})
			}
		}).catch(err => {
			console.log(err)
			return res.status(500).json({
				error: {
					alert: errorRreponse(err.code)
				}
			})
		})
}

exports.getSubscriptionSystem = (req, res) => {
	let subscription = {};
	db.doc(`/subscriptions/${req.body.userId}`)
		.get()
		.then(doc => {
			if (doc.exists) {
				subscription = doc.data()
				db.doc(`latestInvoices/${req.body.userId}`)
					.get()
					.then(invoice => {
						if (invoice.exists) {
							latestInvoice = invoice.data()
							return res.json({
								subscription,
								latestInvoice
							})
						} else {
							console.log("latest invoices not found")
						}
					})
					.catch(err => {
						console.log(err)
						return res.status(500).json({
							error: {
								alert: errorRreponse(err.code)
							}
						})
					})
			} else {
				console.log("subscription not found")
				return res.status(404).json({
					error: {
						alert: {
							title: "Not Found",
							text: `New user created successfully: ${newUser.handle}`
						}
					}

				})
			}
		}).catch(err => {
			console.log(err)
			return res.status(500).json({
				error: "an unknown error occurred"
			})
		})
}

exports.getLiveSubscription = (req, res) => {
	let subscription = {};
	let latestInvoice = {};
	let reducedSubscriptionObject = {}
	let reducedlatestInvoiceDetails = {}

	db.doc(`/subscriptions/${req.body.userId}`)
		.get()
		.then(doc => {
			if (doc.exists) {
				let subscriptionId = doc.data().subscriptionId
				stripe.subscriptions.retrieve(subscriptionId, {
						expand: ['latest_invoice', 'latest_invoice.charge']
					})
					.then(data => {
						//console.log(data)
						reducedSubscriptionObject = {
							createdAt: data.created,
							subscriptionId: data.id,
							userId: req.body.userId,
							customerId: data.customer,
							status: data.status,
							lastestInvoiceId: data.latest_invoice.id
						}
						reducedlatestInvoiceDetails = {
							userId: req.body.userId,
							subscriptionId: data.id,
							createdAt: data.latest_invoice.created,
							invoiceNumber: data.latest_invoice.number,
							invoiceId: data.latest_invoice.id,
							invoiceUrl: data.latest_invoice.hosted_invoice_url,
							invoiceStatus: data.latest_invoice.status,
							invoiceChargeStatus: data.latest_invoice.charge.status,
							latestInvoiceChargeRefunded: data.latest_invoice.charge.refunded,
							amountDue: data.latest_invoice.amount_due,
							attemptCount: data.latest_invoice.attempt_count,
							chargeId: data.latest_invoice.charge.id
						}

						subscription = reducedSubscriptionObject
						latestInvoice = reducedlatestInvoiceDetails

						updateDocument("users", req.body.userId, {
							subscriptionStatus: data.status
						})
						updateDocument("subscriptions", req.body.userId, subscription)
						updateDocument("latestInvoices", req.body.userId, latestInvoice)
					})
					.then(() => {
						return res.json({
							subscription,
							latestInvoice
						})
					})
					.catch(err => {
						console.log(err)
						return res.status(500).json({
							error: err.message
						})
					})
			} else {
				console.log("subscription not found")
				return res.status(400).json({
					error: "subscription not found"
				})
			}
		})
		.catch(err => {
			console.log(err)
			return res.status(500).json({
				error: "an unknown error occurred"
			})
		})
}

const updateDocument = (collection, doc, data) => {
	db.doc(`/${collection}/${doc}`).update(
			data
		)
		.then(() => {
			console.log(`${collection} updated`)
		})
		.catch(err => {
			console.log(err)
		})
}


const getLastestInvoice = (lastestInvoiceId, userId, subscriptionId) => {
	console.log("retrieving lasted invoice")
	stripe.invoices.retrieve(lastestInvoiceId, {
			expand: ['charge']
		})
		.then((data) => {
			let newLastestInvoice = {
				userId,
				subscriptionId: subscriptionId,
				createdAt: data.created,
				invoiceNumber: data.number,
				invoiceId: data.id,
				invoiceUrl: data.hosted_invoice_url,
				invoiceStatus: data.status,
				invoiceChargeStatus: data.charge.status,
				latestInvoiceChargeRefunded: data.charge.refunded,
				amountDue: data.amount_due,
				attemptCount: data.attempt_count
			}
			return newLastestInvoice
		})
		.catch(() => {
			console.log(err => {
				console.log(err)
			})
		})
}

exports.getPaymentMethod = (req, res) => {
	paymentMethod = req.body.paymentMethod
	stripe.paymentMethods.retrieve(paymentMethod)
		.then((currentPaymentMethod) => {
			return res.status(200).json(
				currentPaymentMethod
			)
		})
		.catch((err => {
			console.log(err)
		}))
}

const createSetupIntent = (paymentMethod, customerId) => {
	console.log("createSetupIntent")
	stripe.setupIntents.create({
			payment_method: paymentMethod,
			customer: customerId,
			confirm: true
		})
		.then(setupIntent => {
			return setupIntent.id
		})
		.catch(err => {
			console.error(err)
			return null;
		})
}

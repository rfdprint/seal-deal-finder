const functions = require('firebase-functions');
const fs = require('fs')
var jwt = require('jsonwebtoken');
const {
	admin,
	db,
	config,
	projectId
} = require('../util/admin');

const functionsConfig = functions.config().env
const privateKEY = functionsConfig.emu.key
const devEnv = process.env.FUNCTIONS_EMULATOR;

exports.initEmuUser = (req, res) => {
	if (devEnv || projectId === functionsConfig.projectId.dev) {
		let token = req.headers.authorization
		let users = req.body


		users.map(user => {
			//console.log(user)
			let claims = {
				admin: user.admin
			}
			if (user.admin) {
				admin.auth().setCustomUserClaims(user.userId, claims)
					.catch(err => {
						console.log(err.message)
					})
			} else {
				admin.auth().setCustomUserClaims(user.userId, claims)
					.catch(err => {
						console.log(err.message)
					})
			}
			let userRecord = db.collection('users').doc(user.userId)
			userRecord
				.get()
				.then((doc) => {
					if (doc.exists) {
						userRecord.update(user)
							.then(() => {

								console.log(user)
							})
							.catch(err => {
								console.log(err)
							})
					} else {
						userRecord.set(user)
							.then(() => {
								console.log(user)
							})
							.catch(err => {
								console.log(err)
							})
					}
				})
		})

		return res.status(200).json(users)
	}

}

exports.initDealData = (req, res) => {
	if (devEnv || projectId === functionsConfig.projectId.dev) {
		console.log("Geting emu data")
		let deals;
		let token = req.headers.authorization

		deals = req.body

		const dealsCollection = db.collection('deals');

		deals.forEach(deal => {
			dealsCollection
				.where("dealId", "==", deal.dealId)
				.get()
				.then(doc => {
					if (!doc.exists) {
						dealsCollection.doc(deal.dealId).set(deal)
						console.log({
							message: {
								success: `Deal ${deal.dealId} created`
							}
						})
					} else {
						dealsCollection.doc(deal.dealId).update(deal)
						console.log({
							message: {
								success: `Deal ${deal.dealId} updated`
							}
						})

					}
				})
				.then(() => {
					let pushedDeals = []
					dealsCollection.get()
						.then(snapshot => {
							snapshot.docs.map(doc => {
								pushedDeals.push(doc.data())
							})
						})
						.then(() => {
							return res.json(pushedDeals);
						})
						.catch(err => {
							console.log(err.message)
						})
				})
				.catch(err => {
					console.log(err)
				})
		})
	} else {
		res.status(403).send({
			error: "(cus:emu initDealData) request cannot be made in production"
		});
	}

}
exports.appInitUsers = (req, res) => {

	let email = req.body.email
	let claims = req.body.claims

	let token = req.headers.authorization

	const splitString = str => {
		// Splitting up to 2 terms
		var array = str.split(",");
		return array
	}

	jwt.verify(token, privateKEY, {
		algorithms: ['HS256']
	}, function(err, payload) {
		if (err) {
			console.log(err.message)
			return res.json(`token error:  ${err.message}`)
		} else {
			db.collection('users')
				.where("email", "==", email)
				.get()
				.then(snapshot => {
					snapshot.forEach(doc => {
						if (!doc.empty) {
							userData = doc.data()
							admin.auth().setCustomUserClaims(userData.userId, claims)
								.catch(err => {
									console.log(err)
								})

							const userCollection = db.collection('users').doc(userData.userId);
							userCollection.update({
									roles: claims
								})
								.then(() => {
									let userUpdated = {
										message: "user updated",
										userId: userData.userId,
										userEmail: userData.email,
										claimsAdmin: claims.admin
									}
									console.log(userUpdated)
									return res.json(userUpdated)

								})
								.catch(err => {
									console.log(err)
									errMsg = `could not update role for ${userData.email}`
									return res.json(errMsg)

								})
						}
					})
				})
				.catch((err) => {
					console.log(err)
					errors.push(`could not find user ${user.email}`)
				})

		}
	})
}

exports.appInitUsersTest = (req, res) => {
	let responseErrors = [];
	let responseMessage = [];
	let token = req.headers.authorization

	const splitString = str => {
		// Splitting up to 2 terms
		var array = str.split(",");
		return array
	}

	jwt.verify(token, privateKEY, {
		algorithms: ['HS256']
	}, function(err, payload) {
		if (err) {
			console.log(err.message)
			return res.json(`token error:  ${err.message}`)
		} else {

			let userEmails = [];

			if (req.body.role === "admin") {
				projectId = config.projectId
				rawEmailsString = functionsConfig.users.roles[`${projectId}`].admins.emails
				userEmails = splitString(rawEmailsString)
			}


			let itemsProcessed = 0

			userEmails.forEach(email => {

				db.collection('users')
					.where("email", "==", email)
					.get()
					.then(snapshot => {
						console.log(email)
						snapshot.forEach(doc => {
							if (!doc.empty) {
								userData = doc.data()
								let claims = {
									admin: true
								}
								let roles = {
									admin: true
								}

								admin.auth().setCustomUserClaims(userData.userId, claims)
									.catch(err => {
										console.log(err)
										errMsg = `could not update role for ${userData.email}`
										responseErrors.push(errMsg)
									})

								const userCollection = db.collection('users').doc(userData.userId);

								userCollection.update({
										roles
									})
									.then(() => {
										let userUpdated = {
											message: "user updated",
											userId: userData.userId,
											userEmail: userData.email,
											claimsAdmin: claims.admin,
											rolesAdmin: roles.admin
										}
										responseMessage.push(userUpdated)

									})
									.catch(err => {
										console.log(err)
										errMsg = `could not update role for ${userData.email}`
										responseErrors.push(errMsg)

										itemsProcessed++;
										if (itemsProcessed === userEmails.length) {
											console.log({
												msg: responseMessage,
												errors: responseErrors
											})
											return res.json({
												message: responseMessage,
												error: responseErrors
											})
										}
									})
							}
						})

					})
					.catch((err) => {
						console.log(err)
						errors.push(`could not find user ${user.email}`)
					})

			})
		}

	})
}


exports.emuEnv = (req, res) => {
	console.log("Initializing App")
	let token = req.headers.authorization
	jwt.verify(token, privateKEY, {
		algorithms: ['HS256']
	}, function(err, payload) {
		if (err) {
			console.log(err.message)
			return res.json({
				error: err.message
			})
		} else {
			return db.collection('users')
				.get()
				.catch(err => {
					console.log(err)
					return res.json({
						error: err.message
					})
				})
		}
	})
}

exports.emuTest = (req, res) => {
	console.log("Initializing App")
	let token = req.headers.authorization
	jwt.verify(token, privateKEY, {
		algorithms: ['HS256']
	}, function(err, payload) {
		if (err) {
			console.log(err.message)
			return res.json({
				error: err.message
			})
		} else {
			return res.json({
				env: process.env,
				envJson: functionsConfig
			})
		}
	})
}

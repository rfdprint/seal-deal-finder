const firebase = require('firebase');
const config = require('../util/config/fbConfig');
firebase.initializeApp(config);

const {
	admin,
	db
} = require('../util/admin');

const emu = process.env.FUNCTIONS_EMULATOR;

const {
	validateLoginData,
} = require('../util/validators');


exports.login = (req, res) => {

	const user = {
		email: "emu@example.com",
		password: "123456"
	}
	console.log(user)
	const {
		valid,
		errors
	} = validateLoginData(user);
	if (!valid) return res.status(400).json(errors);

	// As httpOnly cookies are to be used, do not persist any state client side.
	//firebase.auth().setPersistence(firebase.auth.Auth.Persistence.NONE);

	firebase.auth().signInWithEmailAndPassword(user.email, user.password)
		.then(data => {
			//console.log(data)
			return data.user.getIdToken();
		})
		.then(token => {
			//console.log(token)
			// Set session expiration to 5 days.
			//5 Days 60 * 60 * 24 * 5 * 1000;
			// 5 Mins 0 * 5 * 1000;
			const expiresIn = 60 * 60 * 24 * 14 * 1000;
			// Create the session cookie. This will also verify the ID token in the process.
			// The session cookie will have the same claims as the ID token.
			// To only allow session cookie setting on recent sign-in, auth_time in ID token
			// can be checked to ensure user was recently signed in before creating a session cookie.
			admin.auth().createSessionCookie(token, {
					expiresIn
				})
				.then((sessionCookie) => {
					// Set cookie policy for session cookie.
					const options = {
						maxAge: expiresIn,
						httpOnly: true,
						secure: false
					};
					//res.setHeader('Cache-Control', 'private');
					console.log(sessionCookie)
					console.log('session created successfully')
					res.cookie('__session', sessionCookie, options);
					res.end(JSON.stringify({
						status: 'success'
					}));

				})
				.catch(() => {
					res.status(401).send('UNAUTHORIZED REQUEST!');
				});
			/*return res.json({
				token
			})*/
		})
		.catch(err => {
			console.error(err)
			return res.status(403).json({
				general: "Wrong credentials, please try again"
			})
		})
}

exports.logout = (req, res) => {
	res.clearCookie('__session')
	res.end(JSON.stringify({
		status: 'logged out'
	}));
}



//Get any user's details
exports.getUserProfileData = (req, res) => {
	let userData = {};
	let userId;

	db.collection('users').where('handle', "==", req.params.handle)
		.get()
		.then(snapshot => {
			snapshot.forEach(doc => {
				userId = doc.data().userId
			})

			db.doc(`/users/${userId}`).get()
				.then(doc => {
					if (doc.exists) {
						//console.log(doc)
						userData.user = {};
						//userData.user = doc.data();
						userData.user.dealId = doc.id
						userData.user.handle = doc.data().handle
						userData.user.type = doc.data().type
						userData.user.commentCount = doc.data().commentCount
						userData.user.likeCount = doc.data().likeCount
						userData.user.userImage = doc.data().userImage
						userData.user.imageUrl = doc.data().imageUrl
						userData.user.signedUrl = doc.data().signedUrl
						userData.user.verified = doc.data().verified

						db.collection('deals')
							.where('userId', '==', userId)
							.orderBy('createdAt', 'desc')
							.get()
							.then(data => {
								userData.deals = [];
								data.forEach(doc => {
									userData.deals.push({
										dealId: doc.id,
										title: doc.data().title,
										description: doc.data().description,
										city: doc.data().city,
										state: doc.data().state,
										price: doc.data().price,
										arv: doc.data().arv,
										rehab: doc.data().rehab,
										assignmentFee: doc.data().assignmentFee,
										tags: doc.data().tags,
										createdAt: doc.data().createdAt,
										userHandle: doc.data().userHandle,
										commentCount: doc.data().commentCount,
										likeCount: doc.data().likeCount,
										userImage: doc.data().userImage,
										imageUrl: doc.data().imageUrl,
										signedUrl: doc.data().signedUrl,
										userType: doc.data().userType,
										userVerified: doc.data().userVerified
									})
								})

								//console.log(userData.user)
								return res.json({
									profile: {
										user: userData.user,
										deals: userData.deals
									}
								})

							})
							.catch(err => {
								console.log(err)
							})
					} else {
						return res.status(404).json({
							error: "user cannot be found"
						});
					}
				})
				.catch(err => {
					console.log(err)
				})
		})
		.catch(err => {
			console.log(err)
		})
}


// Get own user details
exports.getAuthenticatedUser = (req, res) => {

	let userData = {};
	db.doc(`/users/${"I6pyQGzOwoT9BpiOWrMWGcpyREo1"}`)
		.get()
		.then((doc) => {
			if (doc.exists) {
				userData.credentials = doc.data();
				return db
					.collection('likes')
					.where('userId', '==', "I6pyQGzOwoT9BpiOWrMWGcpyREo1")
					.get()
					.catch(err => {
						console.log(err)
					});
			}
		})
		.then(() => {
			if (emu) {
				userData.emu = true
			}

			return res.json(userData);
		})
		.catch((err) => {
			console.error(err);
			return res.status(500).json({
				error: err.code
			});
		});
};

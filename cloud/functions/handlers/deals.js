const functions = require('firebase-functions');
const {
	admin,
	db,
	config,
	devEnv
} = require('../util/admin');

const {
	deleteImages
} = require("../util/utilFunctions")

const {
	filterObjectkeys
} = require("../util/utilFunctions")

const BusBoy = require('busboy');
const path = require('path');
const os = require('os');
const fs = require('fs');

const {
	validateDealData
} = require('../util/validators');


const Jimp = require('jimp');

//Post one deal
exports.postDeal = (req, res) => {

	const busboy = new BusBoy({
		headers: req.headers
	})

	let imageFileName;
	let imageToBeUploaded = {};
	let newDeal = {};
	let addressFields = {};

	const newId = db.collection('deals').doc().id

	busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
		if (mimetype !== 'image/jpeg' && mimetype !== 'image/png') {
			return res.status(400).json({
				error: 'Wrong file type submitted'
			})
		}

		const imageExtension = filename.split('.')[filename.split('.').length - 1];
		imageFileName = `${newId}.${imageExtension}`;
		//imageFileName = `${Math.round(Math.random() * 1000000000000)}.${imageExtension}`;
		const filepath = path.join(os.tmpdir(), imageFileName);

		imageToBeUploaded = {
			filepath,
			mimetype
		}
		file.pipe(fs.createWriteStream(filepath))
	});

	//Fields
	let formData = new Map();
	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
		formData.set(fieldname, val);
	});


	busboy.on('finish', () => {
		addressFieldsString = formData.get('address')
		newDealString = formData.get('deal')

		newDeal = JSON.parse(newDealString)
		addressFields = JSON.parse(addressFieldsString)
		console.log(addressFields)

		let dateNow = new Date().toISOString();

		if (!req.user.type) {
			userType = ""
		} else {
			userType = req.user.type
		}

		newDeal.dealId = newId;
		//newDeal.city = addressFields.city
		//newDeal.state = addressFields.state
		newDeal.userHandle = req.user.handle
		newDeal.userId = req.user.uid
		newDeal.userImage = req.user.imageUrl
		newDeal.userType = userType
		newDeal.userVerified = req.user.verified
		newDeal.createdAt = dateNow
		newDeal.likeCount = 0;
		newDeal.commentCount = 0;
		newDeal.comments = [];
		newDeal.lastUpdated = null;
		newDeal.updated = false;
		newDeal.imageVersion = 1;

		const {
			valid,
			errors
		} = validateDealData(newDeal);
		//console.log(valid, errors)

		if (!valid) return res.status(400).json(errors);

		if (!imageToBeUploaded.filepath) {
			errors.general = 'image required';
			return res.status(400).json(errors);
		}

		async function resize() {
			// Read the image.
			console.log('Resizing Image')
			const image = await Jimp.read(imageToBeUploaded.filepath);
			// Resize the image to width 150 and heigth 150.
			await image.resize(565.312, 300);
			// Save and overwrite the image
			await image.writeAsync(imageToBeUploaded.filepath);
		}
		//resize();

		const imageMainFolder = 'images'
		let imageSubFolder = 'deals'
		const imageVersion = newDeal.imageVersion;

		if (devEnv) {
			imageSubFolder = `${imageSubFolder}_emu`
		}

		const ImageDestFolder = `${imageMainFolder}/${imageSubFolder}`
		const imageFilePath = `${ImageDestFolder}/${newId}/v${imageVersion}/${imageFileName}`

		admin.storage().bucket().upload(imageToBeUploaded.filepath, {
				resumable: false,
				destination: `${ImageDestFolder}/${newId}/v${imageVersion}/${imageFileName}`,
				metadata: {
					metadata: {
						contentType: imageToBeUploaded.mimetype
					}
				}
			})
			.then(() => {
				//https://firebasestorage.googleapis.com/v0/b/seal-deal-finder-app.appspot.com/o/images%2Fdeals%2FHkrre2c6lyhx3jKMrgbr.png?alt=media
				const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageMainFolder}%2F${imageSubFolder}%2F${newId}%2Fv${imageVersion}%2F${imageFileName}?alt=media`

				newDeal.imageUrl = imageUrl
				newDeal.imageFilePath = imageFilePath

				file = admin.storage().bucket().file(imageFilePath)

				file.getSignedUrl({
					action: 'read',
					expires: '01-01-2100'
				}).then(signedUrls => {
					// signedUrls[0] contains the file's public URL
					newDeal.signedUrl = signedUrls[0]
					//console.log(signedUrls[0])
				}).catch((err => console.log(err)))

			})
			.then(() => {
				db.collection('deals')
					.doc(newId)
					.set(newDeal)
					.then(() => {

						addressFields.dealId = newId
						addressFields.userId = newDeal.userId
						return db.collection('addresses')
							.doc(newId)
							.set(addressFields)
							.catch(err => {
								res.status(500).json({
									error: "Unable to create document",
									errCode: err.code
								});
							});
					})
					.then(() => {
						//incude address fields in response
						newDeal.city = addressFields.city
						newDeal.state = addressFields.state
						newDeal.street = addressFields.street

						res.json({
							deal: newDeal,
							message: {
								success: `Deal: ${newId} saved successfully`,
								dealId: newId
							}
						})
					})
					.catch(err => {
						console.log(err)
						res.status(500).json({
							error: "Unable to create deal",
							errCode: err.code
						});
					})

			})
			.catch(err => {
				console.log(err)
				res.status(500).json({
					error: "Unable to create deal",
					errCode: err.code
				});
			});
	})

	busboy.end(req.rawBody);
}



//Edit deal
exports.editDeal = (req, res) => {

	console.log("edit deal on server launched")
	const busboy = new BusBoy({
		headers: req.headers
	})

	let imageFileName;
	let imageToBeUploaded = {};
	let newDeal = {};
	let addressFields = {};
	let imageExtension;
	busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
		if (mimetype !== 'image/jpeg' && mimetype !== 'image/png') {
			return res.status(400).json({
				error: 'Wrong file type submitted'
			})
		}

		imageExtension = filename.split('.')[filename.split('.').length - 1];

		//imageFileName = `${newDealString.dealId}.${imageExtension}`;
		imageFileName = `${Math.round(Math.random() * 1000000000000)}.${imageExtension}`;
		const filepath = path.join(os.tmpdir(), imageFileName);

		imageToBeUploaded = {
			filepath,
			mimetype
		}
		file.pipe(fs.createWriteStream(filepath))
	});

	//Fields
	let formData = new Map();
	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
		formData.set(fieldname, val);
	});

	busboy.on('finish', () => {
		addressFieldsString = formData.get('address')
		newDealString = formData.get('deal')

		newDeal = JSON.parse(newDealString)
		addressFields = JSON.parse(addressFieldsString)
		console.log(addressFields)
		newDeal.lastUpdated = new Date().toISOString();
		newDeal.updated = true;



		const {
			valid,
			errors
		} = validateDealData(newDeal);

		if (newDeal.userId !== req.user.uid) {
			errors.general = 'unauthorized: can only edit deals you created'
		}

		if (!valid) return res.status(400).json(errors);

		//rewite user data
		if (newDeal.userId === req.user.uid) {
			newDeal.userId = req.user.uid
			newDeal.userImage = req.user.imageUrl
			newDeal.userHandle = req.user.handle
		}
		if (imageToBeUploaded.filepath) {
			async function resize() {
				// Read the image.
				console.log('Resizing Image')
				const image = await Jimp.read(imageToBeUploaded.filepath);
				// Resize the image to width 150 and heigth 150.
				await image.resize(565.312, 300);
				// Save and overwrite the image
				await image.writeAsync(imageToBeUploaded.filepath);
			}
			//resize();

			newDeal.imageVersion = newDeal.imageVersion + 1

			deleteImages("deals", newDeal.dealId) //Delete all previous deal images
			imageFileName = `${newDeal.dealId}.${imageExtension}`;

			const imageMainFolder = 'images'
			let imageSubFolder = 'deals'

			console.log("Developement: " + devEnv)
			if (devEnv) {
				imageSubFolder = `${imageSubFolder}_emu`
			}

			const ImageDestFolder = `${imageMainFolder}/${imageSubFolder}`
			const imageVersion = newDeal.imageVersion

			const imageFilePath = `${ImageDestFolder}/${newDeal.dealId}/v${imageVersion}/${imageFileName}`
			admin.storage().bucket().upload(imageToBeUploaded.filepath, {
					resumable: false,
					destination: imageFilePath,
					metadata: {
						metadata: {
							contentType: imageToBeUploaded.mimetype
						}
					}
				})
				.then(() => {
					//https://firebasestorage.googleapis.com/v0/b/seal-deal-finder-app.appspot.com/o/images%2Fdeals%2FHkrre2c6lyhx3jKMrgbr.png?alt=media
					//const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageMainFolder}%2F${imageSubFolder}%2F${imageFileName}?alt=media`
					const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageMainFolder}%2F${imageSubFolder}%2F${newDeal.dealId}%2Fv${imageVersion}%2F${imageFileName}?alt=media`
					newDeal.imageUrl = imageUrl
					newDeal.imageFilePath = imageFilePath

					file = admin.storage().bucket().file(imageFilePath)

					file.getSignedUrl({
						action: 'read',
						expires: '01-01-2100'
					}).then(signedUrls => {
						// signedUrls[0] contains the file's public URL
						newDeal.signedUrl = signedUrls[0]
						//console.log(signedUrls[0])
					}).catch((err => console.log(err)))

					db.collection('deals')
						.doc(newDeal.dealId)
						.update(newDeal)
						.catch(err => {
							console.log(err)
						})

				})
				.then(() => {
					db.collection('addresses')
						.doc(newDeal.dealId)
						.update(addressFields)
						.catch(err => {
							console.log(err)
						});
				})
				.then(() => {

					//incude address fields
					newDeal.city = addressFields.city
					newDeal.state = addressFields.state
					newDeal.street = addressFields.street

					res.json({
						deal: newDeal,
						message: {
							success: `Deal: ${newDeal.dealId} updated`
						}
					})
				})
				.catch(err => {
					console.log(err)
					res.status(500).json({
						error: "Unable to create deal",
						errCode: err.code
					});
				})

		} else {
			let updatedDeal = db.collection('deals').doc(newDeal.dealId)
			updatedDeal
				.get()
				.then(doc => {
					if (doc.exists) {
						updatedDeal.update(newDeal)
					} else {
						console.log(`dealId:${newDeal.dealId} not found`)
					}
				})
				.then(() => {
					let updatedAddress = db.collection('addresses').doc(newDeal.dealId)
					updatedAddress
						.get()
						.then(doc => {
							if (doc.exists) {
								updatedAddress.update(addressFields)
								newDeal.location = addressFields
							} else {
								console.log(`no address for dealId:${newDeal.dealId} not found`)
							}
						})
						.catch(err => {
							console.log(err)
							res.status(500).json({
								error: "Unable to create document",
								errCode: err.code
							});
						});
				})
				.then(() => {

					//incude address fields
					newDeal.city = addressFields.city
					newDeal.state = addressFields.state
					newDeal.street = addressFields.street

					res.json({
						deal: newDeal,
						message: {
							success: `Deal: ${newDeal.dealId} updated`
						}
					})
				})
				.catch(err => {
					console.log(err)
					res.status(500).json({
						error: "Unable to create deal",
						errCode: err.code
					});
				})
		}
	})

	busboy.end(req.rawBody);
}


//Upload a profile image
exports.uploadDealImage = (req, res) => {
	const busboy = new BusBoy({
		headers: req.headers
	})

	let imageFileName;
	let imageToBeUploaded = {};
	let newDeal = {};

	busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
		if (mimetype !== 'image/jpeg' && mimetype !== 'image/png') {
			return res.status(400).json({
				error: 'Wrong file type submitted'
			})
		}
		//console.log(fieldname);
		//console.log(filename);
		//console.log(mimetype);

		const imageExtension = filename.split('.')[filename.split('.').length - 1];
		imageFileName = `${Math.round(Math.random() * 1000000000000)}.${imageExtension}`;
		const filepath = path.join(os.tmpdir(), imageFileName);

		imageToBeUploaded = {
			filepath,
			mimetype
		}
		file.pipe(fs.createWriteStream(filepath))
	});

	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
		//console.log('Field [' + fieldname + ']: value: ' + val);
	});

	busboy.on('finish', () => {

		admin.storage().bucket().upload(imageToBeUploaded.filepath, {
				resumable: false,
				metadata: {
					metadata: {
						contentType: imageToBeUploaded.mimetype
					}
				}
			})
			.then(() => {
				//imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageFileName}?alt=media`
				/*return db.doc(`/users/${req.user.handle}`).update({
					imageUrl
				})*/
			})
			.then(() => {
				return res.json({
					message: "Image uploaded sucessfully"
				});
			})
			.catch(err => {
				console.error(err);
				return res.status(500).json({
					error: err.code
				})
			})
	})
	busboy.end(req.rawBody);
}

exports.getOneDeal = (req, res) => {

	let deal = {};

	db.doc(`/deals/${req.params.dealId}`)
		.get()
		.then(doc => {
			if (!doc.exists) {
				return res.status(404).json({
					error: 'Deal not found'
				})
			}
			deal = doc.data();
			return db.collection('comments')
				.orderBy('createdAt', 'desc')
				.where('dealId', '==', req.params.dealId)
				.get()
				.catch(err => console.log(err))
		})
		.then(data => {
			comments = [];
			if (!data.empty) {
				data.forEach(doc => {
					if (!doc.empty) {
						console.log(doc.data())
						comments.push(doc.data())
					}
				});
			}

			return db.doc(`addresses/${req.params.dealId}`)
				.get()
				.catch(err => {
					console.log(err)
				})
		})
		.then(doc => {
			if (!doc.empty) {
				deal.city = doc.data().city ? doc.data().city : ""
				deal.state = doc.data().state ? doc.data().state : ""
				deal.street = doc.data().street ? doc.data().street : ""
			}
			return res.json({
				deal: deal,
				comments: comments,
			});
		})
		.catch(err => {
			console.error(err);
			res.status(500).json({
				error: err.code
			})
		})


}

exports.getAllDeals = (req, res) => {
	db.collection('deals')
		.orderBy('createdAt', 'desc')
		.get()
		.then(snapshot => {
			let deals = [];
			snapshot.forEach(doc => {
				if (doc.id !== 'subCollections') {
					let dealData = doc.data()
					//dealData = filterObjectkeys(dealData, ["userId"]) Removes UserId
					deals.push(dealData);
				}
			});
			//console.log(deals)
			return res.json(deals);

		})
		.catch(err => {
			console.log('(allDeals) Error getting documents', err);
		});
}

exports.getUserDeals = (req, res) => {
	db.collection('deals')
		.orderBy('createdAt', 'desc')
		.where('userId', '==', req.body.uid)
		.get()
		.then(snapshot => {
			let deals = [];
			snapshot.forEach(doc => {
				if (doc.id !== 'subCollections') {
					deals.push({
						dealId: doc.id,
						title: doc.data().title,
						description: doc.data().description,
						city: doc.data().city,
						state: doc.data().state,
						price: doc.data().price,
						arv: doc.data().arv,
						rehab: doc.data().rehab,
						assignmentFee: doc.data().assignmentFee,
						tags: doc.data().tags,
						createdAt: doc.data().createdAt,
						userHandle: doc.data().userHandle,
						commentCount: doc.data().commentCount,
						likeCount: doc.data().likeCount,
						userImage: doc.data().userImage,
						imageUrl: doc.data().imageUrl,
						signedUrl: doc.data().signedUrl
					});
				}
			});
			//console.log(deals)
			return res.json(deals);

		})
		.catch(err => {
			console.log('(userDeals) Error getting documents', err);
		});
}

//Delete an deal
exports.deleteDeal = (req, res) => {
	const deal = db.doc(`/deals/${req.params.dealId}`);
	deal.get()
		.then(doc => {
			if (!doc.exists) {
				return res.status(404).json({
					error: 'Deal not found'
				});
			}
			if (doc.data().userId !== req.user.uid) {
				return res.status(403).json({
					error: 'unauthorized action: You can only delete deals you created.'
				});
			} else {
				deal.delete()
					.catch(err => console.log(err))
			}
		})
		.then(() => {
			return res.json({
				alert: {
					title: "Deal Deleted Successfully",
					text: "Your deal has been deleted."
				}
			})
		})
		.catch((err) => {
			console.error(err);
			res.status(500).json({
				error: err.code
			});
		});
}

const {
	admin,
	db
} = require('../util/admin');



exports.commentOnDeal = (req, res) => {
	if (String(req.body.body).trim() === '') return res.status(400).json({
		comment: 'Must not be empty'
	})

	const newComment = {
		body: req.body.body,
		createdAt: new Date().toISOString(),
		dealId: req.params.dealId,
		userId: req.user.uid,
		userHandle: req.user.handle,
		userImage: req.user.imageUrl
	}

	db.doc(`/deals/${req.params.dealId}`)
		.get()
		.then(doc => {
			if (!doc.exists) {
				return res.status(404).json({
					error: 'Deal not found'
				})
			}
			return doc.ref.update({
				commentCount: doc.data().commentCount + 1
			})
		}).then(() => {
			commentId = db.collection('comments').doc().id
			newComment.commentId = commentId
			return db.collection('comments').doc(commentId).set(newComment);
		})
		.then(() => {
			res.json(newComment)
		})
		.catch(err => {
			console.error(err);
			res.status(500).json({
				errors: {
					general: 'Something went wrong'
				}
			})
		})
}

exports.blockCommentOnDeal = (req, res) => {

	const blockedComment = req.body

	db.doc(`/deals/${req.body.dealId}`)
		.get()
		.then(doc => {
			if (!doc.exists) {
				console.log('Deal not found')
				return res.status(404).json({
					error: 'Deal not found'
				})
			}
			const dealUserId = doc.data().userId;
			console.log(dealUserId)
			console.log(req.user.uid)
			if (req.user.uid === dealUserId) {
				const comment = db.doc(`/comments/${req.body.commentId}`)
				comment
					.get()
					.then(doc => {
						if (!doc.exists) {
							console.log('Comment not found')
							res.status(404).json({
								error: 'Comment not found'
							});
						} else {

							const isBlocked = doc.data().blocked
							blockedComment.blocked = !isBlocked

							comment.update(blockedComment)

							res.json(blockedComment)
						}
					})
					.catch((err) => {
						console.error(err);
						res.status(500).json({
							error: err.code
						});
					})
			} else {
				return res.status(403).json({
					error: 'Unauthorized'
				});
			}
		})
		.catch((err) => {
			console.error(err);
			res.status(500).json({
				error: err.code
			});
		})
}

exports.updateCommentOnDeal = (req, res) => {
	const comment = db.doc(`/comments/${req.body.commentId}`)
	comment
		.get()
		.then(doc => {
			if (!doc.exists) {
				return res.status(404).json({
					error: 'Comment not found'
				});
			}
			if (doc.data().userHandle !== req.user.handle) {
				return res.status(403).json({
					error: 'Unauthorized'
				});
			} else {
				comment.update(req.body)
			}
		})
		.then(() => {
			res.json(req.body)
		})
		.catch((err) => {
			console.error(err);
			res.status(500).json({
				error: err.code
			});
		})
}


exports.deleteCommentOnDeal = (req, res) => {
	console.log(req.body.commentId)
	const comment = db.doc(`/comments/${req.body.commentId}`)
	comment
		.get()
		.then(doc => {
			if (!doc.exists) {
				return res.status(404).json({
					error: 'Comment not found'
				});
			}
			if (doc.data().userHandle !== req.user.handle) {
				console.log('agreement userHandle ' + doc.data().userHandle);
				console.log('user handle ' + req.user.handle);
				return res.status(403).json({
					error: 'Unauthorized'
				});
			} else {
				comment.delete()
			}
		})
		.then((res) => {
			db.doc(`/deals/${req.body.dealId}`)
				.get()
				.then(doc => {
					if (!doc.exists) {
						return res.status(404).json({
							error: 'document connected to this comment not found'
						})
					}
					return doc.ref.update({
						commentCount: doc.data().commentCount - 1
					})
				})
		}).then(() => {
			res.json({
				message: {
					success: 'comment deleted succesfully'
				}
			})
		})
		.catch((err) => {
			console.error(err);
			res.status(500).json({
				error: err.code
			});
		})
}

exports.buildSitemap = async (req, res) => {
	// Use firebase-admin to gather necessary data
	// Build the sitemap file string
	// and send it back
	console.log("Sitemap")
	sitemapStr = `<?xml version="1.0" encoding="UTF-8"?>
			<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
			   <url>
				  <loc>http://sealdealfinder.com/</loc>
				  <lastmod>2020-11-18</lastmod>
				  <changefreq>monthly</changefreq>
				  <priority>0.8</priority>
			   </url>
			</urlset>
			`
	console.log(sitemapStr)
	res.set('Content-Type', 'text/xml');
	return res.status(200).send(sitemapStr)
}

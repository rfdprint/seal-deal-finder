const {
	admin,
	db
} = require('../util/admin');

exports.sendDirectMessage = (req, res) => {

	if (String(req.body.body).trim() === '') return res.status(400).json({
		body: 'Must not be empty'
	})

	let conversationId = combineUserIds(req.user.uid, req.body.userId)

	//Note: Adjust properties in the onUserImageChange functions if conversations properties change
	newConversation = {
		conversationId,
		firstParticipant: {
			id: req.user.uid,
			imageUrl: req.user.imageUrl,
			handle: req.user.handle
		},
		secondParticipant: {
			id: req.body.userId,
			imageUrl: req.body.imageUrl,
			handle: req.body.handle
		},
		participants: [req.user.uid, req.body.userId]
	}

	conversation = db.collection('conversations')

	conversation
		.where("conversationId", "==", conversationId)
		.get()
		.then((data) => {
			if (!data.empty) {
				conversation.doc(conversationId).update({
					messageCount: data.docs[0].data().messageCount + 1
				})
			} else {
				newConversation.messageCount = 1
				newConversation.newMessage = 1
				conversation.doc(conversationId).set(newConversation)
				console.log("New converstaion created")

			}
		})
		.catch(err => console.log(err))

	const newMessage = {
		deal: req.body.deal ? req.body.deal : null,
		body: req.body.body,
		createdAt: new Date().toISOString(),
		recipientId: req.body.userId,
		senderId: req.user.uid,
		conversationId: conversationId,
		read: false
	}

	messageId = db.collection('messages').doc().id
	newMessage.messageId = messageId
	newMessage.paticipants = [newMessage.senderId, newMessage.recipientId]

	db.collection('messages').doc(messageId).set(newMessage)
		.then(() => {
			res.json({
				messageData: newMessage,
				message: {
					success: `message sent successfully`,
				}
			})
		})
		.catch(() => {
			res.status(500).json({
				error: "Unable to create deal",
				errCode: err.code
			});
		})
}

exports.sendDirectMessageReply = (req, res) => {


	if (String(req.body.body).trim() === '') return res.status(400).json({
		body: 'Must not be empty test'
	})

	if (String(req.body.userId).trim() === '') return res.status(400).json({
		general: 'Server error, missing parameter userId'
	})

	let conversationId = combineUserIds(req.user.uid, req.body.userId)
	db.doc(`/conversations/${conversationId}`)
		.get()
		.then(data => {
			if (data.empty) {
				return res.status(404).json({
					error: 'conversation not found'
				})
			} else {
				db.doc(`/conversations/${conversationId}`)
					.update({
						messageCount: data.docs[0].data().messageCount + 1
					})
			}
		})
		.catch(() => {
			console.log(err)
			res.status(500).json({
				error: "Unable to send message",
				errCode: err.code
			});
		})

	const newMessage = {
		deal: deal ? deal : null,
		body: req.body.body,
		createdAt: new Date().toISOString(),
		recipientId: req.body.userId,
		senderId: req.user.uid,
		conversationId: conversationId,
		read: false
	}
	messageId = db.collection('messages').doc().id
	newMessage.messageId = messageId
	newMessage.paticipants = [newMessage.senderId, newMessage.recipientId]

	db.collection('messages').doc(messageId).set(newMessage)
		.catch(err => {

		})
	return res.json(newMessage)
}

exports.deleteMessage = (req, res) => {
	const message = db.doc(`/messages/${req.params.messageId}`)
	message
		.get()
		.then(doc => {
			if (doc.empty) {
				return res.status(404).json({
					error: 'message not found'
				})
			} else {

				console.log(doc.data())
				conversationId = doc.data().conversationId
				message.delete()
					.then(() => {

						db.collection('messages').where('conversationId', '==', conversationId)
							.get()
							.then(doc => {
								const conversations = db.doc(`/conversations/${conversationId}`)
								if (doc.empty) {
									conversations.delete()
								} else {
									conversations.get().then(doc => {
										if (!doc.empty) {
											conversations.update({
													messageCount: doc.data().messageCount - 1
												})
												.catch(err => {
													console.log(err)
												})
										}
									})

								}
							})

						/*.then(() => {
							db.doc(`/notifications/${req.params.messageId}`).delete()
							return res.json(req.params.messageId)
						})*/
						return res.json(req.params.messageId)
					})
					.catch(err => {
						console.log(err)
					})

			}
		})

		.catch(err => console.log(err))
}

exports.getConversations = (req, res) => {
	let conversations = [];

	console.log("conversations")

	return db.collection('conversations')
		.where('participants', 'array-contains', req.user.uid)
		.get()
		.then(data => {
			if (!data.empty) {
				//console.log(data.docs[0].data())
				data.forEach(doc => {
					conversations.push(doc.data())
				});
			} else {

				console.log("conversations not found")
			}

		})
		.then(() => {
			//var messagesByConversation = groupBy(messages, "conversationId");

			return res.json(conversations)
		})
		.catch(err => {
			console.log(err)
		})
}

exports.deleteConversation = (req, res) => {
	const conversationId = req.params.conversationId
	const conversation = db.doc(`/conversations/${conversationId}`)

	conversation.delete()
		.then(() => {
			return res.json({
				alert: {
					title: "Conversation Deleted",
					text: `Your conversation has been deleted successfully`
				}
			})
		})
		.catch((err) => {
			console.log(err)
			return res.status(500).json({
				alert: {
					title: "Conversation Delete Error",
					text: `Your conversation could not be deleted`
				}
			})
		})
}

exports.getMessagesByConversationId = (req, res) => {
	let messages = [];
	let readMessages = [];

	let batch = db.batch();
	messageWithMatchConvIds = db.collection('messages')
		.orderBy('createdAt', 'asc')
		.where('conversationId', '==', req.params.conversationId)

	return messageWithMatchConvIds
		.get()
		.then(data => {
			if (!data.empty) {
				data.forEach(doc => {
					messages.push(doc.data())
				});
			} else {
				console.log("messages not found")
				return res.status(404).json({
					error: "messages not found"
				})
			}

		})
		.then(() => {
			//var messagesByConversation = groupBy(messages, "conversationId");
			messages.forEach(
				(messageData) => {
					const messagesCollections = db.doc(`/messages/${messageData.messageId}`)
					batch.update(messagesCollections, {
						read: true
					});
					messageData.read = true
					readMessages.push(messageData)
				});
			batch.commit()
			return res.json(readMessages)
		})
		.catch(err => {
			console.log(err)
		})
}

const groupBy = (collection, property) => {
	var i = 0,
		val, index,
		values = [],
		result = [];
	for (; i < collection.length; i++) {
		val = collection[i][property];
		index = values.indexOf(val);
		if (index > -1)
			result[index].push(collection[i]);
		else {
			values.push(val);
			result.push([collection[i]]);
		}
	}
	return result;
}

const combineUserIds = (userId1, userId2) => {
	if (userId1 < userId2) {
		return userId1 + userId2;
	} else {
		return userId2 + userId1;
	}
}

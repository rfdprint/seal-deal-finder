const {
	filterObjectkeys
} = require("../util/utilFunctions")

const {
	admin,
	firebase,
	db,
	config,
	devEnv
} = require('../util/admin');


const {
	deleteImages
} = require('../util/utilFunctions')

const {
	errorRreponse
} = require('../util/errors')

const {
	analyticsLogin
} = require('../handlers/analytics')

if (devEnv) {
	//firebase.auth().useEmulator('http://localhost:9099/')
}

const BusBoy = require('busboy');
const path = require('path');
const os = require('os');
const fs = require('fs');

const {
	validateSignupData,
	validateLoginData,
	reduceUserDetails
} = require('../util/validators');

const {
	deleteDocs
} = require('../util/utilFunctions');
const {
	database
} = require("firebase-admin");

exports.deleteUser = (req, res) => {
	let userId = req.body.userId
	let confirmedHandle = req.body.confirmedHandle

	if (userId === req.user.uid) {
		console.log("cant delete self")
		return res.status(400).json({
			error: {
				alert: {
					title: "User Delete Error",
					text: "Caution, you cannot delete you own account, please contact support or another system admin to process this request for you."
				}
			}
		});
	}

	user = db.doc(`/users/${userId}`)
	user.get()
		.then(doc => {
			if (!doc.empty) {
				if (doc.data().handle !== confirmedHandle) {
					return res.status(500).json({
						error: {
							alert: {
								title: "User Delete Error",
								text: "The user name you entered does not match the user name on record"
							}
						}
					})
				} else {
					admin.auth().deleteUser(userId)
						.then(function() {
							user.delete()
								.then(() => {
									console.log('Successfully deleted user');
									return res.json({
										success: {
											userId: userId,
											alert: `user deleted successfully ${userId}`
										}
									})
								})
						})
						.catch(err => {
							console.log('Error deleting user:', err);
							return res.status(500).json({
								error: {
									alert: errorRreponse(err.code)
								}
							})

						});
				}
			} else {
				return res.status(500).json({
					error: {
						alert: {
							title: "Server Error",
							text: "User does not exist"
						}
					}
				})
			}
		})
}

exports.deleteOwnUserAccount = (req, res) => {
	let userId = req.body.userId
	let confirmedHandle = req.body.confirmedHandle

	console.log(req)

	if (userId !== req.user.uid) {
		console.log("can only delete own user account")
		return res.status(400).json({
			error: {
				alert: {
					title: "User Delete Error",
					text: "Caution, you cannot delete someone elses account, please contact support or another system admin for assistance."
				}
			}
		});
	}
	console.log(req.body.confirmedHandle)
	user = db.doc(`/users/${userId}`)
	user.get()
		.then(doc => {
			if (!doc.empty) {
				if (doc.data().handle !== confirmedHandle) {
					return res.status(500).json({
						error: {
							alert: {
								title: "User Delete Error",
								text: "The user name you entered does not match the user name on record"
							}
						}
					})
				} else {

					// Revoke all refresh tokens for a specified user for whatever reason.
					// Retrieve the timestamp of the revocation, in seconds since the epoch.
					admin.auth().revokeRefreshTokens(req.user.uid)
						.then((timestamp) => {
							console.log(timestamp)
							console.log(`Token for ${req.user.uid} revoked at: ${timestamp}`);
						})
						.catch(err => {
							console.log(err)
						})

					admin.auth().deleteUser(req.user.uid)
						.then(function() {
							user.delete()
								.then(() => {
									console.log('Successfully deleted user');
									res.clearCookie('__session')
									return res.json({
										success: {
											userId: req.user.uid,
											alert: `your account successfully deleted`
										}
									})
								})


						})
						.catch(err => {
							console.log('Error deleting user:', err);
							return res.status(500).json({
								error: {
									alert: errorRreponse(err.code)
								}
							})

						});
				}
			} else {
				return res.status(500).json({
					error: {
						alert: {
							title: "Server Error",
							text: "User does not exist"
						}
					}
				})
			}
		})
}


exports.setUserRole = (req, res) => {

	admin.auth().setCustomUserClaims(req.body.userId, req.body.claim).then(() => {
			// The new custom claims will propagate to the user's ID token the
			// next time a new one is issued.

			let claim = req.body.claim.admin

			const userRecord = db.collection('users').doc(req.body.userId)
			userRecord
				.update({
					roles: {
						...req.body.claim
					},
					admin: req.body.claim.admin
				})
				.then(() => {
					// Revoke all refresh tokens for a specified user for whatever reason.
					// Retrieve the timestamp of the revocation, in seconds since the epoch.
					admin.auth().revokeRefreshTokens(req.body.userId)
						.then(() => {
							return admin.auth().getUser(req.body.userId);
						})
						.then((userRecord) => {
							return new Date(userRecord.tokensValidAfterTime).getTime() / 1000;
						})
						.then((timestamp) => {
							console.log(`Token for ${req.body.userId} revoked at: ${timestamp}`);
						});
				})
				.then(() => {
					//console.log(req.body.claim)
					return res.json({
						alert: {
							success: 'authorization updated successfully'
						}
					})
				})
				.catch(err => {
					console.log(err)
					return res.status(500).json({
						error: "Unable to complete request"
					})
				})

		})
		.catch(err => {
			console.log(err)
			return res.status(403).json({
				error: "Unable to complete request"
			})
		})

}

exports.getVerificationFile = (req, res) => {

	db.collection('verifications')
		.where("userId", "==", req.body.userId)
		.limit(1)
		.get()
		.then(snapshot => {
			snapshot.forEach(doc => {
				if (doc.exists) {
					//console.log(doc.data().fileUrl)
					return res.json(doc.data())
				} else {
					return res.json({
						message: {
							info: "File not found"
						}
					})
				}
			})
		})
		.catch(err => {
			console.log(err)
			return res.json(err)
		})

}

exports.systemGetUsers = (req, res) => {
	let users = [];

	db.collection('/users/').get()
		.then(snapshot => {
			snapshot.forEach(doc => {
				users.push({
					handle: doc.data().handle,
					userId: doc.data().userId,
					imageUrl: doc.data().imageUrl,
					verified: doc.data().verified,
					roles: doc.data().roles,
					firstName: doc.data().firstName,
					lastName: doc.data().lastName,
					verificationStatus: doc.data().verificationStatus,
					subscriptionPaid: doc.data().subscriptionPaid,
					subscriptionStatus: doc.data().subscriptionStatus,
					type: doc.data().type
				})

			})
		})
		.then(() => {
			return res.json(users)
		})
		.catch((err) => {
			console.log(err)
		})

}

exports.postVerifacation = (req, res) => {

	//console.log(req.body)
	let verification = {
		verified: req.body.verified,
		verificationStatus: req.body.verificationStatus
	}

	db.collection('users').doc(req.body.userId)
		.update(verification)
		.then(() => {
			return res.json({
				alert: {
					success: 'Verifacation updated successfully',
				}
			})
		})
		.catch(err => {
			console.error(err);

			return res.status(500).json({
				error: {
					alert: errorRreponse(err.code)
				}
			})
		})
}

exports.uploadVerifacationFile = (req, res) => {
	const BusBoy = require('busboy');
	const path = require('path');
	const os = require('os');
	const fs = require('fs');

	//console.log(req.user.uid)

	const busboy = new BusBoy({
		headers: req.headers
	})

	let fileName;
	let originalFileName;
	let fileToBeUploaded = {};
	const newId = db.collection('verification').doc().id

	let query = {
		field: "userId",
		matchValue: req.user.uid
	}
	deleteDocs("verifications", query)


	busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
		if (mimetype !== 'application/pdf' && mimetype !== 'application/pdf') {
			return res.status(400).json({
				error: {
					alert: {
						title: "Wrong File Type",
						text: "Only file of type .pdf will be accepted. Please upload a .pdf file and submit again"
					}
				}
			})
		}

		originalFileName = filename;
		const fileExtension = filename.split('.')[filename.split('.').length - 1];
		fileName = `${newId}.${fileExtension}` //`${Math.round(Math.random() * 1000000000000)}.${fileExtension}`;
		const filepath = path.join(os.tmpdir(), fileName);

		fileToBeUploaded = {
			filepath,
			mimetype
		}
		console.log(file)

		file.pipe(fs.createWriteStream(filepath))
	});

	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
		console.log('Field [' + fieldname + ']: value: ' + val);
	});

	busboy.on('finish', () => {

		const fileMainFolder = 'files'
		const fileSubFolder = `verification/${req.user.uid}`
		const file = req.body
		const fileUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${fileMainFolder}%2F${fileSubFolder}%2F${fileName}`

		const fileDestFolder = `${fileMainFolder}/${fileSubFolder}`

		admin.storage().bucket().upload(fileToBeUploaded.filepath, {
				resumable: false,
				destination: `${fileDestFolder}/${fileName}`,
				metadata: {
					metadata: {
						contentType: fileToBeUploaded.mimetype,
						owner: req.user.uid
					}
				}
			})
			.then(() => {

				let file = admin.storage().bucket().file(`${fileDestFolder}/${fileName}`)
				let signedFile = []

				file.getSignedUrl({
					action: 'read',
					expires: '01-01-2100'
				}).then(signedUrls => {
					// signedUrls[0] contains the file's public URL
					db.collection('verifications')
						.doc(newId)
						.set({
							createdAt: new Date().toISOString(),
							userId: req.user.uid,
							verificationId: newId,
							originalFileName: originalFileName,
							fileName: fileName,
							fileLocation: fileDestFolder,
							fileUrl: fileUrl,
							fileSignedUrl: signedUrls[0],

						})
				}).catch((err => console.log(err)))
			})
			.then(() => {
				return res.json({
					message: "File uploaded sucessfully",
					verificationUploaded: true

				});
			})
			.catch(err => {
				console.error(err);
				console.log("*********************************************************")
				console.error(err.code);
				console.log("*********************************************************")
				return res.status(500).json({
					error: {
						alert: errorRreponse(err.code)
					}
				})
			})
	})
	busboy.end(req.rawBody);
	return ('Upload File Attemmpted')

}
////////////////////////////////////////////////////

exports.signup2 = (req, res) => {
	newUser = req.body

	const {
		valid,
		errors
	} = validateSignupData(newUser);
	if (!valid) return res.status(400).json(errors);

	db.collection('users')
		.where('handle', '==', newUser.handle)
		.get()
		.then(snapshot => {

			if (snapshot.exists) {
				return res.status(400).json({
					handle: `This handle is already taken`,
				})
			} else {
				let userId;
				let userCredentials;


				firebase.auth().createUserWithEmailAndPassword(newUser.email, newUser.password)
					.then(data => {

						userId = data.user.uid
						userCredentials = {
							handle: newUser.handle,
							email: newUser.email,
							firstName: newUser.firstName,
							lastName: newUser.lastName,
							type: newUser.type,
							imageVersion: 1,
							verified: false,
							subscriptionPaid: false,
							createdAt: new Date().toISOString(),
							imageUrl: null,
							userId: userId,
							roles: {
								admin: false
							}
						}
						db.collection('users').doc(userId)
							.set(userCredentials)
							.catch((err) => {
								console.log(err)
							})
						return data.user.getIdToken();
					})
					.then(token => {

						//TEMP LOG
						console.log(token)
						// Set session expiration to 5 days.
						//5 Days 60 * 60 * 24 * 5 * 1000;
						// 5 Mins 0 * 5 * 1000;
						const expiresIn = 60 * 60 * 24 * 14 * 1000;
						// Create the session cookie. This will also verify the ID token in the process.
						// The session cookie will have the same claims as the ID token.
						// To only allow session cookie setting on recent sign-in, auth_time in ID token
						// can be checked to ensure user was recently signed in before creating a session cookie.
						admin.auth().createSessionCookie(token, {
								expiresIn
							})
							.then((sessionCookie) => {
								// Set cookie policy for session cookie.
								const options = {
									maxAge: `${ expiresIn }`,
									httpOnly: true,
									secure: false
								};
								//res.setHeader('Cache-Control', 'private');
								res.cookie('__session', sessionCookie, options);
								res.end(JSON.stringify({
									user: userCredentials,
									alert: {
										success: `New user created successfully: ${newUser.handle}`
									}
								}));
							})
							.catch((err) => {
								console.log(err)
							})
					})
					.catch(err => {
						if (err.code === "auth/email-already-in-use" || err.code === "auth/email-already-exists") {
							return res.status(400).json({
								email: "Email is already in use"
							})
						} else if (err.code === "auth/invalid-password" || err.code === "auth/weak-password") {
							return res.status(400).json({
								password: err.message
							})
						} else {
							console.log(err.message)
							console.log(err.code)
							return res.status(500).json({
								general: "Something went wrong, please try again"
							})
						}
					});
			}
		})
		.catch(err => {
			console.log(err.code)
		})
}

exports.login = (req, res) => {

	const user = {
		email: req.body.email,
		password: req.body.password
	}
	console.log(user)
	const {
		valid,
		errors
	} = validateLoginData(user);
	if (!valid) return res.status(400).json(errors);

	// As httpOnly cookies are to be used, do not persist any state client side.
	//firebase.auth().setPersistence(firebase.auth.Auth.Persistence.NONE);

	firebase.auth().signInWithEmailAndPassword(user.email, user.password)
		.then(data => {
			//console.log(data)
			return data.user.getIdToken();
		})
		.then(token => {
			//console.log(token)
			// Set session expiration to 5 days.
			//5 Days 60 * 60 * 24 * 5 * 1000;
			// 5 Mins 0 * 5 * 1000;
			const expiresIn = 60 * 60 * 24 * 14 * 1000;
			// Create the session cookie. This will also verify the ID token in the process.
			// The session cookie will have the same claims as the ID token.
			// To only allow session cookie setting on recent sign-in, auth_time in ID token
			// can be checked to ensure user was recently signed in before creating a session cookie.
			admin.auth().createSessionCookie(token, {
					expiresIn
				})
				.then((sessionCookie) => {
					console.log(analyticsLogin())

					// Set cookie policy for session cookie.
					const options = {
						maxAge: expiresIn,
						httpOnly: true,
						secure: false
					};
					//res.setHeader('Cache-Control', 'private');
					analyticsLogin()
					console.log('session created successfully')
					res.cookie('__session', sessionCookie, options);
					res.end(JSON.stringify({
						status: 'success'
					}));

				})
				.catch((err) => {
					console.log(err)
					return res.status(401).json({
						general: "Wrong credentials, please try again"
					})
				});
			/*return res.json({
				token
			})*/
		})
		.catch(err => {
			console.error(err)
			return res.status(403).json({
				general: "Wrong credentials, please try again"
			})
		})
}

exports.logout = (req, res) => {
	res.clearCookie('__session')
	res.end(JSON.stringify({
		status: 'logged out'
	}));
}


/*
exports.login = (req, res) => {
	const user = {
		email: req.body.email,
		password: req.body.password
	}

	const {
		valid,
		errors
	} = validateLoginData(user);
	if (!valid) return res.status(400).json(errors);

	firebase.auth().signInWithEmailAndPassword(user.email, user.password)
		.then(data => {
			console.log(data)
			return data.user.getIdToken();
		})
		.then(token => {
			return res.json({
				token
			})
		})
		.catch(err => {
			console.error(err)
			return res.status(403).json({
				general: "Wrong credentials, please try again"
			})
		})
}
*/

//Add user details
exports.editUserDetails = (req, res) => {

	const busboy = new BusBoy({
		headers: req.headers
	})

	let imageFile;
	let imageFileName;
	let imageToBeUploaded = {};
	let userDetails = {};

	busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
		if (mimetype !== 'image/jpeg' && mimetype !== 'image/png') {
			return res.status(400).json({
				error: 'Wrong file type submitted'
			})
		}

		const imageExtension = filename.split('.')[filename.split('.').length - 1];

		//imageFileName = `${newDealString.dealId}.${imageExtension}`;
		imageFile = file;
		imageFileName = `${Math.round(Math.random() * 1000000000000)}.${imageExtension}`;
		const filepath = path.join(os.tmpdir(), imageFileName);

		imageToBeUploaded = {
			filepath,
			mimetype
		}
		file.pipe(fs.createWriteStream(filepath))
	});

	//Fields
	let formData = new Map();
	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
		formData.set(fieldname, val);
	});

	busboy.on('finish', () => {

		let newUserDetailString = formData.get('userDetails')

		userDetails = JSON.parse(newUserDetailString)
		userDetails.userId = req.user.uid
		userDetails.handle = req.user.handle

		if (imageFileName && (imageFileName !== "undefined")) {
			userDetails.imageVersion = userDetails.imageVersion + 1

			//Delete all previous user images
			deleteImages("profiles", userDetails.userId)
				.then(() => {
					console.log(`previous user profile images deleted for ${userDetails.userId}`)
				})
				.catch(err => {
					console.log(err)
				})
			const imageExtension = imageFileName.split('.')[imageFileName.split('.').length - 1];
			imageFileName = `${userDetails.userId}.${imageExtension}`;

			const imageMainFolder = 'images'
			const imageSubFolder = 'profiles'
			const imageDestFolder = `${imageMainFolder}/${imageSubFolder}`
			const imageVersion = userDetails.imageVersion

			const imageFilePath = `${imageDestFolder}/${userDetails.userId}/v${imageVersion}/${imageFileName}_500x250`

			admin.storage().bucket().upload(imageToBeUploaded.filepath, {
					resumable: false,
					destination: imageFilePath,
					metadata: {
						metadata: {
							contentType: imageToBeUploaded.mimetype
						}
					}
				})
				.then(() => {
					console.log(`user profile images added for ${userDetails.userId}`)
				})
				.then(() => {
					const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageMainFolder}%2F${imageSubFolder}%2F${userDetails.userId}%2Fv${imageVersion}%2F${imageFileName}_500x250?alt=media`
					userDetails.imageUrl = imageUrl
					userDetails.imageFilePath = imageFilePath

					file = admin.storage().bucket().file(imageFilePath)

					file.getSignedUrl({
						action: 'read',
						expires: '01-01-2100'
					}).then(signedUrls => {
						// signedUrls[0] contains the file's public URL
						userDetails.signedUrl = signedUrls[0]
						//console.log(signedUrls[0])
					}).catch((err => console.log(err)))

					console.log(userDetails)
					userCollection = db.collection('users')
						.doc(userDetails.userId)
						.update(userDetails)
						.then(() => {
							console.log(`user profile updated for ${userDetails.userId}`)
						})
						.catch(err => {
							console.log(err)
						})
				}).then(() => {
					console.log(userDetails)

					return res.json({
						user: userDetails,
						message: {
							success: `user updated successfully: ${userDetails.handle}`
						}
					});
				})
				.catch(err => {
					console.log(err)
					res.status(500).json({
						error: "Unable to create deal",
						errCode: err.code
					});
				})
		} else {
			console.log(userDetails)
			db.collection('users')
				.doc(userDetails.userId)
				.update(userDetails)
				.then(() => {
					return res.json({
						user: userDetails,
						message: {
							success: `user updated successfully: ${userDetails.handle}`
						}
					});
				})
				.catch(err => {
					console.log(err)
				})
		}

	})
	busboy.end(req.rawBody);
}



//Get any user's details
exports.getUserProfileData = (req, res) => {
	let userData = {};
	let userId;

	db.collection('users').where('handle', "==", req.params.handle)
		.get()
		.then(snapshot => {
			snapshot.forEach(doc => {
				userId = doc.data().userId
			})

			db.doc(`/users/${userId}`).get()
				.then(doc => {
					if (doc.exists) {
						//console.log(doc)
						userData.user = {};
						//userData.user = doc.data();
						userData.user.dealId = doc.id
						userData.user.userId = userId
						userData.user.handle = doc.data().handle
						userData.user.type = doc.data().type
						userData.user.commentCount = doc.data().commentCount
						userData.user.likeCount = doc.data().likeCount
						userData.user.userImage = doc.data().userImage
						userData.user.imageUrl = doc.data().imageUrl
						userData.user.signedUrl = doc.data().signedUrl
						userData.user.verified = doc.data().verified

						//console.log(userData.user)
						console.log(userData.user)
						return res.json({
							profile: {
								user: userData.user
							}
						})

					} else {
						return res.status(404).json({
							error: "user cannot be found"
						});
					}
				})
				.catch(err => {
					console.log(err)
				})
		})
		.catch(err => {
			console.log(err)
		})
}


// Get own user details
exports.getAuthenticatedUser = (req, res) => {
	let userData = {};
	db.doc(`/users/${req.user.uid}`)
		.get()
		.then((doc) => {
			if (doc.exists) {
				userData.credentials = doc.data();
				//console.log(doc.data())
			}
			return db
				.collection('likes')
				.where('userId', '==', req.user.uid)
				.get()
				.catch((err) => {
					console.log(`Like Error: ${err}`)
				});
		})
		.then(data => {
			if (data) {
				userData.likes = [];
				data.forEach(doc => {
					userData.likes.push(doc.data());
				});
			}
			return db.collection('notifications').where('recipientId', '==', req.user.uid)
				.orderBy('createdAt', 'desc').limit(20).get().catch((err) => {
					console.log(`Notifications Error: ${err}`)
				})
				.catch(err => {
					console.log(err)
				})
		})
		.then(data => {
			if (data) {

				userData.notifications = [];
				data.forEach(doc => {
					userData.notifications.push({
						notificationId: doc.data().notificationId,
						messageId: doc.data().messageId,
						conversationId: doc.data().conversationId,
						sender: doc.data().sender,
						recipient: doc.data().recipient,
						participants: doc.data().participants,
						read: doc.data().read,
						messageId: doc.data().messageId,
						type: doc.data().type,
						createdAt: doc.data().createdAt,
					})
				})
			}
			if (devEnv) {
				userData.emu = true
			} else {
				userData.emu = false
			}

			return res.json(userData);
		})
		.catch((err) => {
			console.error(err);
			return res.status(500).json({
				error: {
					alert: errorRreponse(err.code)
				}
			})
		});
};

//Upload a profile image
exports.uploadUserImage = (req, res) => {
	const BusBoy = require('busboy');
	const path = require('path');
	const os = require('os');
	const fs = require('fs');

	const busboy = new BusBoy({
		headers: req.headers
	})

	let imageFileName;
	let imageToBeUploaded = {};

	busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
		if (mimetype !== 'image/jpeg' && mimetype !== 'image/png') {
			return res.status(400).json({
				error: 'Wrong file type submitted'
			})
		}
		//console.log(fieldname);
		//console.log(filename);
		//console.log(mimetype);

		const imageExtension = filename.split('.')[filename.split('.').length - 1];
		imageFileName = `${Math.round(Math.random() * 1000000000000)}.${imageExtension}`;
		const filepath = path.join(os.tmpdir(), imageFileName);

		imageToBeUploaded = {
			filepath,
			mimetype
		}
		file.pipe(fs.createWriteStream(filepath))
	});

	busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
		//console.log('Field [' + fieldname + ']: value: ' + val);
	});

	busboy.on('finish', () => {

		const imageMainFolder = 'images'
		const imageSubFolder = 'profiles'

		const ImageDestFolder = `${imageMainFolder}/${imageSubFolder}`

		admin.storage().bucket().upload(imageToBeUploaded.filepath, {
				resumable: false,
				destination: `${ImageDestFolder}/${imageFileName}`,
				metadata: {
					metadata: {
						contentType: imageToBeUploaded.mimetype
					}
				}
			})
			.then(() => {
				//const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageFileName}?alt=media`

				const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageMainFolder}%2F${imageSubFolder}%2F${imageFileName}?alt=media`

				/*file = admin.storage().bucket().file(`${ImageDestFolder}/${imageFileName}`)
				let signedImageUrl;
				file.getSignedUrl({
					action: 'read',
					expires: '01-01-2100'
				}).then(signedUrls => {
					// signedUrls[0] contains the file's public URL
					signedUrl = signedUrls[0]
					//console.log(signedUrls[0])
				}).catch((err => console.log(err)))*/

				return db.doc(`/users/${req.user.uid}`).update({
					imageUrl,
					imageFileName,
					imageUrlLocation: ImageDestFolder
				})
			})
			.then(() => {
				return res.json({
					message: "Image uploaded sucessfully"
				});
			})
			.catch(err => {
				console.error(err);
				return res.status(500).json({
					error: err.code
				})
			})
	})
	busboy.end(req.rawBody);

}

exports.getNotifications = (req, res) => {
	notifications = []
	db.collection('notifications')
		.where('recipientId', '==', req.user.uid)
		.where('read', '==', false)
		.orderBy('createdAt', 'desc')
		.limit(25)
		.get()
		.then((querySnapshot) => {
			querySnapshot.forEach((doc) => {
				notifications.push(doc.data())
			});
			return res.json({
				notifications: notifications
			});
		})
		.catch((err) => {
			console.log(`Notifications Error: ${err}`)
			return res.status(500).json({
				error: err.code,
			})
		})

}

exports.deleteReadNotifications = (req, res) => {
	let batch = db.batch();
	let notificationIds = req.body
	notificationIds.forEach(
		notificationId => {
			const notification = db.doc(`/notifications/${notificationId}`)
			batch.delete(notification);
		});
	batch.commit()
		.then(() => {
			return res.json({
				message: 'Notifications deleted',
			})
		})
		.catch(err => {
			console.error(err);
			return res.status(500).json({
				error: err
			});
		})
}

exports.resetPasswordEmail = (req, res) => {
	console.log(req.body.emailAddress)
	const emailAddress = req.body.emailAddress
	firebase.auth().sendPasswordResetEmail(emailAddress).then(function() {
		// Email sent.
		return res.json({
			success: {
				alert: {
					title: "Email Sent",
					test: 'You have been sent an email to reset your password',
				}
			}
		})
	}).catch(function(err) {
		return res.status(500).json({
			error: {
				alert: errorRreponse(err.code)
			}
		})
	});

}

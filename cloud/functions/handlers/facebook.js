//https://developers.facebook.com/docs/graph-api/reference/v6.0/page/feed

//TEST_URL: https://www.facebook.com/Seal-Deal-Finder-108992524222807
//PROD_URL: https://www.facebook.com/Seal-Deal-Finder-107883014257808
const {
	projectId
} = require('../util/admin');

const {
	errorRreponse
} = require('../util/errors')


const functions = require('firebase-functions');

const functionsConfig = functions.config().env;
var FB = require('fb').default;

const facebookKey = functionsConfig.facebook[`${projectId}`]

//FB.setAccessToken("EAAD8PYmCFZC8BALcLx8RDBma7At2zcdQZCxQBHFqFkNce9A1ZB2DhSX27yJa6YL8okF98niCq7IOCyKA9Lf7ESe3ZA6ZAeKdwg6kZCb9Nio9za4ZBr5WnZBnOlhkRPe70t6BZAxqyGJIsyzYodCWPGvkKIOzL4QUD03n7n2OTP0M3Ka8XE2qLg3KnZAO0IA7y8AFWyVGcS30UNfAZDZD");
//	accessToken: facebookKey.extendedTokenNoExp,

FB.options({
	accessToken: facebookKey.extendedTokenNoExp,
	appId: facebookKey.clientId,
	appSecret: facebookKey.clientSecret
})

exports.facebookGetPage = (req, res) => {
	//GET
	FB.api(
			`/${facebookKey.pageId}/feed`,
			'GET', {})
		.then(data => {
			//console.log(data)
			//local ONLY
			return res.status(200).json(data)
		})
		.catch(err => {
			//console.log(err)
			//CAUTION REMOVE TOKEN CODE BELOW IN PRODUTION!!!!!!!!!!!!
			return res.status(500).json({
				error: err.message,
				errCode: err.code,
				//token: facebookKey.extendedTokenNoExp
			});

		})
}


exports.facebookPostDeal = (req, res) => {
	console.log('facebook')
	//params: message, link
	//message: simple message post, since part of the description is collected from the open graph bot thanks to SSR
	//link: the deal link (not the image link)
	//POST

	let baseUrl;
	let dealId = req.body.dealId //KZhQ9F0RNUA6n23tQ9vr
	let message = req.body.message

	let successMessage
	switch (facebookKey.FBenv) {
		case "prod":
			postSuccessMessage = `your deal posted to facebook successfully.`

			break;
		case "dev":
			postSuccessMessage = `your deal posted successfully to the TEST FACEBOOK PAGE. Click the link below to visit the test page.`
	}

	if (!message) {
		message = "Another great deal from sealdealfinder.com!"
	}
	console.log("***FaceBook Post Base URL******")
	console.log(facebookKey.seal_deal_finder_base_url)

	//"link": `https://${baseUrl}/deals/${dealId}`
	const dealLink = `https://${facebookKey.seal_deal_finder_base_url}/deals/${dealId}`
	FB.api(
			`/${facebookKey.pageId}/feed`,
			"POST", {
				"message": message,
				"link": dealLink
			})
		.then(data => {
			console.log(data)
			return res.status(200).json({
				message: {
					success: postSuccessMessage,
					data: {
						facebookPostId: data.id,
						facebookEnv: facebookKey.FBenv,
						facebookPageLink: facebookKey.pageLink,
						facebookTestUserCredentials: facebookKey.testUserCredentials
					}

				}
			})
		})
		.catch(err => {
			console.error(err)
			return res.status(500).json({
				error: {
					alert: {
						title: "Facebook Error",
						text: "an error occured while attempting to post the deal, please try again"
					}
				}
			})
		})
}

exports.facebookExtendAccess = (req, res) => {

	FB.api('oauth/access_token', {
			client_id: facebookKey.clientId,
			client_secret: facebookKey.clientSecret,
			grant_type: 'fb_exchange_token',
			fb_exchange_token: facebookKey.accessToken
		})
		.then(data => {
			console.log(data)
			var accessToken = data.access_token;
			var expires = data.expires ? data.expires : 0;
			return res.status(200).json({
				accessToken,
				expires
			})
		})
		.catch(err => {
			console.log(!err ? 'error occurred' : err.message);
			return res.status(500).json({
				error: err.message,
				errCode: err.code,

			});

		})

}

/*"data": [
    {
      "access_token": "EAAD8PYmCFZC8BAGgmfiTZBjRHjtBGkelrwEZAzj5WY085uz5LL6qGS8RmPYDNUt6iczblWhiuPMhohtuZAS4cAjnMvpkNyTCCZCKhZANYtbdUX2i7u6ibHTOYZAwcWIjeGohcHZAZBgJJ6HQzusSpNQMnZChEEurfMUq3EDZBgkIkWxk1GrlVgD0vAISUpUHhdDbErkolkNFiurIAZDZD",
      "category": "Real Estate",
      "category_list": [
        {
          "id": "198327773511962",
          "name": "Real Estate"
        }
      ],
      "name": "Seal Deal Finder",
      "id": "107883014257808",
      "tasks": [
        "ANALYZE",
        "ADVERTISE",
        "MANAGE_JOBS",
        "MODERATE",
        "CREATE_CONTENT",
        "MANAGE"
      ]
    }
  ]*/

exports.facebookTokenAccess = (req, res) => {
	FB.api('https://graph.facebook.com/108992524222807', function(res) {
		if (!res || res.error) {
			console.log(!res ? 'error occurred' : res.error);
			return;
		}
		console.log(res.id);
		console.log(res.name);
	})
}

const {
	admin,
	db,
	stripeConfigSK
} = require('../util/admin');

const stripe = require('stripe')(stripeConfigSK.key);
//scoop update stripe
//$ stripe listen --forward-to localhost:5000/seal-deal-finder-app/us-central1/api/charge/subscription/confirm/webhook --events payment_intent.succeeded
//$ stripe listen --forward-to localhost:5000/seal-deal-finder-app/us-central1/api/webhook/subscription/created --events customer.subscription.created
//stripe trigger payment_intent.succeeded

exports.subcriptionDeleted = (req, res) => {
	//console.log(req.rawBody)

	console.log("Hook Triggered")
	const endpointSecret = 'whsec_pkmCbw6fcHg9XH8m8DoP5j5YHYwvxQTG';
	const sig = req.headers['stripe-signature'];

	let event;
	try {
		event = stripe.webhooks.constructEvent(req.rawBody, sig, endpointSecret);
	} catch (err) {

		res.status(400).send(`Webhook Error: ${err.message}`);
	}

	// Handle the event
	if (event) {
		switch (event.type) {
			case 'customer.subscription.deleted':
				const subcription = event.data.object;
				db.doc(`/subcriptions/${subcription.metadata.userId}`).update({
					status: subcription.status
				})
				break;
			default:
				// Unexpected event type
				return res.status(400).end();
		}

		// Return a res to acknowledge receipt of the event
		res.json({
			received: true
		});
	} else {
		// Unexpected event type
		console.log('event object null')
		return res.status(400).end();
	}
}

exports.chargeUpdated = (req, res) => {
	//console.log(req.rawBody)
	//stripe listen --forward-to localhost:5000/seal-deal-finder-app/us-central1/api/webhook/charge/updated --events charge.failed,charge.refunded

	console.log("Charge Hook Triggered")
	const endpointSecret = 'whsec_pkmCbw6fcHg9XH8m8DoP5j5YHYwvxQTG';
	const sig = req.headers['stripe-signature'];

	let event;
	try {
		event = stripe.webhooks.constructEvent(req.rawBody, sig, endpointSecret);
	} catch (err) {

		res.status(400).send(`Webhook Error: ${err.message}`);
	}

	// Handle the event
	if (event) {
		const charge = event.data.object;
		stripe.invoices.retrieve(charge.invoice, {
				expand: ['subscription']
			})
			.then(invoice => {
				switch (event.type) {
					case 'charge.failed':
					case 'charge.refunded':
						let updatedLatestInvoice = db.collection('latestInvoices').doc(invoice.subscription.metadata.userId)

						updatedLatestInvoice
							.get()
							.then(doc => {
								if (doc.exists) {
									console.log({
										latestInvoiceCharge: charge.status,
										latestInvoiceChargeRefunded: charge.refunded,
										attemptCount: invoice.attempt_count,
										userId: invoice.subscription.metadata.userId
									})

									updatedLatestInvoice.update({
											latestInvoiceCharge: charge.status,
											latestInvoiceChargeRefunded: charge.refunded,
											attemptCount: invoice.attempt_count
										})
										.catch(err => {
											console.log(err)
										})
								} else {
									console.log("Invoice not found")
									console.log({
										latestInvoiceCharge: charge.status,
										latestInvoiceChargeRefunded: charge.refunded,
										attemptCount: invoice.attempt_count
									})
								}
							})
							.catch(err => {
								console.log(err)
							})
						break;

					default:
						// Unexpected event type
						return res.status(400).end();
				}
			})
			.catch(err => {
				console.log(err)
			})
		// Return a res to acknowledge receipt of the event
		res.json({
			received: true
		});
	} else {
		// Unexpected event type
		console.log('event object null')
		return res.status(400).end();
	}
}

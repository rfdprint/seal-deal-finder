var zipcodes = require('zipcodes');
var axios = require('axios');

var geoConfig = null //currently not used

exports.getAddressByZipcode = (req, res) => {
	const zipcode = req.body.zipcode
	console.log(zipcode)
	var results = zipcodes.lookup(zipcode);

	res.json({
		location: {
			zipcode: results.zip,
			latitude: results.latitude,
			longitude: results.longitude,
			city: results.city,
			state: results.state,
			country: results.country,
		}
	})
}

exports.getAddressSuggestion = (req, res) => {
	const params = {
		'app_id': "JmgWafyZBIEhxDqTSD6I",
		'apiKey': geoConfig.apiKey,
		'query': req.body.query,
		'maxresults': 1,
		/*'resultType': 'postalCode',
		'country': 'USA'*/
	}
	axios.get(
		'https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json', {
			params
		}
	).then((doc) => {

		res.json(doc.data)
	}).catch((err) => {
		console.log(err)
		return res.json(err)
	});
}

exports.getGeocode = (req, res) => {
	const params = {
		'apiKey': geoConfig.apiKey,
		'searchtext': req.body.searchtext
	}
	axios.get(
		'https://geocoder.ls.hereapi.com/6.2/geocode.json', {
			params
		}
	).then((doc) => {
		console.log(doc.data)
		console.log(res)
		res.json(doc.data)
	}).catch((err) => {
		console.log(err)
		return res.json(err)
	});
}

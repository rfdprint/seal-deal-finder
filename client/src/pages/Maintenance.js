import React from "react";
import FullScreenLayout from "../components/layouts/FullScreenLayout";
import MaintenanceSection from "../components/maintenace/MaintenanceSection";
//MUI Components
import { useTheme } from "@material-ui/core/styles";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const styles = makeStyles(theme => ({
	...theme.customStyles,
	content: {
		position: "relative",
		objectFit: "fill"
	},
	overlay: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: "100%"
	},
	overlayText: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -60%)",
		color: "white",
		textShadow: "2px 2px 8px #000000"
	},
	smallScreenText: {
		marginTop: 35
	}
}));

const Maintenance = () => {
	const theme = useTheme();
	const xtraSmallScreen = useMediaQuery(theme.breakpoints.down("xs"));
	const smallScreen = useMediaQuery(theme.breakpoints.down("sm"));
	const mediumScreen = useMediaQuery(theme.breakpoints.down("md"));

	{
		/*src={
						smallScreen
							? maintenanceImageSm
							: xtraSmallScreen
							? maintenanceImageSm
							: maintenanceImage
					*/
	}

	const classes = styles();
	return (
		<FullScreenLayout className={classes.content}>
			<MaintenanceSection />
		</FullScreenLayout>
	);
};

export default Maintenance;

import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
// Components
import StaticProfile from "../components/profile/StaticProfile";
import AgreementCard from "../components/agreements/AgreementCard";
//MUI Component
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
// Redux
import { useDispatch, useSelector } from "react-redux";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
import { getUserData } from "../redux/actions/dataActions";
const styles = makeStyles(theme => ({
	...theme.customStyles
}));

const User = () => {
	const data = useSelector(state => state.data);
	const [profile, setProfile] = useState("");
	const [agreementIdParam, setAgreementIdParam] = useState(null);
	const dispatch = useDispatch();
	const params = useParams();
	const classes = styles();
	const handle = params.handle;

	useEffect(() => {
		//ComponentDidMount
		const agreementId = params.agreementId;

		if (agreementId) {
			setAgreementIdParam(agreementId);
		}

		dispatch(getUserData(handle));
		axios
			.get(`/user/${handle}`)
			.then(res => {
				setProfile(res.data.user);
			})
			.catch(err => console.log(err));
	}, []);

	const { agreements, loading } = data;

	const agreementList = loading ? (
		<p>Loading...</p>
	) : agreements === null ? (
		<p>No agreements from this user</p>
	) : !agreementIdParam ? (
		agreements.map(agreement => (
			<AgreementCard key={agreement.agreementId} agreement={agreement} />
		))
	) : (
		agreements.map(agreement => {
			if (agreement.agreementId !== agreementIdParam)
				return (
					<AgreementCard
						key={agreement.agreementId}
						agreement={agreement}
					/>
				);
			else
				return (
					<AgreementCard
						key={agreement.agreementId}
						agreement={agreement}
						openDialog
					/>
				);
		})
	);
	return (
		<Grid container direction="row" justify="space-around">
			<Grid container item sm={9} xs={12}>
				{agreementList}
			</Grid>
			<Grid item sm={3} sm={3} xs={12}>
				{profile === null ? (
					<p>Loading profile...</p>
				) : (
					<StaticProfile profile={profile} />
				)}
			</Grid>
		</Grid>
	);
};

export default User;

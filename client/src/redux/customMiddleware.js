import { SET_USER } from "./types";

export function getUserDataMiddleware({ dispatch }) {
	return function (next) {
		return function (action) {
			if (action.type === SET_USER) {
				axios
					.get("/users", {
						headers: {
							Authorization: action.token,
						},
					})
					.then((res) => {
						console.log(res);
						console.log(res.data);
						action.payload = res.data;
					})
					.catch((err) => {
						console.log(err.response);
					});
			}
			return next(action);
		};
	};
}

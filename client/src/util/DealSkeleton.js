import React from "react";
import placeholder from "../images/cropped-image-test.jpg";

//MUI Components
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles(theme => ({
	...theme.customStyles,
	card: {
		display: "flex",
		marginBottom: 20
	},
	cardContent: {
		width: "100%",
		flexDirection: "row",
		padding: 25
	},
	cover: {
		minWidth: 200,
		height: 200,
		objectFit: "cover"
	},
	handle: {
		width: 60,
		height: 20,
		backgroundColor: theme.palette.primary.main,
		marginBottom: 7
	},
	date: {
		height: 14,
		width: 100,
		backgroundColor: "rgba(0,0,0,0.3)",
		marginBottom: 10
	},
	fullLine: {
		height: 15,
		width: "90%",
		marginBottom: 10,
		backgroundColor: "rgba(0,0,0,0.3)"
	},
	halfline: {
		height: 15,
		width: "50%",
		marginBottom: 10,
		backgroundColor: "rgba(0,0,0,0.3)"
	}
}));

const DealSkeleton = props => {
	const classes = styles();

	const content = Array.from({ length: 4 }).map((item, index) => (
		<Grid key={index} item xs={12} sm={6}>
			<Card>
				<CardMedia className={classes.cover} image={placeholder} />
				<CardContent>
					<div className={classes.handle}></div>
					<div className={classes.date}></div>
					<div className={classes.fullLine}></div>
					<div className={classes.fullLine}></div>
					<div className={classes.halfLine}></div>
				</CardContent>
			</Card>
		</Grid>
	));

	return (
		<Grid spacing={2} container>
			{content}
		</Grid>
	);
};

export default DealSkeleton;

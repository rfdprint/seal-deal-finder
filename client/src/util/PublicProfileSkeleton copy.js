import React, { Fragment } from "react";
import noImg from "../images/no-img.png";

//MUI
import InputAdornment from "@material-ui/core/InputAdornment";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import MuiLink from "@material-ui/core/Link";
import IconButton from "@material-ui/core/IconButton";
import ToolTip from "@material-ui/core/Tooltip";
import WebIcon from "@material-ui/icons/Web";
import CircularProgress from "@material-ui/core/CircularProgress";
//Icons
import LocationOn from "@material-ui/icons/LocationOn";
import LinkIcon from "@material-ui/icons/Link";
import CalendarToday from "@material-ui/icons/CalendarToday";
import EditIcon from "@material-ui/icons/Edit";
import KeyboardReturn from "@material-ui/icons/KeyboardReturn";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import InfoIcon from "@material-ui/icons/Info";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles((theme) => ({
	...theme.customStyles,
	paper: {
		padding: 10,
		backgroundColor: theme.palette.secondary.main,
	},
	spinner: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
	},
	card: {
		display: "flex",
		marginBottom: 20,
	},
	cardContent: {
		width: "100%",
		flexDirection: "coloumn",
		padding: 25,
	},
	cover: {
		minWidth: 200,
		objectFit: "cover",
	},
	handle: {
		width: 60,
		height: 20,
		backgroundColor: theme.palette.primary.main,
		marginBottom: 7,
	},
	date: {
		height: 14,
		width: 100,
		backgroundColor: "FFF",
		marginBottom: 10,
	},
	fullLine: {
		height: 15,
		width: "100%",
		marginBottom: 10,
		backgroundColor: "grey",
	},
	halfline: {
		height: 15,
		width: "50%",
		marginBottom: 10,
		backgroundColor: "grey",
	},
	profileDetails: {
		color: "white",
		textAlgin: "right",
	},
	profileFields: {
		margin: 10,
		padding: 10,
	},
	toolTipButton: { color: "white" },
	textBackground: {
		backgroundColor: "white",
		width: "100%",
	},
	profileFields: {
		color: theme.palette.primary.main,
	},
	profileFieldSeparator: {
		width: "100%",
		borderWidth: 0.1,
		border: "dotted",
		borderBottom: "1px solid rgba(0,0,0,0.2)",
		marginBottom: 10,
	},
	icon: {
		verticalAlign: "middle",
		marginRight: 10,
	},
	infoIcon: {
		margin: "0 auto",
		textAlign: "center",
		display: "block",
	},
	profileInnerBorder: {
		borderColor: theme.palette.primary.main,
		boderWidth: 1,
		borderStyle: "solid",
		borderRadius: 4,
		padding: 3,
	},
}));

const ProfileSkeleton = (props) => {
	const classes = styles();

	const content = (
		<Paper className={classes.paper}>
			<div className={classes.profile}>
				<div className="image-wrapper" style={{ position: "relative" }}>
					<div className={classes.spinner}>
						<CircularProgress style={{ color: "white" }} />
					</div>
					<img src={noImg} alt="profile" className="profile-image" />
				</div>
				<hr />
				<div className={classes.profileDetails}>
					<div className={classes.profileFields}>
						<Typography>
							<AccountCircleIcon className={classes.icon} />
						</Typography>

						<hr className={classes.profileFieldSeparator} />

						<Typography>
							<LocationOn className={classes.icon} />
						</Typography>

						<hr className={classes.profileFieldSeparator} />

						<Typography>
							<WebIcon className={classes.icon} />
						</Typography>

						<hr className={classes.profileFieldSeparator} />
						<Typography>
							<CalendarToday className={classes.icon} />
						</Typography>
						<hr className={classes.profileFieldSeparator} />

						<Typography>
							<InfoIcon className={classes.infoIcon} />
						</Typography>
					</div>
				</div>
			</div>
		</Paper>
	);

	return <Fragment>{content}</Fragment>;
};

export default ProfileSkeleton;

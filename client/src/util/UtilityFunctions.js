export const addCommasToNumber = (number) => {
	if (number && number !== 0) {
		return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	} else return number;
};

export function secondsToISOString(seconds) {
	const date = new Date(seconds * 1000);
	return date;
}

export const captializeFirstLetter = (str) => {
	if (str) {
		let results = str.replace(/^\w/, (c) => c.toUpperCase());
		return results;
	}
};

export const objectNotEmpty = (object) => {
	if (!object) return false;

	if (Object.entries(object).length > 0) {
		return true;
	} else {
		return false;
	}
};

export const getTagLabel = (key) => {
	const label = {
		singleFamily: "Single Family",
		incomeProducing: "Income Producer",
	};
	return label[key];
};

export const CheckFileSize = (size) => {
	console.log(size);
	console.log(`Upload File Size: ${size}`);
	if (size > 5000000) {
		console.log("File too large");
		return true;
	} else return false;
};

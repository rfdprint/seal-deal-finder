import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
//MUI
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const defaultProps = {
	linkTo: "/",
	text: "Link Button",
	size: "medium",
};

const GeneralLinkButton = ({
	menu,
	variant,
	className,
	size = defaultProps.size,
	text = defaultProps.text,
	linkTo = defaultProps.linkTo,
}) => {
	return (
		<Button
			underline="none"
			size={clsx(menu ? "small" : size)}
			fullWidth
			className={className}
			variant={variant}
			color={"secondary"}
			component={Link}
			to={linkTo}>
			{text}
		</Button>
	);
};

GeneralLinkButton.propTypes = {
	className: PropTypes.string,
	linkTo: PropTypes.string,
	menu: PropTypes.bool,
	size: PropTypes.string,
	text: PropTypes.any.isRequired,
	variant: PropTypes.string,
};

export default GeneralLinkButton;

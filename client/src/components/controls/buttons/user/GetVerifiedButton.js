import PropTypes from "prop-types";
import React from "react";
import clsx from "clsx";
//MUI
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
//Redux
import { useSelector } from "react-redux";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles((theme) => ({
	...theme.customStyles,
	menuButtonRight: {
		marginRight: theme.spacing(2),
		float: "right",
	},
}));

const defaultProps = {
	text: "Get Verified",
	variant: "outlined",
};

const GetVerifiedButton = ({
	size,
	menu,
	className,
	menuButtonRight,
	text = defaultProps.text,
	variant = defaultProps.variant,
}) => {
	const authUser = useSelector((state) => state.user.credentials);
	const classes = styles();
	return (
		<Grid item>
			{!authUser.verified && (
				<Button
					size={clsx(menu ? "small" : size)}
					className={clsx(
						classes.verifiedButton,
						menuButtonRight ? classes.menuButtonRight : null,
						className
					)}
					variant={variant}
					color={"secondary"}
					component={Link}
					to={"/subscriptions"}>
					{text}
				</Button>
			)}
		</Grid>
	);
};

GetVerifiedButton.propTypes = {
	className: PropTypes.string,
	menu: PropTypes.any,
	menuButtonRight: PropTypes.any,
	size: PropTypes.any,
	text: PropTypes.string,
	variant: PropTypes.string,
};

export default GetVerifiedButton;

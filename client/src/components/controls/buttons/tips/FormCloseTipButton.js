import React from "react";
import clsx from "clsx";
//MUI
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
const styles = makeStyles(theme => ({
	...theme.customStyles
}));

const defaultProps = {
	linkTo: "/",
	text: "Deal Board"
};

const DealButtonOutlined = ({
	size,
	menu,
	variant,
	className,
	text = defaultProps.text,
	linkTo = defaultProps.linkTo
}) => {
	return (
		<Grid item>
			<Button
				size={menu ? "small" : size}
				fullWidth
				className={clsx(classes.closeButton, className)}
				variant={variant}
				color={"secondary"}
				component={Link}
				to={linkTo}>
				{text}
			</Button>
		</Grid>
	);
};

export default DealButtonOutlined;

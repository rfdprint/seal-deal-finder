import React from "react";
//Component

import ToolTipButton from "../../../../components/controls/buttons/ToolTipButton";
//MUI
import Grid from "@material-ui/core/Grid";
//Icons
import EditIcon from "@material-ui/icons/Edit";
//Redux
import { useDispatch } from "react-redux";
import { toggleFormDialog } from "../../../../redux/actions/uiActions";

const FormToolTip = ({ form, menu, args, className, titleClassName }) => {
	const dispatch = useDispatch();

	const handleOpen = () => {
		dispatch(toggleFormDialog(true, form, args));
	};

	return (
		<Grid item>
			<ToolTipButton
				className={className}
				titleClassName={titleClassName}
				title="Edit profile picture"
				onClick={() => handleOpen()}>
				<EditIcon />
			</ToolTipButton>
		</Grid>
	);
};

export default FormToolTip;

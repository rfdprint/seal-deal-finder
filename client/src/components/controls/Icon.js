import React from "react";
import clsx from "clsx";
//MUI Components
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	icon: {
		maxWidth: 25,
		maxHeight: 20,
		margin: "0 10px",
	},
}));

const Icon = ({ icon, className }) => {
	const classes = styles();
	return <img className={clsx(className, classes.icon)} src={icon} />;
};

export default Icon;

import React, { useState } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Slider, { defaultValueReducer } from "@material-ui/core/Slider";

/**
 * this slider has a max that is not a multiple of its step. We use a custom
 * `valueReducer` to adjust the given values
 */
const ZoomSlider = () => {
	const [value, setValue] = useState(1);

	const handleChange = (event, value) => {
		setValue(value);
	};

	return (
		<Slider
			value={value}
			min={1}
			max={2}
			step={0.1}
			onChange={handleChange}
		/>
	);
};

ZoomSlider.propTypes = {
	classes: PropTypes.object.isRequired
};

export default ZoomSlider;

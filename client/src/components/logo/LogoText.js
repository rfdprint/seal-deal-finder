import React from "react";
import logoText from "../../images/logo-text@177.png";
import clsx from "clsx";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles(theme => ({
	...theme.customStyles,
	logo: {
		maxWidth: "100%",
		display: "block",
		margin: "0 auto"
	},
	large: {
		maxWidth: 300
	},
	small: {
		maxWidth: 150
	}
}));

const LogoText = ({ small }) => {
	const classes = styles();
	return (
		<img
			className={clsx(
				classes.logo,
				classes.loginForms,
				!small ? classes.large : classes.small
			)}
			src={logoText}
			alt="logo"></img>
	);
};

export default LogoText;

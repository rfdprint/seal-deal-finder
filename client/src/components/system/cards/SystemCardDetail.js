import React from "react";
//MUI Components
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";

const styles = makeStyles((theme) => ({
	...theme.customStyles,
	userId: {
		textAlign: "center",
	},
	adminUser: {
		backgroundColor: theme.palette.accent.main,
	},
	adminLabel: {
		color: "red",
	},
}));

const SystemCardDetail = ({ user }) => {
	const classes = styles();

	const checkAdminRole = (systemRoles) => {
		let isAdmin = false;
		if (systemRoles) {
			if (systemRoles.admin) {
				isAdmin = systemRoles.admin;
			}
		}
		return isAdmin;
	};

	return (
		<Grid item xs={12}>
			<hr />
			<Grid item>
				<Typography
					className={
						user.verified
							? classes.verifiedText
							: classes.notVerifiedText
					}>
					{checkAdminRole(user.roles) ? (
						<span className={classes.adminLabel}>
							(Admin User){" "}
						</span>
					) : null}
					{user.verified ? `Verified` : `Not Verified`}
				</Typography>
			</Grid>
			<Typography className={classes.userId} variant="caption">
				ID:{user.userId}
			</Typography>
		</Grid>
	);
};

export default SystemCardDetail;

import React from "react";
import clsx from "clsx";
//Components
import ContentFullScreen from "./body/ContentFullScreen";
//MUI Components
import Grid from "@material-ui/core/Grid";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	dealCard: {
		backgroundColor: "#51C46C",
	},
	root: {
		flexGrow: 1,
	},
	sideProfile: {
		minWidth: 240,
	},
	content: {
		padding: theme.spacing(2),
	},
}));

const MainLayout = ({ children, sidebar, backgroundColor }) => {
	const classes = styles();
	return (
		<ContentFullScreen backgroundColor={backgroundColor}>
			<div className={clsx(classes.root, classes.content)}>
				<Grid container justify="space-around" spacing={2}>
					<Grid
						className={classes.sideProfile}
						item
						xs={12}
						sm={12}
						md={3}>
						{sidebar}
					</Grid>
					<Grid item xs={12} sm={9}>
						<Grid container justify="space-evenly">
							{children}
						</Grid>
					</Grid>
				</Grid>
			</div>
		</ContentFullScreen>
	);
};

export default MainLayout;

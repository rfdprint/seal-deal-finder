import React, { Fragment } from "react";
//Components
import BodyLayout from "./body/ContentStandard";
//MUI Components
import Grid from "@material-ui/core/Grid";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
const styles = makeStyles(theme => ({
	...theme.customStyles,
	dealCard: {
		backgroundColor: "#51C46C"
	},
	root: {
		flexGrow: 1
	},
	sideProfile: {
		minWidth: 240
	}
}));

const MainLayout = ({ children, sidebar, sidebarRight, backgroundColor }) => {
	const classes = styles();
	return (
		<BodyLayout backgroundColor={backgroundColor}>
			<div className={classes.root}>
				<Grid container justify="space-around" spacing={2}>
					<Grid
						className={classes.sideProfile}
						item
						xs={12}
						sm={12}
						md={3}>
						{sidebar}
					</Grid>
					<Grid item xs={12} sm={6}>
						<Grid container justify="space-evenly">
							{children}
						</Grid>
					</Grid>
					<Grid
						className={classes.sideProfile}
						item
						xs={12}
						sm={12}
						md={3}>
						{sidebarRight}
					</Grid>
				</Grid>
			</div>
		</BodyLayout>
	);
};

export default MainLayout;

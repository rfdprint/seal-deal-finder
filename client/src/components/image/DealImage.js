import React, { useState } from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
//Components
import userImagePlaceholder from "../../images/no-img.png";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles((theme) => ({
	...theme.customStyles,
	cover: {
		width: "100%",
		objectFit: "cover",
		/*minWidth: 200,

		objectFit: "cover"*/
	},
}));

const DealImage = ({
	src,
	fallbackSrc,
	className,
	style,
	smallBadge,
	shadow,
}) => {
	const [state, setState] = useState({
		src: src,
		errored: false,
	});

	const classes = styles();

	const onError = () => {
		setState({
			...state,
			src: userImagePlaceholder,
			errored: true,
		});
	};

	return (
		<img
			src={state.src}
			className={clsx(classes.cover, className)}
			onError={() => onError()}
			alt="deal"
		/>
	);
};

DealImage.propTypes = {
	src: PropTypes.string,
	fallbackSrc: PropTypes.string,
};

export default DealImage;

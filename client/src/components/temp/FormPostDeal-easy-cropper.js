import React, { Fragment, useState, useEffect, useCallback } from "react";
import AvatarEditor from "react-avatar-editor";
import Cropper from "react-easy-crop";
import getCroppedImg from "../deals/cropImage";
import placeholder from "../../images/deal-placeholder.jpg";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
//Components
import ToolTipButton from "../../components/controls/buttons/ToolTipButton";
//MUI
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Input from "@material-ui/core/Input";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import Slider from "@material-ui/core/Slider";
//MUI Icons
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import CloseIcon from "@material-ui/icons/Close";
import AddIcon from "@material-ui/icons/Add";
//Redux
import { useSelector, useDispatch } from "react-redux";
import {
	postDeal,
	uploadDealImage,
	findAddressByZicode,
} from "../../redux/actions/dataActions";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import { colors, CircularProgress } from "@material-ui/core";
//import { CLEAR_ERRORS } from "../redux/types";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	submitButton: {
		position: "relative",
		left: "50%",
		transform: "translateX(-50%)",
	},
	progressSpinner: {
		position: "absolute",
	},
	search: {
		textAlign: "center",
		display: "block",
		margin: "0 auto",
		maxWidth: 300,
		color: theme.palette.secondary.main,
	},
	editor: {
		maxWidth: 250,
	},
	cropContainer: {
		background: "#333",
	},
	cropperSmall: {
		height: 100,
		width: 200,
		position: "relative",
	},
	cropperLarge: {
		position: "relative",
		height: 200,
		width: 440,
	},
}));

const FormPostDeal = () => {
	const location = useSelector((state) => state.data.location);
	const loading = null;
	const errors = null;
	const messages = null;

	const initializeRequiredAPIFields = {
		city: null,
		state: null,
	};

	const [input, setInput] = useState(initializeRequiredAPIFields);
	const [image, setImage] = useState(placeholder);
	const [imageBlob, setimageBlob] = useState(null);
	//const [zoom, setZoom] = useState(1);
	const [editor, setEditor] = useState({});
	const [sliderDisabled, setSliderDisabled] = useState(true);
	const [open, setOpen] = useState(false);

	//easy-react-image-crop
	const [crop, setCrop] = useState({ x: 0, y: 0 });
	const [rotation, setRotation] = useState(0);
	const [zoom, setZoom] = useState(1);
	const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
	const [croppedImage, setCroppedImage] = useState(null);

	useEffect(() => {
		setInput({
			...input,
			city: location.city,
			state: location.state,
		});
	}, [location.city]);

	const classes = styles();
	const dispatch = useDispatch();

	const handleSearchClick = () => {
		dispatch(findAddressByZicode(input.zipcode));
	};

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
		setInput(initializeRequiredAPIFields);
	};

	const handleInputChange = (e) => {
		e.preventDefault();
		setInput({
			...input,
			[e.currentTarget.name]: e.currentTarget.value,
		});
	};

	const handleImageAdd = () => {
		const fileInput = document.getElementById("dealImageInput");
		fileInput.click();
	};

	const handleSaveImageClick = () => {
		if (editor) {
			let canvas = editor.getImageScaledToCanvas().toDataURL();
			let name = image.name;

			fetch(canvas)
				.then((res) => res.blob())
				.then((blob) => {
					const formData = new FormData();
					formData.append("image", blob, name);
					dispatch(uploadDealImage(formData));
				});
		}
	};

	const handleZoomSliderChange = (event, zoom) => {
		setZoom(zoom);
	};

	const sliderZoomControl = (
		<Slider
			disabled={sliderDisabled}
			value={zoom}
			min={1}
			max={2}
			step={0.01}
			onChange={handleZoomSliderChange}
		/>
	);

	const setEditorRef = (ref) => {
		setEditor(ref);
	};

	const handleEditorChange = () => {
		if (image.name) {
			console.log(image.name);
			setSliderDisabled(false);
		}

		getCroppedImage();
		console.log("Image Changed");
	};

	const editorContent = (
		<AvatarEditor
			ref={(ref) => setEditorRef(ref)}
			image={image}
			width={300}
			height={150}
			border={0}
			rotate={0}
			scale={zoom}
			disableHiDPIScaling={true}
			onImageChange={() => handleEditorChange()}
		/>
	);

	const handleImageChange = (event) => {
		const image = event.target.files[0];
		setImage(image);
		getCroppedImage();
	};

	const getCroppedImage = () => {
		if (editor) {
			let canvas = editor.getImageScaledToCanvas().toDataURL();
			fetch(canvas)
				.then((res) => res.blob())
				.then((blob) => {
					setimageBlob(blob);
				});
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		const imageFormData = new FormData();
		imageFormData.append("image", imageBlob, image.name);

		dispatch(postDeal(input, imageFormData));
	};

	const dogImg =
		"https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000";

	const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
		setCroppedAreaPixels(croppedAreaPixels);
	}, []);

	const showCroppedImage = useCallback(async () => {
		try {
			const croppedImage = await getCroppedImg(
				dogImg,
				croppedAreaPixels,
				rotation
			);
			console.log("donee", { croppedImage });
			setCroppedImage(croppedImage);
		} catch (e) {
			console.error(e);
		}
	}, [croppedAreaPixels, rotation]);

	const onClose = useCallback(() => {
		setCroppedImage(null);
	}, []);

	///Easy-React-Crop
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.up("sm"));

	const cropEditor = (
		<div
			className={matches ? classes.cropperLarge : classes.cropperSmall}
			style={{
				height: 200,
				maxWidth: 440,
				width: "100%",
				position: "relative",
			}}>
			<Cropper
				image={placeholder}
				crop={crop}
				zoom={zoom}
				aspectRatio={16 / 9}
				onCropChange={setCrop}
				onCropComplete={onCropComplete}
				onZoomChange={setZoom}
				cropSize={{ width: 440, height: 200 }}
				guides={false}
			/>
		</div>
	);

	return (
		<Fragment>
			<Fab
				style={{
					position: "fixed",
					float: "right",
					bottom: 20,
					right: 10,
				}}
				color="primary"
				aria-label="add"
				onClick={() => handleOpen()}>
				<AddIcon color="secondary" />
			</Fab>
			<Dialog
				open={open}
				onClose={() => handleClose()}
				fullWidth
				maxWidth="md">
				<ToolTipButton
					title="Close"
					onClick={() => handleClose()}
					titleClassName={classes.closeButton}>
					<CloseIcon />
				</ToolTipButton>

				<DialogTitle>"Post New Deal"</DialogTitle>

				<input
					type="file"
					id="dealImageInput"
					onChange={(e) => handleImageChange(e)}
					hidden="hidden"
				/>

				<DialogContent className={classes.DialogContent}>
					<Grid container spacing={2}>
						{/*Column#1*/}
						{/*Field: title*/}
						<Grid item xs={12} sm={6}>
							<Grid
								container
								item
								justify="space-around"
								direction="row"
								spacing={2}
								xs={12}>
								<Grid item xs={12}>
									{cropEditor}
								</Grid>
								<Grid item>
									<div className={`image-wrapper`}>
										{sliderZoomControl}
										<Grid
											container
											justify="center"
											spacing={2}>
											<Grid item>
												<Button
													variant="outlined"
													color="primary"
													onClick={() =>
														handleImageAdd()
													}>
													Upload Image
												</Button>
											</Grid>
											<Grid item>
												<Button
													variant="outlined"
													color="primary"
													onClick={(e) => {
														handleSaveImageClick(e);
													}}>
													Save Image (Testing)
												</Button>
											</Grid>
										</Grid>
									</div>
								</Grid>
								<TextField
									size="small"
									variant="outlined"
									name="title"
									type="text"
									label="Deal Title"
									value={input.title}
									placeholder="Enter Title Name"
									className={classes.textField}
									onChange={(e) => handleInputChange(e)}
									fullWidth
								/>
								{/*Field: clientName*/}
								<TextField
									size="small"
									variant="outlined"
									name="description"
									type="text"
									label="Deal Description"
									value={input.description}
									placeholder="Enter Data"
									onChange={(e) => handleInputChange(e)}
									multiline
									rows={5}
									fullWidth
								/>
							</Grid>
						</Grid>
						{/*Column#2*/}

						<Grid item sm={6}>
							<Grid item>
								<TextField
									size="small"
									variant="outlined"
									name="zipcode"
									type="number"
									label="Zipcode"
									value={input.zipcode}
									className={classes.textField}
									onChange={(e) => handleInputChange(e)}
									fullWidth
								/>
								<Button
									fullWidth
									className={classes.search}
									align="center"
									variant="contained"
									color="primary"
									onClick={() => {
										handleSearchClick();
									}}>
									Search
								</Button>
								<hr className={classes.profileFieldSeparator} />
							</Grid>
							{location.zipcode && (
								<Fragment>
									<Grid item>
										<TextField
											size="small"
											disabled={
												location.city ? false : true
											}
											variant="outlined"
											name="address"
											type="text"
											label="Street Address"
											value={input.address}
											className={classes.textField}
											onChange={(e) =>
												handleInputChange(e)
											}
											fullWidth
										/>
									</Grid>
									<Grid
										item
										container
										justify="space-around"
										xs={12}
										spacing={1}>
										<Grid item xs={6}>
											<FormControl
												className={classes.customFields}
												variant="outlined">
												<InputLabel
													shrink={
														location.city
															? true
															: false
													}
													disabled={
														location.city
															? false
															: true
													}
													htmlFor="city">
													City
												</InputLabel>
												<OutlinedInput
													size="small"
													disabled={true}
													name="city"
													id="city"
													value={input.city}
													onChange={(e) =>
														handleInputChange(e)
													}
													label="Name"
												/>
											</FormControl>
										</Grid>

										<Grid item xs={6}>
											<FormControl
												variant="outlined"
												className={
													classes.customFields
												}>
												<InputLabel
													shrink={
														location.state
															? true
															: false
													}
													htmlFor="state">
													State
												</InputLabel>
												<OutlinedInput
													size="small"
													disabled={true}
													name="state"
													id="state"
													value={input.state}
													onChange={(e) =>
														handleInputChange(e)
													}
													label="State"
												/>
											</FormControl>
										</Grid>
									</Grid>
								</Fragment>
							)}
						</Grid>
					</Grid>

					<Button
						type="submit"
						variant="contained"
						color="primary"
						className={classes.formButtons}
						onClick={(e) => handleSubmit(e)}>
						Submit
					</Button>
				</DialogContent>
			</Dialog>
		</Fragment>
	);
};
export default FormPostDeal;

import React from "react";
import { Link } from "react-router-dom";
//MUI Components
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
// Redux
import { useDispatch, useSelector } from "react-redux";
import { logoutUser } from "../../redux/actions/userActions";

//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles(theme => ({
	...theme.customStyles,
	menuButtonRight: {
		marginRight: theme.spacing(2),
		float: "right",
		flexGrow: 1
	}
}));

const NavBarMainItems = props => {
	const authenticated = useSelector(state => state.user.authenticated);
	const authUser = useSelector(state => state.user.credentials);
	const dispatch = useDispatch();
	const classes = styles();

	const handleLogout = () => {
		dispatch(logoutUser());
	};

	return (
		<Grid align="center" justify="center" container spacing={2}>
			<Grid item>
				<Button
					size="small"
					color={props.color}
					component={Link}
					to={`/`}>
					Deal Board
				</Button>
			</Grid>
			<Grid item>
				<Button
					size="small"
					color={props.color}
					component={Link}
					to={`/system`}>
					Admin Panel
				</Button>
			</Grid>
			{!authUser.verified && (
				<Grid item>
					<Button
						fullWidth
						className={classes.verifiedButton}
						size="small"
						variant="outlined"
						color={"secondary"}
						component={Link}
						to={"/subscriptions"}>
						Get Verified
					</Button>
				</Grid>
			)}
			<Grid item className={classes.menuButtonRight}>
				{authenticated ? (
					<Button
						className={classes.menuButtonRight}
						size="small"
						variant="outlined"
						color="secondary"
						onClick={() => {
							handleLogout();
						}}>
						Logout
					</Button>
				) : null}
			</Grid>
		</Grid>
	);
};

export default NavBarMainItems;

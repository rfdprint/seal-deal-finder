import React from "react";

//Components
import ActionBar from "./ActionBar";
import LoginToolbar from "./toolbars/LoginToolbar";

const defaultProps = { color: "secondary", filter: false };
const NavBarMainMenu = ({ color = defaultProps.color }) => {
	return (
		<ActionBar>
			<LoginToolbar color={color} />
		</ActionBar>
	);
};

export default NavBarMainMenu;

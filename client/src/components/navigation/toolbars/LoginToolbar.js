import React from "react";
import { Link } from "react-router-dom";
//MUI Components
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles(theme => ({
	...theme.customStyles,
	menuButtonRight: {
		marginRight: theme.spacing(2),
		float: "right",
		flexGrow: 1
	}
}));

const LoginToolbar = props => {
	const classes = styles();

	return (
		<Grid align="center" justify="center" container spacing={2}>
			<Grid item className={classes.menuButtonRight}>
				<Button
					className={classes.menuButtonRight}
					size="small"
					variant="outlined"
					color="secondary"
					component={Link}
					to="/login">
					Login
				</Button>
			</Grid>
		</Grid>
	);
};

export default LoginToolbar;

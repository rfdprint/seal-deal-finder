import React, { Fragment, useState } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
//Components
//import PostAgreement from "../agreements/PostAgreement";

//MUI Components
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
//import AppIcon from "../../images/logo-white-v4-150px.png";
import ToolTipButton from "../../components/controls/buttons/ToolTipButton";
import Messages from "./Messages";
//Icons
import HomeIcon from "@material-ui/icons/Home";
import MenuIcon from "@material-ui/icons/Menu";
//Redux
import { useDispatch } from "react-redux";
import { logoutUser } from "../../redux/actions/userActions";

/*[theme.breakpoints.up("sm")]: {
			display: "none"
		},*/
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
		marginLeft: theme.spacing(2),
	},
	menuOpenButton: {
		marginRight: theme.spacing(2),
		float: "right",
		flexGrow: 1,
		color: "white",
	},
	logo: {
		flexGrow: 1,
	},
	center: {
		flexGrow: 1,
	},
	mainNav: {
		zIndex: 2,
	},
}));

const Navbar = (props) => {
	const classes = styles(props);
	const authenticated = useSelector((state) => state.user.authenticated);

	const dispatch = useDispatch();
	const [navVisible, setNavVisable] = useState(false);

	const handleToggleMenu = () => {
		setNavVisable(!navVisible);
	};

	let appMainNavOptions = authenticated ? (
		<Fragment>
			<Messages />
			<Button color="inherit" onClick={() => dispatch(logoutUser())}>
				Logout
			</Button>
		</Fragment>
	) : (
		<Fragment>
			<Button color="inherit" component={Link} to="/login">
				Login
			</Button>
			<Button color="inherit" component={Link} to="/signup">
				Signup
			</Button>
		</Fragment>
	);

	return (
		<Fragment>
			{navVisible ? (
				<AppBar className={classes.mainNav}>
					<Toolbar>
						<div className={classes.logo}>
							Nav for Testing Only
							{/*<img src={AppIcon} alt="rfdprint logo"></img>*/}
						</div>
						<div className={classes.center}>
							<Link to="/">
								<ToolTipButton title="Home">
									<HomeIcon />
								</ToolTipButton>
							</Link>
						</div>
						{appMainNavOptions}
						<IconButton
							style={{ float: "right", color: "white" }}
							edge="start"
							className={classes.menuButton}
							color="inherit"
							aria-label="menu"
							onClick={() => handleToggleMenu()}>
							<MenuIcon />
						</IconButton>
					</Toolbar>
				</AppBar>
			) : (
				<IconButton
					edge="start"
					className={classes.menuOpenButton}
					color="inherit"
					aria-label="menu"
					onClick={() => handleToggleMenu()}>
					<MenuIcon />
				</IconButton>
			)}
		</Fragment>
	);
};

export default Navbar;

import React, { Fragment } from "react";
import { Elements, StripeProvider } from "react-stripe-elements";
import clsx from "clsx";
//Components
import CheckoutFormSubscription from "../subscriptions/CheckoutFormSubscription";
import UserTypeBadge from "../badges/UserTypeBadge";
//MUI
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
// Redux
import { useSelector } from "react-redux";

//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	main: {
		backgroundColor: "white",

		padding: theme.spacing(3),
	},
	sideProfile: {
		minWidth: 240,
	},
	title: {
		marginBottom: 0,
	},
	subTitle: {
		marginBottom: 10,
	},
}));

const SubscriptionSection = () => {
	const user = useSelector((state) => state.user.credentials);
	const classes = styles();

	const env = process.env.REACT_APP_ENV;

	let publicKey;
	switch (env) {
		case "production":
			publicKey = process.env.REACT_APP_STRIPE_LIVE_PUBLIC_KEY; //live
			break;
		case "staging":
			publicKey = process.env.REACT_APP_STRIPE_LIVE_PUBLIC_KEY; //live
			break;
		case "development":
			publicKey = process.env.REACT_APP_STRIPE_TEST_PUBLIC_KEY; //test
			break;
		default:
			publicKey = process.env.REACT_APP_STRIPE_TEST_PUBLIC_KEY;
	}
	const userBadge = (
		<>
			<div style={{ maxWidth: 150 }}>
				<UserTypeBadge user={{ verified: true, type: "investor" }} />
			</div>
			<hr />
		</>
	);

	const subscriptionForm = (
		<StripeProvider apiKey={publicKey}>
			<div className="example">
				<Typography variant="h5" className={classes.title}>
					Upgrade to a Premium Membership for $10 (per month){" "}
					{env == "staging" ? " (Staging)" : ""}
					{env == "development" ? " (Development)" : ""}
				</Typography>
				<Typography
					align="center"
					variant="body1"
					className={classes.subTitle}>
					Show everyone that you are ligitimate by upgrading your
					account to Premium*
				</Typography>

				<Typography>Example: {userBadge}</Typography>

				<Elements
					style={{
						borderColor: "green",
						borderStyle: "solid",
						borderWidth: 1,
					}}>
					<CheckoutFormSubscription />
				</Elements>
				<hr />
				<Typography
					align="center"
					variant="body1"
					className={classes.subTitle}>
					*premium accounts will be verified by a sealdealfinder.com
					team member.
				</Typography>
			</div>
		</StripeProvider>
	);

	return (
		<Grid item align="center" sm={12}>
			<Paper className={clsx(classes.main, classes.formBackground)}>
				{user.userId ? (
					subscriptionForm
				) : (
					<Fragment>
						<Typography variant="h4" className={classes.title}>
							Subscription Payment Form
						</Typography>
						<Typography
							variant="body1"
							className={classes.subTitle}>
							You must login before you are able to upgrade.
						</Typography>
					</Fragment>
				)}
			</Paper>
		</Grid>
	);
};

export default SubscriptionSection;

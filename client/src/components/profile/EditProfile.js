import React, { Fragment, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
//Components
import ToolTipButton from "../../components/controls/buttons/ToolTipButton";
import { DealTextField } from "../controls/DealTextFields";
import DealSelect from "../controls/DealSelect";
//MUI
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import ToolTip from "@material-ui/core/Tooltip";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
//MUI Icons
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
//Redux
import { useSelector, useDispatch } from "react-redux";
import { editUserDetails } from "../../redux/actions/userActions";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import { colors } from "@material-ui/core";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	button: {
		float: "right",
	},
	toolTipButton: { color: "white" },
}));

const EditProfile = (props) => {
	const classes = styles(props);
	const dispatch = useDispatch();
	const credentials = useSelector((state) => state.user.credentials);
	const deals = useSelector((state) => state.data.deals);
	const [userDetails, setUserDetails] = useState({
		about: "",
		website: "",
		location: "",
		type: "",
	});
	const [diaglog, setDiaglog] = useState({ open: false });

	const handleInputChange = (e) => {
		setUserDetails({
			...userDetails,
			[e.currentTarget.name]: e.currentTarget.value,
		});
	};

	const handleSelectChange = (name) => (e) => {
		e.preventDefault();
		setUserDetails({
			...userDetails,
			[name]: e.target.value,
		});
		console.log(userDetails);
	};

	const handleSubmit = () => {
		console.log(userDetails);
		dispatch(editUserDetails(userDetails));
		handleClose();
	};

	const handleOpen = (credentials) => {
		setUserDetails({
			...userDetails,
			type: credentials.type ? credentials.type : "",
			about: credentials.about ? credentials.about : "",
			website: credentials.website ? credentials.website : "",
			location: credentials.location ? credentials.location : "",
		});
		setDiaglog({ open: true });
	};

	const handleClose = () => {
		setDiaglog({ ...diaglog, open: false });
	};

	const { about, website, location } = userDetails;
	const { open } = diaglog;
	return (
		<Fragment>
			<Button
				fullWidth
				className={classes.sidebarButton}
				variant={props.btnVariant ? props.btnVariant : "outlined"}
				color={props.btnColor ? props.btnColor : "secondary"}
				onClick={() => handleOpen(credentials)}>
				Edit Profile
			</Button>
			{/*<ToolTipButton
				titleClassName={classes.toolTipButton}
				title="Edit user details"
				onClick={() => handleOpen(credentials)}
				btnClassName={classes.button}>
				<EditIcon />
			</ToolTipButton>
			*/}
			<Dialog
				open={open}
				close={() => handleClose()}
				fullWidth
				maxWidth="sm">
				<DialogTitle>Edit user Details</DialogTitle>
				<DialogContent dividers>
					<form>
						<Grid container>
							<Grid id="col1" item container spacing={2}>
								<DealSelect
									id="type"
									label="User Type"
									value={userDetails.type}
									handleSelectChange={handleSelectChange(
										"type"
									)}>
									<MenuItem value={"wholesaler"}>
										Wholesaler
									</MenuItem>
									<MenuItem value={"investor"}>
										Investor
									</MenuItem>
								</DealSelect>
								<DealTextField
									halfWidth
									name="website"
									type="text"
									label="Website"
									placeholder="Enter website address"
									className={classes.textField}
									value={website}
									onChange={(e) => handleInputChange(e)}
									fullWidth
								/>
								<DealTextField
									halfWidth
									name="location"
									type="text"
									label="Location"
									placeholder="Enter a location"
									className={classes.textField}
									value={location}
									onChange={(e) => handleInputChange(e)}
									fullWidth
								/>
								<DealTextField
									name="about"
									type="text"
									label="About"
									multiline
									rows="3"
									placeholder="Enter a short about"
									className={classes.textField}
									value={about}
									onChange={(e) => handleInputChange(e)}
									fullWidth
								/>
							</Grid>
						</Grid>
					</form>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color="primary">
						Cancel
					</Button>
					<Button
						className="button"
						onClick={handleSubmit}
						color="primary">
						Save Changes
					</Button>
				</DialogActions>
			</Dialog>
		</Fragment>
	);
};

export default EditProfile;

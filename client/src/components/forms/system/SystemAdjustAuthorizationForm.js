import PropTypes from "prop-types";
import React, { Fragment, useEffect } from "react";
import DeleteUserButton from "../../controls/buttons/user/DeleteUserButton";
import SpinnerButton from "../../controls/SpinnerButton";
//MUI Components
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
// Redux
import { useDispatch, useSelector } from "react-redux";
import { setUserRole } from "../../../redux/actions/systemActions";
import {
	setAlertWithConfirmation,
	clearAlert,
} from "../../../redux/actions/alertActions";

import clsx from "clsx";

const styles = makeStyles((theme) => ({
	...theme.customStyles,
	card: {
		position: "relative",
		display: "flex",
	},
	userId: {
		textAlign: "center",
	},
	adminUser: {
		backgroundColor: theme.palette.primary.main,
	},
	adminLabel: {
		color: theme.palette.accent.main,
	},
	assignAdmin: {
		color: "red",
	},
	confirmedChipColorDeactivated: {
		backgroundColor: "lightGrey",
		color: "grey",
	},
	confirmedChipColorActivated: {
		backgroundColor: theme.palette.accent.main,
		color: "#fff",
	},
	confirmedChip: {
		marginBottom: 10,
	},
	boldText: {
		fontWeight: 700,
	},
}));

const SystemAdjustAuthorizationForm = ({ size }) => {
	const user = useSelector((state) => state.system.user);
	const alert = useSelector((state) => state.alert);
	const loading = useSelector((state) => state.UI.loading);

	const dispatch = useDispatch();
	const classes = styles();

	useEffect(() => {
		//confirmation turn off after server after response--> systemAction setUserRole()
		if (alert.confirmationForm === "systemAuthRole" && alert.confirmed) {
			const role = {
				claim: { admin: !checkAdminRole(user.roles) },
				userId: user.userId,
			};
			dispatch(setUserRole(role));
			dispatch(clearAlert());
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [alert.confirmed, dispatch]);

	const assignMsg = (
		<div>
			<Typography component="div" paragraph variant="body1">
				Are you sure you want to change{" "}
				<Typography
					component="span"
					className={classes.boldText}
					display="inline"
					variant="body1">
					{`${user.handle}'s`}
				</Typography>
				authorization role to{" "}
				<Typography
					component="span"
					className={classes.boldText}
					display="inline"
					variant="body1">
					admin
				</Typography>
				?
			</Typography>
			<Typography component="div" variant="body1">
				Assigning admin authorization to{" "}
				<Typography
					component="span"
					className={classes.boldText}
					display="inline"
					variant="body1">
					{user.handle}{" "}
				</Typography>
				will give this user access to all areas of this website,
				including restricted areas. This user will also have the ability
				to assign admin authorization to other users.
			</Typography>
		</div>
	);

	const unAssignMsg = (
		<div>
			<Typography component="div" paragraph variant="body1">
				Are you sure you want to remove{" "}
				<Typography
					component="span"
					className={classes.boldText}
					display="inline"
					variant="body1">
					admin
				</Typography>{" "}
				authorization from{" "}
				<Typography
					component="span"
					className={classes.boldText}
					display="inline"
					variant="body1">
					{user.handle}
				</Typography>
				?
			</Typography>
			<Typography component="div" variant="body1">
				This user (
				<Typography
					component="span"
					className={classes.boldText}
					display="inline"
					variant="body1">
					{user.handle}){" "}
				</Typography>{" "}
				will no longer be able to access to all areas of this website,
				including restricted areas. This user will also not have the
				ability to assign other users admin right.
			</Typography>
		</div>
	);

	const handleRoleChange = () => {
		dispatch(
			setAlertWithConfirmation(
				"systemAuthRole",
				!user.roles.admin ? "error" : "information",
				!user.roles.admin
					? "Confirm Authorization Assignment "
					: "Confirm Authorization Removal",
				!user.roles.admin ? assignMsg : unAssignMsg
			)
		);
	};

	const checkAdminRole = (systemRoles) => {
		let isAdmin = false;
		if (systemRoles) {
			if (systemRoles.admin) {
				isAdmin = systemRoles.admin;
			}
		}
		return isAdmin;
	};

	return (
		<Fragment>
			<Grid
				item
				container
				alignItems="center"
				justify="center"
				xs={12}
				spacing={2}>
				<Grid item xs={12}>
					<Typography
						component="span"
						variant="body2"
						align="center"
						justify="center">
						Adjust Authorization for:{" "}
						<Typography
							display="inline"
							variant="body2"
							className={classes.boldText}>
							{user.handle}
						</Typography>
					</Typography>
				</Grid>
				<Grid item xs={10}>
					<SpinnerButton
						size={size}
						disabled={loading}
						className={clsx(
							checkAdminRole(user.roles)
								? classes.adminUser
								: classes.notAdminUser
						)}
						fullWidth
						variant="contained"
						color="primary"
						onClick={() => handleRoleChange()}>
						{checkAdminRole(user.roles)
							? "Unassign Admin"
							: "Assign Admin"}
						{loading && (
							<CircularProgress
								size={30}
								className={classes.spinner}
							/>
						)}
					</SpinnerButton>
				</Grid>
				<Grid item xs={10}>
					<DeleteUserButton
						style={{ marginBottom: 20 }}
						fullWidth
						user={user}
						text={`Delete ${user.handle}`}
					/>
				</Grid>
			</Grid>
		</Fragment>
	);
};

SystemAdjustAuthorizationForm.propTypes = {
	size: PropTypes.any,
};

export default SystemAdjustAuthorizationForm;

import React, { Fragment } from "react";
import { DealTextField } from "../../controls/DealTextFields";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
}));
const FormInfoFields = (onChange, value, errorTitle, inputDescription) => {
	const classes = styles();
	console.log(value);
	return (
		<Fragment>
			<DealTextField
				name="title"
				type="text"
				label="Deal Title"
				value={value}
				placeholder="Enter Title Name"
				helperText={errorTitle}
				error={errorTitle ? true : false}
				onChange={onChange}
			/>

			<DealTextField
				className={classes.description}
				name="description"
				type="text"
				label="Deal Description"
				value={inputDescription}
				placeholder="Enter Data"
				onChange={onChange}
				multiline
				rows={5}
			/>
		</Fragment>
	);
};

export default FormInfoFields;

/**
 * Use the CSS tab above to style your Element's container.
 */
import React from "react";
//MUI
import Grid from "@material-ui/core/Grid";

import {
	Elements,
	CardElement,
	CardNumberElement,
	CardExpiryElement,
	CardCvcElement,
} from "react-stripe-elements";
import { useTheme } from "@material-ui/core/styles";

//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	cardElementWrapper: {
		borderColor: "green",
		borderStyle: "solid",
		borderWidth: 1,
		borderRadius: 4,
	},
	cardElement: {
		padding: 10,
	},
}));

const CardSection = () => {
	const theme = useTheme();
	const classes = styles();

	const inlineStyle = {
		base: {
			color: theme.palette.primary.main,
			fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
			fontSmoothing: "antialiased",
			":-webkit-autofill": {
				color: "#fce883",
			},
			fontSize: "16px",
			"::placeholder": {
				color: theme.palette.placeholder.main,
			},

			iconColor: theme.palette.primary.main,
		},
		invalid: {
			color: "#fa755a",
			iconColor: "#fa755a",
		},
		backgroundColor: "white",
	};

	return (
		<Grid container justify="space-between" spacing={2}>
			<Grid item xs={12}>
				<div className={classes.cardElementWrapper}>
					<CardNumberElement
						className={classes.cardElement}
						style={inlineStyle}
					/>
				</div>
			</Grid>
			<Grid item xs={5}>
				<div className={classes.cardElementWrapper}>
					<CardExpiryElement
						className={classes.cardElement}
						style={inlineStyle}
					/>
				</div>
			</Grid>
			<Grid item xs={5}>
				<div className={classes.cardElementWrapper}>
					<CardCvcElement
						className={classes.cardElement}
						style={inlineStyle}
					/>
				</div>
			</Grid>
		</Grid>
	);
};

export default CardSection;

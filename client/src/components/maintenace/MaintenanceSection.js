import React, { Fragment } from "react";
import maintenanceImage from "../../images/house-purchase-cp.jpg";
import maintenanceImageSm from "../../images/house-purchase-cp-sm.jpg";
import splashScreen from "../../images/splash-screen-logo.png";
//MUI Components
import Typography from "@material-ui/core/Typography";
import NavBarMaintenance from "../../components/navigation/NavBarMaintenance";
import { useTheme } from "@material-ui/core/styles";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const styles = makeStyles(theme => ({
	...theme.customStyles,
	content: {
		position: "relative",
		objectFit: "fill"
	},
	overlay: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: "100%"
	},
	overlayText: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -60%)",
		color: "white",
		textShadow: "2px 2px 8px #000000"
	},
	smallScreenText: {
		marginTop: 35
	}
}));

const MaintenanceSection = () => {
	const theme = useTheme();
	const xtraSmallScreen = useMediaQuery(theme.breakpoints.down("xs"));
	const smallScreen = useMediaQuery(theme.breakpoints.down("sm"));
	const mediumScreen = useMediaQuery(theme.breakpoints.down("md"));

	{
		/*src={
						smallScreen
							? maintenanceImageSm
							: xtraSmallScreen
							? maintenanceImageSm
							: maintenanceImage
					*/
	}

	const classes = styles();
	return (
		<Fragment>
			<div className={classes.overlay}>
				<Typography
					className={`${classes.overlayText} ${
						smallScreen ? classes.smallScreenText : ""
					}`}
					align="center"
					variant="h1">
					{/*` Extra Small ${xtraSmallScreen} Small? ${smallScreen}`}{" "}
					{` Medium? ${mediumScreen}`}*/}
					Seal Deal Finder Coming Soon
				</Typography>
			</div>
			<div>
				<img
					style={{ maxWidth: "100%" }}
					src={
						smallScreen
							? maintenanceImageSm
							: xtraSmallScreen
							? maintenanceImageSm
							: maintenanceImage
					}
					className="App-logo"
					alt="logo"
				/>
			</div>
		</Fragment>
	);
};

export default MaintenanceSection;

import React from "react";
//MUI Components
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { DialogActions, Button } from "@material-ui/core";

const ConfirmationDialog = (props) => {
	return (
		<Dialog open={props.open} close={() => props.handleCancelClick()}>
			<DialogTitle className={props.classNameText}>
				{props.title}
			</DialogTitle>
			<DialogContent className={props.classNameText}>
				{props.content}
			</DialogContent>

			<DialogActions>
				<Button
					className={props.classNameText}
					onClick={() => props.handleConfirmClick()}>
					Confirm
				</Button>
				<Button
					className={props.classNameText}
					onClick={() => props.handleCancelClick()}>
					Cancel
				</Button>
			</DialogActions>
		</Dialog>
	);
};

export default ConfirmationDialog;

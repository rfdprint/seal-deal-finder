import React from "react";

//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";

const styles = makeStyles(theme => ({
	...theme.customStyles,
	addressForm: {
		position: "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: "100%"
	}
}));

const App = () => {
	const classes = styles();
	return <div className={classes.addressForm}>{""}</div>;
};

export default App;

import React, { Fragment, useState, useEffect, useCallback } from "react";
import AvatarEditor from "react-avatar-editor";
import clsx from "clsx";

import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";

import placeholder from "../../images/deal-placeholder.jpg";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
//Components
import ToolTipButton from "../../components/controls/buttons/ToolTipButton";
import {
	DealTextField,
	DealLockedField,
	DealCurrencyTextField,
	DealCheckbox,
} from "../controls/DealTextFields";
import DealChip from "../controls/DealChip";
//MUI
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Chip from "@material-ui/core/Chip";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import Fab from "@material-ui/core/Fab";
import Grid from "@material-ui/core/Grid";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormHelperText from "@material-ui/core/FormHelperText";
import Input from "@material-ui/core/Input";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputAdornment from "@material-ui/core/InputAdornment";
import Slider from "@material-ui/core/Slider";
//MUI Icons
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import CloseIcon from "@material-ui/icons/Close";
import AddIcon from "@material-ui/icons/Add";
import RestoreIcon from "@material-ui/icons/Restore";
//Redux
import { useSelector, useDispatch } from "react-redux";
import {
	postDeal,
	uploadDealImage,
	findAddressByZicode,
} from "../../redux/actions/dataActions";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import { colors, CircularProgress, Checkbox } from "@material-ui/core";
//import { CLEAR_ERRORS } from "../redux/types";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	search: {
		textAlign: "center",
		display: "block",
		margin: "0 auto",
		maxWidth: 300,
		color: theme.palette.secondary.main,
	},
	imageWrapper: {
		minHeight: 200,
	},
	editor: {
		maxWidth: 250,
	},
	cropContainer: {
		background: "#333",
	},
	cropperSmall: {
		height: 200,
	},
	cropperLarge: {
		height: 200,
	},
	priceFieldPaper: {
		backgroundColor: "#fff",
		color: theme.palette.primary.main,
		borderColor: theme.palette.primary.main,
		borderWidth: 1,
		borderStyle: "solid",
		width: "100%",
		padding: 3,
		minHeight: 56,
	},
	responseArea: {
		padding: 10,
		borderWidth: 1,
		borderRadius: 4,
		padding: 10,
		borderWidth: 1,
		borderRadius: 4,
		borderStyle: "solid",
	},
	responseAreaSuccess: {
		color: theme.palette.primary.main,
		borderColor: theme.palette.primary.main,
	},
	responseAreaError: {
		color: "red",
		borderColor: "red",
	},
	submitButton: {
		color: "white",
		marginTop: 0,
		[theme.breakpoints.up("sm")]: {
			marginBottom: 10,
		},
	},
	closeIcon: {
		float: "right",
	},
	priceLabel: {
		fontWeight: 700,
	},
}));

const Prices = (props) => {
	const classes = styles();

	const numberWithCommas = (number) => {
		if (number) {
			return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}
	};

	return (
		<Paper className={classes.priceFieldPaper}>
			<Grid container justify="space-evenly" spacing={2}>
				<Grid item>
					<Typography variant="body2" align="center">
						<Grid
							direction="column"
							justify="space-evenly"
							container>
							<Grid className={classes.priceLabel} item>
								Price
							</Grid>
							<Grid item>{numberWithCommas(props.price)}</Grid>
						</Grid>
					</Typography>
				</Grid>

				<Grid item>
					<Typography variant="body2" align="center">
						<Grid
							direction="column"
							justify="space-evenly"
							container>
							<Grid className={classes.priceLabel} item>
								ARV
							</Grid>
							<Grid item>{numberWithCommas(props.arv)}</Grid>
						</Grid>
					</Typography>
				</Grid>
			</Grid>
		</Paper>
	);
};

export default Prices;

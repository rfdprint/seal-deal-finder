import React, { Fragment, useState, useEffect, useCallback } from "react";
import AvatarEditor from "react-avatar-editor";
import clsx from "clsx";

import Cropper from "react-cropper";

import placeholder from "../../images/deal-placeholder.jpg";
//Components
import ToolTipButton from "../../components/controls/buttons/ToolTipButton";
import {
	DealTextField,
	DealLockedField,
	DealCurrencyTextField,
} from "../controls/DealTextFields";
import DealChip from "../controls/DealChip";
//MUI
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Grid from "@material-ui/core/Grid";
import Slider from "@material-ui/core/Slider";
//MUI Icons
import CloseIcon from "@material-ui/icons/Close";
//Redux
import { useSelector, useDispatch } from "react-redux";
import {
	postDeal,
	editDeal,
	findAddressByZicode,
} from "../../redux/actions/dataActions";
import {
	toggleCreateDealForm,
	clearResponses,
} from "../../redux/actions/uiActions";
//Styles
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import { CircularProgress } from "@material-ui/core";
//import { CLEAR_ERRORS } from "../redux/types";
const styles = makeStyles((theme) => ({
	...theme.customStyles,
	search: {
		textAlign: "center",
		display: "block",
		margin: "0 auto",
		maxWidth: 300,
		color: theme.palette.secondary.main,
	},
	imageWrapper: {
		minHeight: 200,
	},
	editor: {
		maxWidth: 250,
	},
	cropContainer: {
		background: "#333",
	},
	cropperSmall: {
		height: 200,
	},
	cropperLarge: {
		height: 200,
	},
	priceFieldPaper: {
		backgroundColor: theme.palette.primary.main,
		width: "100%",
		padding: 10,
	},
	responseArea: {
		padding: 10,
		borderWidth: 1,
		borderRadius: 4,
		padding: 10,
		borderWidth: 1,
		borderRadius: 4,
		borderStyle: "solid",
	},
	responseAreaSuccess: {
		color: theme.palette.primary.main,
		borderColor: theme.palette.primary.main,
	},
	responseAreaError: {
		color: "red",
		borderColor: "red",
	},
	submitButton: {
		color: "white",
		marginTop: 0,
		[theme.breakpoints.up("sm")]: {
			marginBottom: 10,
		},
	},
	closeIcon: {
		float: "right",
	},
	description: {
		whiteSpace: "pre-line",
	},
}));

const CreateEditDealForm = ({ type }) => {
	const deal = useSelector((state) => state.data.deal);

	const location = useSelector((state) => state.data.location);
	const loading = useSelector((state) => state.data.loading);
	const errors = useSelector((state) => state.UI.responses.errors);
	const messages = useSelector((state) => state.UI.responses.messages);

	const match = type === fabType;

	let defaultFields;
	const defaultCreateFields = {
		title: "",
		city: "",
		state: "",
		zipcode: "",
		price: "",
		arv: "",
		rehab: "",
		agreementFee: "",
		tags: {
			incomeProducing: {
				checked: false,
				label: "",
			},
			singleFamily: {
				checked: false,
				label: "",
			},
		},
	};

	const [image, setImage] = useState({
		name: "",
		blob: null,
		file: null,
	});

	const setDefaults = () => {
		switch (type) {
			case "create":
				//Requied for API
				defaultFields = defaultCreateFields;
				break;
			case "edit":
				defaultFields = {
					...deal,
				};
				break;
			default:
				defaultFields = defaultCreateFields;
		}
		return defaultFields;
	};

	const [input, setInput] = useState(setDefaults());

	const [undoImageCrop, setUndoImageCrop] = useState(null);
	const [sliderDisabled, setSliderDisabled] = useState(true);
	const [zoom, setZoom] = useState(0.01);

	useEffect(() => {
		setInput({
			...input,
			city: location.city,
			state: location.state,
		});
	}, [location.city]);

	const classes = styles();
	const dispatch = useDispatch();

	const handleSearchClick = () => {
		dispatch(findAddressByZicode(input.zipcode));
	};

	const handleClose = () => {
		setInput(defaultFields);
		dispatch(clearResponses());
		dispatch(toggleCreateDealForm(type));
	};

	const handleInputChange = (e) => {
		e.preventDefault(e);

		setInput({
			...input,
			[e.currentTarget.name]: e.currentTarget.value,
		});
		//console.log(input);
	};

	const handleCurrencyChange = (e, c) => {
		e.preventDefault(e);

		setInput({
			...input,
			[e.target.name]: e.target.value,
		});
		//console.log(input);
	};

	const handletagChange = (e) => {
		e.preventDefault();
		setInput({
			...input,
			tags: {
				...input.tags,
				[e.currentTarget.name]: {
					checked: e.currentTarget.checked,
					label: e.currentTarget.value,
				},
			},
		});
		console.log(input);
	};

	const handleImageAdd = () => {
		const fileInput = document.getElementById("dealImageInput");
		fileInput.click();
		setSliderDisabled(false);
	};

	const handleZoomSliderChange = (event, zoom) => {
		setZoom(zoom);
	};

	const sliderZoomControl = (
		<Slider
			disabled={sliderDisabled}
			value={zoom}
			min={0.1}
			max={2}
			step={0.001}
			onChange={handleZoomSliderChange}
		/>
	);

	const handleImageChange = (event) => {
		setUndoImageCrop(null);

		const image = event.target.files[0];

		if (image) {
			let reader = new FileReader();
			reader.readAsDataURL(image);
			reader.onload = () => {
				setImage({
					...image,
					file: reader.result,
					name: image.name,
				});
			};
		}
	};

	const ref = React.createRef(null);

	///Easy-React-Crop
	const theme = useTheme();
	const matches = useMediaQuery(theme.breakpoints.up("sm"));

	const handleCrop = () => {
		setUndoImageCrop(image.file);
		const newImage = ref.current.cropper.getCroppedCanvas().toDataURL();
		setImage({ ...image, file: newImage });
	};

	const handleEnterKeyPressed = (e) => {
		console.log(e.keyCode);
		if (e.keyCode == 13) {
			handleSearchClick();
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (ref.current) {
			let canvas = ref.current.cropper.getCroppedCanvas().toDataURL();

			if (canvas) {
				fetch(canvas)
					.then((res) => res.blob())
					.then((blob) => {
						if (type === "create") {
							dispatch(postDeal(input, blob, image.name));
						} else {
							dispatch(editDeal(input, blob, image.name));
						}
					})
					.catch((err) => {
						console.log(err);
					});
			}
		} else {
			if (type === "create") {
				dispatch(postDeal(input, null, null));
			} else {
				dispatch(editDeal(input, null, null));
			}
		}
	};

	const cropEditor = image.file ? (
		<Cropper
			className={matches ? classes.cropperLarge : classes.cropperSmall}
			ref={ref}
			src={image.file}
			style={{ width: "100%" }}
			// Cropper.js options
			aspectRatio={20 / 9}
			disabled={true}
			autoCrop={true}
			autoCropArea={1}
			guides={false}
			zoomOnWheel={false}
			zoomTo={zoom}
			modal={false}
			viewMode={2}
		/>
	) : (
		<img
			style={{ maxWidth: "100%" }}
			src={type === "edit" ? deal.imageUrl : placeholder}
		/>
	);

	const actionButtonWidth = type === "edit" ? 3 : 6;

	return (
		<Fragment>
			<Dialog
				fullScreen={!matches ? true : false}
				scroll="body"
				open={open && match}
				onClose={() => handleClose()}
				fullWidth
				maxWidth="md">
				<ToolTipButton
					className={classes.closeButton}
					title="Close"
					onClick={() => handleClose()}
					titleClassName={classes.closeButton}>
					<CloseIcon />
				</ToolTipButton>

				<DialogTitle style={{ maxHeight: 20 }}>
					{type === "create" ? "Create A New Deal" : "Edit Deal"}
				</DialogTitle>

				<input
					type="file"
					id="dealImageInput"
					onChange={(e) => handleImageChange(e)}
					hidden="hidden"
				/>

				<DialogContent className={classes.DialogContent}>
					<Grid container spacing={1}>
						{/*Column#1*/}
						{/*Field: title*/}
						<Grid item xs={12} sm={12} md={6}>
							<Grid
								container
								item
								justify="space-around"
								direction="row"
								spacing={2}
								xs={12}>
								<Grid item xs={12}>
									<div className={classes.imageWrapper}>
										{cropEditor}
										{sliderZoomControl}
										<Grid
											container
											justify="center"
											spacing={2}>
											<Grid item xs={12} sm={6}>
												<Button
													fullWidth
													variant="outlined"
													color="primary"
													onClick={() =>
														handleImageAdd()
													}>
													Upload Image
												</Button>
											</Grid>

											{!undoImageCrop ? (
												<Grid
													item
													xs={12}
													sm={actionButtonWidth}>
													<Button
														fullWidth
														className={
															classes.imageButtons
														}
														variant="outlined"
														color="primary"
														onClick={(e) => {
															handleCrop(e);
														}}>
														Crop
													</Button>
												</Grid>
											) : (
												<Grid
													item
													xs={12}
													sm={actionButtonWidth}>
													<Button
														fullWidth
														className={
															classes.imageButtons
														}
														variant="outlined"
														color="primary"
														onClick={(e) => {
															setImage({
																...image,
																file: undoImageCrop,
															});
															setUndoImageCrop(
																null
															);
														}}>
														Uncrop
													</Button>
												</Grid>
											)}
											{type === "edit" && (
												<Grid
													item
													xs={12}
													sm={actionButtonWidth}>
													<Button
														fullWidth
														className={
															classes.imageButtons
														}
														variant="outlined"
														color="primary"
														onClick={(e) => {
															setImage({
																...image,
																file: null,
															});
															setUndoImageCrop(
																null
															);
														}}>
														Restore
													</Button>
												</Grid>
											)}
										</Grid>
									</div>
								</Grid>
								<DealTextField
									name="title"
									type="text"
									label="Deal Title"
									value={input.title}
									placeholder="Enter Title Name"
									helperText={
										errors.title ? errors.title : null
									}
									error={errors.title ? true : false}
									onChange={(e) => handleInputChange(e)}
								/>

								<DealTextField
									className={classes.description}
									name="description"
									type="text"
									label="Deal Description"
									value={input.description}
									placeholder="Enter Data"
									onChange={(e) => handleInputChange(e)}
									multiline
									rows={5}
								/>
							</Grid>
							<hr className={classes.invisibleSeparator} />
						</Grid>

						{/*Column#2*/}
						<Grid item xs={12} sm={12} md={6}>
							<Grid
								container
								spacing={2}
								justify="space-around"
								direction="row"
								xs={12}>
								{/*Price Fields*/}
								<Grid item container>
									<Paper className={classes.priceFieldPaper}>
										<Grid container spacing={2}>
											<DealCurrencyTextField
												halfWidth
												value={input.price}
												label="Price"
												name="price"
												helperText={
													errors.price
														? errors.price
														: null
												}
												error={
													errors.price ? true : false
												}
												onChange={(e) => {
													handleCurrencyChange(e);
												}}
											/>
											<DealCurrencyTextField
												value={input.arv}
												halfWidth
												label="ARV"
												name="arv"
												onChange={(e) =>
													handleCurrencyChange(e)
												}
											/>
											<DealCurrencyTextField
												value={input.rehab}
												halfWidth
												label="Rehab Cost"
												name="rehab"
												onChange={(e) =>
													handleCurrencyChange(e)
												}
											/>
											<DealCurrencyTextField
												value={input.assignmentFee}
												halfWidth
												label="Agreement Fee"
												name="assignmentFee"
												onChange={(e) =>
													handleCurrencyChange(e)
												}
											/>
										</Grid>
									</Paper>

									<hr
										className={classes.invisibleSeparator}
									/>
								</Grid>

								{/*Address Fields*/}
								<div className="preview"></div>
								<Button
									fullWidth
									className={classes.search}
									align="center"
									variant="contained"
									color="primary"
									onClick={() => {
										handleSearchClick();
									}}>
									Find Location by Zipcode
								</Button>
								<DealTextField
									name="zipcode"
									type="number"
									label="Zipcode"
									value={input.zipcode}
									helperText={
										errors.zipcode ? errors.zipcode : null
									}
									error={errors.zipcode ? true : false}
									onChange={(e) => handleInputChange(e)}
									onKeyDown={(e) => handleEnterKeyPressed(e)}
								/>
								<DealTextField
									disabled={location.city ? false : true}
									variant="outlined"
									name="address"
									type="text"
									label="Street Address"
									value={input.address}
									onChange={(e) => handleInputChange(e)}
								/>
								<DealLockedField
									halfWidth
									shrink={location.zipcode ? true : false}
									name="city"
									label="City"
									value={input.city}
								/>
								<DealLockedField
									halfWidth
									shrink={location.state ? true : false}
									name="state"
									label="State"
									value={input.state}
								/>
								{/*<hr
									style={{ width: "95%" }}
									className={classes.visibleSeparator}
								/>*/}
								{/*End Price Fields*/}
								{/*Deal Chips*/}
								<Grid item xs={12}>
									<Typography variant="body2">
										Optional Tags:
									</Typography>
								</Grid>

								<DealChip
									halfWidth
									id="incomeProducing"
									name="incomeProducing"
									checked={input.tags.incomeProducing.checked}
									value="Income Producing"
									label="Income Producing"
									onChange={(e) => handletagChange(e)}
								/>
								<DealChip
									halfWidth
									id="singleFamily"
									name="singleFamily"
									checked={input.tags.singleFamily.checked}
									value="Single Family"
									label="Single Family"
									onChange={(e) => handletagChange(e)}
								/>
							</Grid>
						</Grid>
						{/*End Column #2*/}
						<hr className={classes.visibleSeparator} />
						<Grid
							item
							container
							spacing={1}
							direction="row-reverse"
							xs={12}>
							<Grid item xs={12} sm={3}>
								<Button
									fullWidth
									type="submit"
									variant="contained"
									color="primary"
									className={clsx(
										classes.button,
										classes.submitButton
									)}
									onClick={(e) => handleSubmit(e)}
									disabled={loading}>
									Publish Deal
									{loading && (
										<CircularProgress
											size={30}
											className={classes.spinner}
										/>
									)}
								</Button>
							</Grid>
							<Grid item xs={12} sm={9}>
								{errors.general && (
									<div
										className={clsx(
											classes.responseArea,
											classes.responseAreaError
										)}>
										<Typography
											variant="body2"
											style={{
												color: "red",
											}}>
											{errors.general}
										</Typography>
									</div>
								)}
								{messages.success && (
									<div
										className={clsx(
											classes.responseArea,
											classes.responseAreaSuccess
										)}>
										<Typography
											variant="body2"
											className={classes.success}>
											{messages.success}
										</Typography>
									</div>
								)}
							</Grid>
						</Grid>
					</Grid>
				</DialogContent>
			</Dialog>
		</Fragment>
	);
};
export default CreateEditDealForm;

import React from "react";
import { Link } from "react-router-dom";
import placeholder from "../../images/deal-placeholder.jpg";
//Component
import Prices from "./Prices";
import UserBadge from "../badges/UserBadge";
import DealDescription from "../deals/DealDescription";
//MUI Components
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

const styles = makeStyles((theme) => ({
	...theme.customStyles,
	deal: {
		flexGrow: 1,
	},
	dealDescription: {
		minHeight: 144,
	},
	publicCard: {
		backgroundColor: theme.palette.information.main,
	},
	card: {
		position: "absolute",
		marginBottom: 20,
	},
	cardContent: {
		width: "100%",
		flexDirection: "row",
		padding: 25,
	},
	cover: {
		width: "100%",
		height: 200,
		objectFit: "contain",
		/*minWidth: 200,

		objectFit: "cover"*/
	},
	chipSecction: { position: "relative" },
	customChipDeactivated: {
		backgroundColor: "lightGrey",
		color: "grey",
	},
	customChipActivated: {
		backgroundColor: theme.palette.primary.main,
		color: "#ffffff",
	},
	customChip: {
		position: "relative",
		width: "100%",
		borderRadius: 50,
		margin: 0,
	},
	chipLabel: {
		textAlign: "center",
	},
	chipCheckBox: {
		visibility: "hidden",
		padding: "5px",
		color: "grey",
	},
	chipText: {
		padding: "2.5px",
		margin: "3px auto",
	},
}));

const DealCard = (props) => {
	const {
		imageUrl,
		title,
		description,
		price,
		arv,
		rehab,
		assignmentFee,
		userImage,
		tags,
		userHandle,
		userVerified,
		userType,
		dealId,
	} = props.deal;
	const classes = styles();

	let tagChips;
	if (tags) {
		tagChips = Object.entries(tags).map((tag) => {
			let dealTag = tag[1];
			return (
				dealTag.checked && (
					<Grid item xs={6}>
						<Paper
							className={`${
								dealTag.checked
									? classes.customChipActivated
									: classes.customChipDeactivated
							} ${classes.customChip}`}>
							<Typography className={classes.chipLabel}>
								{dealTag.label}
							</Typography>
						</Paper>
					</Grid>
				)
			);
		});
	}

	return (
		<Grid
			className={props.publicDeal ? classes.publicCard : null}
			item
			xs={12}
			component={Card}>
			<Link to={`/deals/${dealId}`}>
				<CardMedia
					className={classes.cover}
					image={imageUrl ? imageUrl : placeholder}
				/>
			</Link>

			<CardContent>
				<Grid spacing={2} container>
					<Prices
						priceFields={[
							{ label: "Price", value: price },
							{ label: "ARV", value: arv },
							{ label: "Rehab Cost", value: rehab },
							{
								label: "Assignment Fee",
								value: assignmentFee,
							},
						]}
					/>

					{/*Content Body*/}
					<Grid item container spacing={2}>
						<Grid item xs={3}>
							<UserBadge
								user={{
									handle: userHandle,
									imageUrl: userImage,
									verified: userVerified,
									type: userType,
								}}
							/>
						</Grid>
						<Grid item xs={9}>
							<Typography variant="h6">{title}</Typography>
							<Typography
								className={classes.dealDescription}
								varaint="caption">
								<DealDescription
									title={title}
									description={description}>
									{description}
								</DealDescription>
							</Typography>
						</Grid>
						<Grid
							className={classes.chipSecction}
							spacing={2}
							container>
							{tagChips ? tagChips : null}
						</Grid>
					</Grid>
				</Grid>
			</CardContent>
		</Grid>
	);
};

export default DealCard;

import React, { useState } from "react";
import PropTypes from "prop-types";
import Truncate from "react-truncate";

const ReadMore = props => {
	const [expanded, setExpanded] = useState(false);
	const [truncated, setTruncated] = useState(false);

	const handleTruncate = t => {
		console.log(t + " " + truncated);
		if (truncated !== t) {
			setTruncated(truncated);
		}
	};

	const toggleLines = event => {
		event.preventDefault();
		setExpanded(!expanded);
	};

	const { children, more, less, lines } = props;

	return (
		<div>
			<Truncate
				lines={!expanded && lines}
				ellipsis={
					<span>
						...{" "}
						<a href="#" onClick={e => toggleLines(e)}>
							{more}
						</a>
					</span>
				}
				onTruncate={t => handleTruncate(t)}>
				{children}
			</Truncate>
			{!truncated && expanded && (
				<span>
					{" "}
					<a href="#" onClick={e => toggleLines(e)}>
						{less}
					</a>
				</span>
			)}
		</div>
	);
};

ReadMore.defaultProps = {
	lines: 3,
	more: "Read more",
	less: "Show less"
};

ReadMore.propTypes = {
	children: PropTypes.node.isRequired,
	lines: PropTypes.number,
	less: PropTypes.string,
	more: PropTypes.string
};

export default ReadMore;

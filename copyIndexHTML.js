//node -e 'require("./copyIndexHTML").copy()'
var fs = require('fs');


let copyPath = "./client/build/index.html"

let pastePath = "./cloud/functions/web/index.html"


fs.copyFile(copyPath, pastePath, (err) => {
	if (err) throw err;
	console.log(`Copy successful: ${copyPath} copied to ${pastePath}`)
});

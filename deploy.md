Git:
	-GIT FLOW: Git Flow https://jeffkreeftmeijer.com/git-flow/
	-REBASE -i: https://code.visualstudio.com/docs/editor/versioncontrol

Use (Project Alias):
	-firebase use default
	-firebase use development

Auth:
	SA: gcloud auth activate-service-account \
[SERVICE ACCOUNT]\
          --key-file=[path to key] --[PROJECT ID]

Deploy:
-Switch Host Development: firebase target:apply hosting dev seal-deal-finder-dev-f0675
-Deploy: firebase deploy --only hosting:dev

-Switch Host Staging: firebase target:apply hosting staging seal-deal-finder-staging
-Deploy: firebase deploy --only hosting:staging

-Switch Host Production: firebase target:apply hosting prod seal-deal-finder-app
-Deploy: firebase deploy --only hosting:prod

-Clear: firebase ttarget:clear hosting dev seal-deal-finder-dev-f0675

Indexes:
	-Deploy: firebase deploy --only firestore:indexes
	-View: firebase firestore:indexes

Rules:
-Deploy: firebase deploy --only firestore:rules

CORS: //Google Cloud SDK **Change dir to project folder
	-Set: gsutil cors set cors.json gs://seal-deal-finder-dev-f0675.appspot.com
	-View: gsutil cors get gs://seal-deal-finder-dev-f0675.appspot.com
	-NOTE: uses cors.json in the root folder
	-Docs:
		-https://cloud.google.com/storage/docs/xml-api/put-bucket-cors
		-https://cloud.google.com/storage/docs/configuring-cors#gsutil_1

Environment Variables:
	-View: firebase functions:config:get --project dev
	-Set: firebase functions:config:set workspace.env="dev"
	-Set File: firebase functions:config:set env="$(cat env.json)"
	-Clean & Set:  firebase functions:config:unset env && firebase functions:config:set env="$(cat env.json)"
	-EMU:
		-GET & SET: firebase functions:config:get > .runtimeconfig.json
Tunnel:
	NGROK: ngrok http 3000 -host-header="localhost:3000"

 Data:
 	Emulator:
		-Export: firebase emulators:export ./data
		-Import: firebase emulators:start --import=./data
		-Dev: firebase emulators:start --import=./downloads/dev/backup
	Dev:
	-Swicth Project [gcloud]: gcloud config set project [PROJECT_ID]
		- gcloud config set project seal-deal-finder-app
		- gcloud config set project seal-deal-finder-dev-f0675

	-Import: gcloud firestore import gs://seal-deal-finder-dev-f0675.appspot.com/[EXPORT_PREFIX]/
	-Import: gcloud firestore import gs://seal-deal-finder-app.appspot.com/[EXPORT_PREFIX]/

	-Export: gcloud firestore export gs://seal-deal-finder-dev-f0675.appspot.com/[FOLDER NAME]
	-Export: gcloud firestore export gs://seal-deal-finder-app.appspot.com/[FOLDER NAME]
	Download: gsutil.cmd cp -r gs://seal-deal-finder-dev-f0675.appspot.com/[FOLDER NAME] ./downloads/dev
	Download: gsutil.cmd cp -r gs://seal-deal-finder-app.appspot.com/[FOLDER NAME] ./downloads/prod
	-JSON File:
		-Name: firebase-export-metadata.json
		- Data: {
				"version": "8.2.0",
				"firestore": {
				"version": "1.11.3",
				"path": "dev/[FOLDER NAME]",
				"metadata_file": "dev/[FOLDER NAME]/[FOLDER NAME].overall_export_metadata"
					}
				}

		-Download Docs: https://medium.com/@BrodaNoel/how-to-backup-firebase-firestore-8be94a66138c
	-Docs: https://firebase.google.com/docs/firestore/manage-data/export-import
	Cloud Shell:
		-Dev: https://console.cloud.google.com/home/dashboard?cloudshell=true&_ga=2.127811790.2128018286.1589166016-1437445610.1587849071&_gac=1.7677830.1587684790.EAIaIQobChMI0-X62Nr_6AIVCZ6fCh29jw7DEAAYASAAEgJhqPD_BwE&project=seal-deal-finder-dev-f0675&folder&organizationId

Tokens:
	-Implement: //https: //medium.com/@siddharthac6/	json-web-token-jwt-the-right-way-of-implementing-with-node-js-65b8915d550e

	-Generate: //https://www.csfieldguide.org.nz/en/interactives/rsa-key-generator/

Alias Project
	-Deploy:
		-DEV: firebase deploy --project dev
		-DEV:FUNCTIONS:  firebase deploy --only functions --project dev
	-Serve
		DEV: firebase serve --project dev

Google Cloud Services
	Update: gcloud components update


Facebook
	-TEST_URL: https://www.facebook.com/Seal-Deal-Finder-108992524222807
	-PROD_URL: https://www.facebook.com/Seal-Deal-Finder-107883014257808

	Function Handler (Facebook)
		-Localhost(facebook test page)
			Token:  facebookKey.extendedTokenNoExp
			PageId: facebookKey.pageId

		-Production(facebook production page)
			Token:  facebookKey.prod_extendedTokenNoExp
			PageId: facebookKey.prod_pageIdn

//TEST**********************
//https://stackoverflow.com/questions/42841453/running-cloud-functions-locally-gives-error-functions-config-is-not-available
//firebase functions:config:get --project dev | ac .runtimeconfig.json
//firebase functions:config:get --project dev  | ac .runtimeconfig.json

netstat -ano | findstr :8080

taskkill/pid [PID] /F

@babel/core @babel/preset-env @babel/preset-react babel-loader customize-cra dotenv eslint-plugin-react file-loader image-webpack-loader mocha node-cmd prettier react-app-rewired react-dom yargs

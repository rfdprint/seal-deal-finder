module.exports = [
  {
    dealId: "vtWLfUHqQDYkHSInDdRE",
    title: "Emulator Deal (live photo for testing)",
    userHandle: "emu",
    userId: "YTne3lhD57blGzRPFozU24sY0ik2",
    userImage: "/images/mockup-profile-2.png",
    userType: "wholesaler",
    userVerified: false,
    imageUrl:
      "https://firebasestorage.googleapis.com/v0/b/seal-deal-finder-dev-f0675.appspot.com/o/images%2Fdeals%2FH4MSBgVJf7dkWvJf1OdY%2Fv1%2FH4MSBgVJf7dkWvJf1OdY.jpg?alt=media",
    zipcode: "61101",
    price: 10000,
    arv: 15156,
    rehab: 500000,
    agreementFee: "",
    description:
      "Sed cursus turpis vitae tortor. Ut a nisl id ante tempus hendrerit. Vestibulum eu odio.\n\n\nFusce convallis metus id felis luctus adipiscing. Vivamus laoreet. Etiam ultricies nisi vel augue.\n\nPhasellus viverra nulla ut metus varius laoreet. Donec vitae orci sed dolor rutrum auctor. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl.",
    assignmentFee: 1651,
    street: "2012 Arthur Ave",
    createdAt: "2020-04-13T04:02:23.021Z",
    likeCount: 0,
    commentCount: 0,
    comments: [],
    lastUpdated: null,
    updated: false,
    imageVersion: 1,
    imageFilePath: "images/house-1.jpg",
    tags: {
      incomeProducing: {
        checked: true,
        label: "Income Producing",
      },
      singleFamily: {
        checked: true,
        label: "Single Family",
      },
    },
  },
];

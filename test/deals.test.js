const assert = require('assert');
module.exports = (firebase, authObject, getFirestore, getAdminFirestore) => {

	const {
		myId,
		theirId,
		adminId,
		myAuth,
		adminAuth
	} = authObject

	describe("Deal Rules Test", () => {
		//Deal Queries
		describe("Deal Queries", () => {

			it("Can't query all deals", async () => {
				const db = getFirestore()
				const testDoc = db.collection("deals")
				await firebase.assertFails(testDoc.get())
			})
			it("Can't read deals that have a status of draft", async () => {
				const db = getFirestore()
				const testQuery = db.collection("deals").where("status", "==", "draft");
				await firebase.assertFails(testQuery.get())
			})

			it("Can query deals that have a status of published", async () => {
				const db = getFirestore()
				const testQuery = db.collection("deals").where("status", "==", "published");
				await firebase.assertSucceeds(testQuery.get())
			})

			it("Can query deals that have a status of sold", async () => {
				const db = getFirestore()
				const testQuery = db.collection("deals").where("status", "==", "sold");
				await firebase.assertSucceeds(testQuery.get())
			})


		})
		//Deal Reads
		describe("Deal Reads", () => {
			it("Can't read a deal that another user created with the status of draft", async () => {
				const admin = getAdminFirestore();
				const dealId = "user_deal"
				const setupDoc = admin.collection("deals").doc(dealId)

				await setupDoc.set({
					userId: theirId,
					status: "draft",
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("deals").doc(dealId)
				await firebase.assertFails(testRead.get())
			})
			it("Can read a deal that another user created with the status of published", async () => {
				const admin = getAdminFirestore();
				const dealId = "user_deal"
				const setupDoc = admin.collection("deals").doc(dealId)

				await setupDoc.set({
					userId: theirId,
					status: "published",
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("deals").doc(dealId)
				await firebase.assertSucceeds(testRead.get())
			})
			it("Can read a deal that another user created with the status of sold", async () => {
				const admin = getAdminFirestore();
				const dealId = "user_deal"
				const setupDoc = admin.collection("deals").doc(dealId)

				await setupDoc.set({
					userId: theirId,
					status: "sold",
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("deals").doc(dealId)
				await firebase.assertSucceeds(testRead.get())
			})
		})

		//Deal Writes
		describe("Deal Writes", () => {
			it("Can't create a deal if not logged in", async () => {
				const db = getFirestore()
				const testDoc = db.collection("deals").doc("newDeal")
				await firebase.assertFails(testDoc.set({
					foo: "bar"
				}))
			})

			it("Can't update a deal that the user did not create", async () => {
				const admin = getAdminFirestore();
				const dealId = "user_deal"
				const setupDoc = admin.collection("deals").doc(dealId)
				await setupDoc.set({
					userId: theirId,
					status: "published",
					content: "before"
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("deals").doc(dealId)
				await firebase.assertFails(testRead.update({
					content: "after"
				}))
			})
			it("Can update a deal that the user created", async () => {
				const admin = getAdminFirestore();
				const dealId = "user_deal"
				const setupDoc = admin.collection("deals").doc(dealId)

				await setupDoc.set({
					userId: myId,
					status: "published",
					content: "before"
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("deals").doc(dealId)
				await firebase.assertSucceeds(testRead.update({
					content: "after"
				}))
			})
		})
		describe("Deal Admin", () => {
			it("Allows admin to update user deal", async () => {
				const admin = getAdminFirestore();
				const dealId = "user_deal"
				const setupDoc = admin.collection("deals").doc(dealId)
				await setupDoc.set({
					userId: theirId,
					content: "before"
				})

				const db = getFirestore(adminAuth)
				const testRead = db.collection("deals").doc(dealId)
				await firebase.assertSucceeds(testRead.update({
					content: "after"
				}))

			})
		})
	})
}

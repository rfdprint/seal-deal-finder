const assert = require('assert');
module.exports = (firebase, authObject, getFirestore, getAdminFirestore) => {

	const {
		myId,
		theirId,
		adminId,
		myAuth,
		adminAuth
	} = authObject

	describe("Subscriptions Rules Test", () => {
		//subscription Queries
		describe("Subscriptions Queries", () => {

			it("Can't query all subscriptions", async () => {
				const db = getFirestore()
				const testDoc = db.collection("subscriptions")
				await firebase.assertFails(testDoc.get())
			})

		})
		//subscription Reads

		it("Can read a single subscription created by the user", async () => {
			const admin = getAdminFirestore();
			const subscriptionId = "user_subscription"
			const setupDoc = admin.collection("subscriptions").doc(subscriptionId)
			await setupDoc.set({
				userId: myId,
				status: "active"
			})

			const db = getFirestore(myAuth)
			const testRead = db.collection("subscriptions").doc(subscriptionId)
			await firebase.assertSucceeds(testRead.get())

		})

		describe("subscription Reads", () => {
			it("Can't read a single subscriptions that another user created with the status of draft", async () => {
				const admin = getAdminFirestore();
				const subscriptionId = "user_subscription"
				const setupDoc = admin.collection("subscriptions").doc(subscriptionId)

				await setupDoc.set({
					userId: theirId,
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("subscriptions").doc(subscriptionId)
				await firebase.assertFails(testRead.get())
			})

		})

		//subscription Writes
		describe("subscription Writes", () => {
			it("Can't create a subscription if not logged in", async () => {
				const db = getFirestore()
				const testDoc = db.collection("subscriptions").doc("newsubscription")
				await firebase.assertFails(testDoc.set({
					foo: "bar"
				}))
			})

			it("Can't update a subscription that the user did not create", async () => {
				const admin = getAdminFirestore();
				const subscriptionId = "user_subscription"
				const setupDoc = admin.collection("subscriptions").doc(subscriptionId)
				await setupDoc.set({
					userId: theirId,
					status: "published",
					content: "before"
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("subscriptions").doc(subscriptionId)
				await firebase.assertFails(testRead.update({
					content: "after"
				}))
			})
			it("Can update a subscription that the user created", async () => {
				const admin = getAdminFirestore();
				const subscriptionId = "user_subscription"
				const setupDoc = admin.collection("subscriptions").doc(subscriptionId)

				await setupDoc.set({
					userId: myId,
					status: "published",
					content: "before"
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("subscriptions").doc(subscriptionId)
				await firebase.assertSucceeds(testRead.update({
					content: "after"
				}))
			})
		})
		describe("Subscription Admin", () => {
			it("Allows admin to query all subscriptions", async () => {
				const db = getFirestore(adminAuth)
				const testQuery = db.collection("subscriptions")
				await firebase.assertSucceeds(testQuery.get())
			})
			it("Allows admin to update user subscription", async () => {
				const admin = getAdminFirestore();
				const subscriptionId = "user_subscription"
				const setupDoc = admin.collection("subscriptions").doc(subscriptionId)
				await setupDoc.set({
					userId: theirId,
					content: "before"
				})

				const db = getFirestore(adminAuth)
				const testRead = db.collection("subscriptions").doc(subscriptionId)
				await firebase.assertSucceeds(testRead.update({
					content: "after"
				}))

			})
		})
	})
}

const assert = require('assert');
module.exports = (firebase, authObject, getFirestore, getAdminFirestore) => {

	const {
		myId,
		theirId,
		adminId,
		myAuth,
		adminAuth
	} = authObject

	describe("Addresses Rules Test", () => {
		//Addresses Queries
		describe("Addresses Queries", () => {

			it("Can't query all addresses", async () => {
				const db = getFirestore()
				const testDoc = db.collection("addresses")
				await firebase.assertFails(testDoc.get())
			})
			it("Can't query addresses that have a visibility of private", async () => {
				const db = getFirestore()
				const testQuery = db.collection("addresses").where("visibility", "==", "private");
				await firebase.assertFails(testQuery.get())
			})
			it("Can't query private addresses another user created", async () => {
				const db = getFirestore(myAuth)
				const testQuery = db.collection("addresses")
					.where("userId", "==", `${theirId}`)
					.where("visibility", "==", "private");
				await firebase.assertFails(testQuery.get())
			})
			it("Can query addresses that are marked public", async () => {
				const db = getFirestore()
				const testQuery = db.collection("addresses").where("visibility", "==", "public");
				await firebase.assertSucceeds(testQuery.get())
			})
			it("Can query private addresses the user created", async () => {
				const db = getFirestore(myAuth)
				const testQuery = db.collection("addresses")
					.where("userId", "==", `${myId}`)
					.where("visibility", "==", "private");
				await firebase.assertSucceeds(testQuery.get())
			})
			it("Can query all addresses the user created", async () => {
				const db = getFirestore(myAuth)
				const testQuery = db.collection("addresses")
					.where("userId", "==", `${myId}`)
				await firebase.assertSucceeds(testQuery.get())
			})
		})
		//Address Reads
		describe("Address Read", () => {
			it("Can't read a single address that another user created with the visibility of private", async () => {
				const admin = getAdminFirestore();
				const addressId = "user_address"
				const setupDoc = admin.collection("addresses").doc(addressId)

				await setupDoc.set({
					userId: theirId,
					visibility: "private",
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("addresses").doc(addressId)
				await firebase.assertFails(testRead.get())
			})
			it("Can read a single public address from another user", async () => {
				const admin = getAdminFirestore();
				const addressId = "public_address"
				const setupDoc = admin.collection("addresses").doc(addressId)
				await setupDoc.set({
					userId: theirId,
					visibility: "public"
				})

				const db = getFirestore()
				const testRead = db.collection("addresses").doc(addressId)
				await firebase.assertSucceeds(testRead.get())

			})

			it("Can read a single private address created by the user", async () => {
				const admin = getAdminFirestore();
				const addressId = "user_address"
				const setupDoc = admin.collection("addresses").doc(addressId)

				await setupDoc.set({
					userId: myId,
					visibility: "private",
				})

				const db = getFirestore(myAuth)
				const testRead = db.collection("addresses").doc(addressId)
				await firebase.assertSucceeds(testRead.get())
			})
		})
	})
}

const assert = require('assert');
const firebase = require('@firebase/testing')

//Tests
const dealsTests = require('./deals.test')
const subscriptionsTests = require('./subscriptions.test')
const addresessTests = require('./addresses.test')

const clearData = true // if true, clears emulator data before and after each test runs

const MY_PROJECT_ID = "seal-deal-finder-app";

const myId = "user_abc";
const theirId = "user_xyz";
const adminId = "user_admin";

const myAuth = {
	uid: myId,
	email: "abc@example.com"
}
const adminAuth = {
	uid: adminId,
	email: "admin@example.com",
	admin: true
}

const authObject = {
	myId,
	theirId,
	adminId,
	myAuth,
	adminAuth
}

const clearEmulatorData = async () => {
	if (clearData) {
		await firebase.clearFirestoreData({
			projectId: MY_PROJECT_ID
		})
	}
}

function getFirestore(auth) {
	return firebase.initializeTestApp({
		projectId: MY_PROJECT_ID,
		auth: auth
	}).firestore();
}

function getAdminFirestore() {
	return firebase.initializeAdminApp({
		projectId: MY_PROJECT_ID
	}).firestore()
}


beforeEach(async () => {
	clearEmulatorData()
})

describe("Seal Deal Finder App", () => {

	it("Can write to user document with the same ID as our user", async () => {
		const db = getFirestore(myAuth)
		const testDoc = db.collection("users").doc(myId)
		await firebase.assertSucceeds(testDoc.set({
			foo: "bar"
		}))
	})

	it("Can't write to user document with the different ID as our user", async () => {
		const db = getFirestore(myAuth)
		const testDoc = db.collection("users").doc(theirId)
		await firebase.assertFails(testDoc.set({
			foo: "bar"
		}))
	})




	dealsTests(firebase, authObject, getFirestore, getAdminFirestore)
	subscriptionsTests(firebase, authObject, getFirestore, getAdminFirestore)
	addresessTests(firebase, authObject, getFirestore, getAdminFirestore)

})




after(async () => {
	clearEmulatorData()
})

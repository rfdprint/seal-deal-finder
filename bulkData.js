//https://www.csfieldguide.org.nz/en/interactives/rsa-key-generator/
//https: //medium.com/@siddharthac6/json-web-token-jwt-the-right-way-of-implementing-with-node-js-65b8915d550e

require("dotenv").config();
const axios = require("axios");
const readline = require("readline");
const fs = require("fs");
const jwt = require("jsonwebtoken");

//const privateKEY = fs.readFileSync('./emu.key', 'utf8');
const runTimeConfig = require("./.runtimeconfig.json");
const config = runTimeConfig.env;
const privateKEY = config.emu.key;

//User Id***************************
const userId = "ArCia8pdWbgXhxKRs66ZuMURdZH2";
const roles = {
  claims: {
    admin: true,
    tech: true,
  },
  //email: "emu@example.com"
  email: "sales@rfdprint.com",
};

const token = jwt.sign(
  {
    userId: userId,
  },
  privateKEY,
  {
    algorithm: "HS256",
  }
);

const env = "prod"; //dev emu prod

let baseUrl;
let users;
let deals;

switch (env) {
  case "emu":
    baseUrl = "http://localhost:5000/seal-deal-finder-dev-f0675/us-central1";
    users = require("./bulkUsers");
    deals = require("./bulkDealsEmu");
    break;
    i;
  case "dev":
    baseUrl =
      "https://us-central1-seal-deal-finder-dev-f0675.cloudfunctions.net"; //Development
    users = require("./bulkUsers");
    deals = require("./bulkDeals");
    break;
  case "prod":
    baseUrl = "https://us-central1-seal-deal-finder-app.cloudfunctions.net"; //Development
    users = require("./bulkUsers");
    deals = require("./bulkDeals");
}

//let baseUrl = "http://localhost:5000/seal-deal-finder-dev-f0675/us-central1" //EMU
//let baseUrl = "https://us-central1-seal-deal-finder-dev-f0675.cloudfunctions.net" //Development
//let baseUrl = "http://localhost:5000/seal-deal-finder-app/us-central1" //Production

axios.defaults.headers.common["Authorization"] = token;

const sendRequest = (url, body) => {
  axios
    .post(`${baseUrl}${url}`, body ? body : null)
    .then((res) => {
      console.log(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);
process.stdin.on("keypress", (str, key) => {
  if (key.ctrl && key.name === "c") {
    console.log("shutting down...");
    process.exit();
  } else {
    switch (key.name) {
      case "u":
        sendRequest("/api/emu", users);
        break;
      case "d":
        sendRequest("/api/emu/deals", deals);
        break;
      case "i":
        //ensure record has emailS
        sendRequest("/api/emu/init/users", roles);
        break;
      case "e":
        sendRequest("/api/emu/env", null);
        break;
      default:
        console.log(`press the "u" key to init the emulator user`);
        console.log(`press the "d" key to init the emulator deal data`);
        console.log(
          `***CAUTION PRODUCTION*** press the "i" key to init app Admin ***CAUTION PRODUCTION***`
        );
    }
  }
});
console.log(`press the "u" key to init the emulator user`);
console.log(`press the "d" key to init the emulator deal data`);
console.log(
  `***CAUTION PRODUCTION*** press the "i" key to init app Admin ***CAUTION PRODUCTION***`
);
